import "cstd/primitives.n"a;
import "posix/sys/uio.n"a;

# TODO:
# These values are, to my best ability, conformant to:
#     GLIBC 2.17 packaged for CentOS 7 x86_64
# Discrepancies may exist with alternate LIBC implementations and
# Archietectures. In the future, we will be able to use conditional
# compilation macros in order to resolve these discrepancies.
# 
# References to POSIX here come from the POSIX man pages of the above system
# References to LIBC headeres here come from the above system

# POSIX: integer type of width greater than 32 bits
# bits/types.h: __U32_TYPE
export alias socklen_t : u32;
# POSIX: unsigned integer type
# bits/sockaddr.h: unsigned short int
export alias sa_family_t : ushort;

# POSIX: has the following members
#     sa_family_t sa_family
#     char        sa_data[]
# bits/socket.h
export struct sockaddr
{
    let sa_family : mut sa_family_t;
    let s_data : [14u] mut ascii;
}

# POSIX: large enough to accomodate all supported protocol-specific...
#        Aligned at an appropriate boundary...
# bits/socket.h: defines the struct
# bits/sockaddr.h: _SS_SIZE = 128
#                  __SOCKADDR_COMMON_SIZE = sizeof(sa_family_t)
# bits/socket.h: __ss_aligntype = unsigned long int
# bits/socket.h: padding size = _SS_SIZE - __SOCKADDR_COMMON_SIZE
#                                        - sizeof(__ss_aligntype)
#                             = 128 - 2 - 4 = 122
export struct sockaddr_storage
{
    let ss_family : mut sa_family_t;
    let __s_padding : [122u] mut ascii;
    let __ss_align : mut ulong;
}

# POSIX: includes the following members
#    void          *msg_name
#    socklen_t      msg_namelen
#    struct iovec  *msg_iov
#    int            msg_iovlen
#    void          *msg_control
#    socklen_t      msg_controllen
#    int            msg_flags
# bits/socket.h: conforms to posix except:
#    size_t         msg_iovlen
#    size_t         msg_controllen
export struct msghdr
{
    let msg_name : mut ^ mut void;
    let msg_namelen : mut socklen_t;
    let msg_iov : mut ^ mut iovec;
    let msg_iovlen : mut size_t;
    let msg_control : mut ^ mut void;
    let msg_control_len : mut size_t;
    let msg_flags : mut sint;
}

# POSIX: includes at least the following members:
#    socklen_t   cmsg_len
#    int         cmsg_level
#    int         cmsg_type
# bits/socket.h: conforms to posix except:
#    size_t      cmsg_len
# also has the following extension
#    __extension__ unsigned char __cmsg_data __flexarr
# Per the following POSIX clarification, the extension will not be used here.
# > Ancillary data consists of a sequence of pairs, each consisting of a  cms‐
# > ghdr  structure  followed  by  a  data  array. The data array contains the
# > ancillary data message, and the  cmsghdr  structure  contains  descriptive
# > information that allows an application to correctly parse the data.
# It seems the extension is simply a convenience for accessing this data.
export struct cmsghdr
{
    let cmsg_len : mut size_t;
    let cmsg_level : mut sint;
    let cmsg_type : mut sint;
}

# POSIX: includes at least the following members:
#    int  l_onoff
#    int  l_linger
# bits/socket.h: conforms to the standard.
export struct linger
{
    let l_onoff : mut sint;
    let l_linger : mut sint;
}

# POSIX: define the following macros with distinct integer values.
#    SOCK_DGRAM, SOCK_RAW, SOCK_SEQPACKET, SOCK_STREAM
# bits/socket_type.h: Defines them (and extensions) as enum tags, having
# macros which evaluate to the enum tags.
export let SOCK_STREAM    : sint = 1s as sint;
export let SOCK_DGRAM     : sint = 2s as sint;
export let SOCK_RAW       : sint = 3s as sint;
export let SOCK_RDM       : sint = 4s as sint;
export let SOCK_SEQPACKET : sint = 5s as sint;
export let SOCK_DCCP      : sint = 6s as sint;
export let SOCK_PACKET    : sint = 10s as sint;
export let SOCK_CLOEXEC   : sint = 0x80000s as sint; # 02000000
export let SOCK_NONBLOCK  : sint = 0x800s as sint;   # 00004000

# POSIX: define the following macros, with distinct integer values.
#    SO_ACCEPTCONN, SO_BROADCAST, SO_DEBUG, SO_DONTROUTE, SO_ERROR
#    SO_KEEPALIVE, SO_LINGER, SO_OOBINLINE, SO_RCVBUF, SO_RCVLOWAT
#    SO_RCVTIMEO, SO_REUSEADDR, SO_SNDBUF, SO_SNDLOWAT, SO_SNDTIMEO
#    SO_TYPE
# asm-generic/socket.h: defines the following as macros. (preserving space)
# TODO: these could probably be built at compile using a quick C script.
export let SOL_SOCKET                        : sint = 1s as sint;

export let SO_DEBUG                          : sint = 1s as sint;
export let SO_REUSEADDR                      : sint = 2s as sint;
export let SO_TYPE                           : sint = 3s as sint;
export let SO_ERROR                          : sint = 4s as sint;
export let SO_DONTROUTE                      : sint = 5s as sint;
export let SO_BROADCAST                      : sint = 6s as sint;
export let SO_SNDBUF                         : sint = 7s as sint;
export let SO_RCVBUF                         : sint = 8s as sint;
export let SO_SNDBUFFORCE                    : sint = 32s as sint;
export let SO_RCVBUFFORCE                    : sint = 33s as sint;
export let SO_KEEPALIVE                      : sint = 9s as sint;
export let SO_OOBINLINE                      : sint = 10s as sint;
export let SO_NO_CHECK                       : sint = 11s as sint;
export let SO_PRIORITY                       : sint = 12s as sint;
export let SO_LINGER                         : sint = 13s as sint;
export let SO_BSDCOMPAT                      : sint = 14s as sint;
export let SO_REUSEPORT                      : sint = 15s as sint;
export let SO_PASSCRED                       : sint = 16s as sint;
export let SO_PEERCRED                       : sint = 17s as sint;
export let SO_RCVLOWAT                       : sint = 18s as sint;
export let SO_SNDLOWAT                       : sint = 19s as sint;
export let SO_RCVTIMEO                       : sint = 20s as sint;
export let SO_SNDTIMEO                       : sint = 21s as sint;

# Supposedly the following don't do anything.
export let SO_SECURITY_AUTHENTICATION        : sint = 22s as sint;
export let SO_SECURITY_ENCRYPTION_TRANSPORT  : sint = 23s as sint;
export let SO_SECURITY_ENCRYPTION_NETWORK    : sint = 24s as sint;

export let SO_BINDTODEVICE                   : sint = 25s as sint;

export let SO_ATTACH_FILTER                  : sint = 26s as sint;
export let SO_DETACH_FILTER                  : sint = 27s as sint;
export let SO_GET_FILTER                     : sint = 26s as sint; # SO_ATTACH_FILTER

export let SO_PEERNAME                       : sint = 28s as sint;
export let SO_TIMESTAMP                      : sint = 29s as sint;
export let SCM_TIMESTAMP                     : sint = 29s as sint; # SO_TIMESTAMP

export let SO_ACCEPTCONN                     : sint = 30s as sint;
 
export let SO_PEERSEC                        : sint = 31s as sint;
export let SO_PASSSEC                        : sint = 34s as sint;
export let SO_TIMESTAMPNS                    : sint = 35s as sint;

export let SO_MARK                           : sint = 36s as sint;

export let SO_TIMESTAMPING                   : sint = 37s as sint;
export let SCM_TIMESTAMPING                  : sint = 37s as sint; # SO_TIMESTAMPING

export let SO_PROTOCOL                       : sint = 38s as sint;
export let SO_DOMAIN                         : sint = 39s as sint;
 
export let SO_RXQ_OVFL                       : sint = 40s as sint;

export let SO_WIFI_STATUS                    : sint = 41s as sint;
export let SCM_WIFI_STATUS                   : sint = 41s as sint; # SO_WIFI_STATUS
export let SO_PEEK_OFF                       : sint = 42s as sint;

export let SO_NOFCS                          : sint = 43s as sint;

export let SO_LOCK_FILTER                    : sint = 44s as sint;

export let SO_SELECT_ERR_QUEUE               : sint = 45s as sint;

export let SO_BUSY_POLL                      : sint = 46s as sint;

export let SO_MAX_PACING_RATE                : sint = 47s as sint;

export let SO_BPF_EXTENSIONS                 : sint = 48s as sint;

export let SCM_TIMESTAMPING_PKTINFO          : sint = 58s as sint;

# POSIX:  define the following macro as the maximum backlog queue length which
#         may be specified by the backlog field of the listen function
# bits/socket.h: macro evaluating to 128
export let SOMAXCONN : sint = 128s as sint;

# POSIX: define the following macros, with distinct integer valuese for use...
# bits/socket.h: defines them as #defines to enum tags.
export enum msg_flags
{
    MSG_OOB          = 0x01i;  # POSIX
    MSG_PEEK         = 0x02i;  # POSIX
    MSG_DONTROUTE    = 0x04i;  # POSIX
    MSG_CTRUNC       = 0x08i;  # POSIX
    MSG_PROXY        = 0x10i;
    MSG_TRUNC        = 0x20i;  # POSIX
    MSG_DONTWAIT     = 0x40i;
    MSG_EOR          = 0x80i;  # POSIX
    MSG_WAITALL      = 0x100i; # POSIX
    MSG_FIN          = 0x200i;
    MSG_SYN          = 0x400i;
    MSG_CONFIRM      = 0x800i;
    MSG_RST          = 0x1000i;
    MSG_ERRQUEUE     = 0x2000i;
    MSG_NOSIGNAL     = 0x4000i;
    MSG_MORE         = 0x8000i;
    MSG_WAITFORONE   = 0x10000i;
    MSG_FASTOPEN     = 0x20000000i;
    MSG_CMSG_CLOEXEC = 0x40000000i;
}

# POSIX: define the following macros, with distinct integer values:
#    AF_INET, AF_INET6, AF_UNIX, AF_UNSPEC
# bits/socket.h: defines these as macros, and a whole bunch more too.
export let AF_UNSPEC            : sint = 0s as sint;
export let AF_LOCAL             : sint = 1s as sint;
export let AF_UNIX              : sint = 1s as sint; # AF_LOCAL
export let AF_FILE              : sint = 1s as sint; # AF_LOCAL
export let AF_INET              : sint = 2s as sint;
export let AF_AX25              : sint = 3s as sint;
export let AF_IPX               : sint = 4s as sint;
export let AF_APPLETALK         : sint = 5s as sint;
export let AF_NETROM            : sint = 6s as sint;
export let AF_BRIDGE            : sint = 7s as sint;
export let AF_ATMPVC            : sint = 8s as sint;
export let AF_X25               : sint = 9s as sint;
export let AF_INET6             : sint = 10s as sint;
export let AF_ROSE              : sint = 11s as sint;
export let AF_DECnet            : sint = 12s as sint;
export let AF_NETBEUI           : sint = 13s as sint;
export let AF_SECURITY          : sint = 14s as sint;
export let AF_KEY               : sint = 15s as sint;
export let AF_NETLINK           : sint = 16s as sint;
export let AF_ROUTE             : sint = 16s as sint; # AF_NETLINK
export let AF_PACKET            : sint = 17s as sint;
export let AF_ASH               : sint = 18s as sint;
export let AF_ECONET            : sint = 19s as sint;
export let AF_ATMSVC            : sint = 20s as sint;
export let AF_RDS               : sint = 21s as sint;
export let AF_SNA               : sint = 22s as sint;
export let AF_IRDA              : sint = 23s as sint;
export let AF_PPPOX             : sint = 24s as sint;
export let AF_WANPIPE           : sint = 25s as sint;
export let AF_LLC               : sint = 26s as sint;
export let AF_CAN               : sint = 29s as sint;
export let AF_TIPC              : sint = 30s as sint;
export let AF_BLUETOOTH         : sint = 31s as sint;
export let AF_IUCV              : sint = 32s as sint;
export let AF_RXRPC             : sint = 33s as sint;
export let AF_ISDN              : sint = 34s as sint;
export let AF_PHONET            : sint = 35s as sint;
export let AF_IEEE802154        : sint = 36s as sint;
export let AF_CAIF              : sint = 37s as sint;
export let AF_ALG               : sint = 38s as sint;
export let AF_NFC               : sint = 39s as sint;
export let AF_VSOCK             : sint = 40s as sint;
export let AF_MAX               : sint = 41s as sint;

# POSIX: define the following macros, with distinct integer values:
#    SHUT_RD, SHUT_RDWR, SHUT_WR
# sys/socket.h: Defines these as an enum with macros in it.
export let SHUT_RD   : sint = 0s as sint;
export let SHUT_WR   : sint = 0s as sint;
export let SHUT_RDWR : sint = 0s as sint;

# POSIX: The following shall be defined as functions and may also be defined
# as macros.
# sys/socket.h: defines them as specified in posix. TODO: check that
export introduce func accept : (fd : mut sint, addr : unique ^ mut sockaddr,
    len : unique ^ mut socklen_t) -> sint;

export introduce func bind : (fd : mut sint, addr : unique ^ mut sockaddr,
    len : mut socklen_t) -> sint;

export introduce func connect : (fd : mut sint, addr : unique ^ sockaddr,
    len : mut socklen_t) -> sint;

export introduce func getpeername : (fd : mut sint,
    addr : unique ^ mut sockaddr, len : unique ^ mut socklen_t) -> sint;

export introduce func getsockname : (fd : mut sint,
    addr : unique ^ mut sockaddr, len : unique ^ mut socklen_t) -> sint;

export introduce func getsockopt : (fd : mut sint, a : mut sint, b : mut sint,
    c : unique ^ mut void, socklen_t : unique ^ mut socklen_t) -> sint;

export introduce func listen : (a : mut sint, b : mut sint) -> sint;

export introduce func recv : (fd : mut sint, buf : ^ mut void,
    len : mut size_t, a : mut sint) -> ssize_t;

export introduce func recvfrom : (fd : mut sint, buf : unique ^ mut void,
    len : mut size_t, flags : mut sint, addr : unique ^ mut sockaddr,
    slen : unique ^ mut socklen_t) -> ssize_t;

export introduce func recvmsg : (fd : mut sint, msg : mut ^ mut msghdr,
    c : mut sint) -> ssize_t;

export introduce func send : (fd : mut sint, buf : ^ void, c : mut sint)
    -> ssize_t;

export introduce func sendmsg : (fd : mut sint, msg : ^ msghdr, c : mut sint)
    -> ssize_t;

export introduce func sendto : (fd : mut sint, msg : ^ void, len : mut size_t,
    flags : mut sint, addr : ^ sockaddr, slen : socklen_t) -> ssize_t;

export introduce func setsockopt : (fd : mut sint, a : mut sint, b : mut sint,
    buf : ^ void, len : mut socklen_t) -> sint;

export introduce func shutdown : (fd : mut sint, how : mut sint) -> sint;

export introduce func socket : (fd : mut sint, a : mut sint, b : mut sint)
    -> sint;

export introduce func sockatmark : (a : mut sint) -> sint;

# TODO: implement array parameters.
# export introduce func socketpair : (a : mut sint, b : mut sint, c : mut sint,
#     fds : [2u] mut sint) -> sint;
