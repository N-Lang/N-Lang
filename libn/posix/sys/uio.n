import "cstd/primitives.n"a;

# TODO:
# These values are, to my best ability, conformant to:
#     GLIBC 2.17 packaged for CentOS 7 x86_64
# Discrepancies may exist with alternate LIBC implementations and
# Archietectures. In the future, we will be able to use conditional
# compilation macros in order to resolve these discrepancies.
# 
# References to POSIX here come from the POSIX man pages of the above system
# References to LIBC headeres here come from the above system

# POSIX: include at least the following members:
#    void    *iov_base
#    size_t   iov_len
# bits/uio.h: conforms to the standard
export struct iovec
{
    let iov_base : mut ^ mut void;
    let iov_len  : mut size_t;
}

# POSIX: the following shall be declared as functions and may also be defined
#        as macros.
# sys/uio.h: conforms to the standard
export introduce func readv :
    (a : mut sint, b : ^ iovec, c : mut sint) -> ssize_t;
export introduce func writev :
    (a : mut sint, b : ^ iovec, c : mut sint) -> ssize_t;
