import "posix/sys/socket.n"a;

# TODO:
# These values are, to my best ability, conformant to:
#     GLIBC 2.17 packaged for CentOS 7 x86_64
# Discrepancies may exist with alternate LIBC implementations and
# Archietectures. In the future, we will be able to use conditional
# compilation macros in order to resolve these discrepancies.
# 
# References to POSIX here come from the POSIX man pages of the above system
# References to LIBC headeres here come from the above system

# TODO: I've ignored IPv6 support so far

# POSIX: equivelant to the type uint16_t
export alias in_port_t : u16;

# POSIX: equivelant to the type uint32_t
export alias in_addr_t : u32;

# POSIX: includes at least the following member:
#    in_addr_t    s_addr
# netinet/in.h: conforms exactly
export struct in_addr
{
    let s_addr : mut in_addr_t;
}

# POSIX: includes at least the following members, in network byte order
#    sa_family_t      sin_family
#    in_port_t        sin_port
#    struct in_addr   sin_addr
# netinet/in.h: adds sin_zero for padding to sizeof sockaddr
#     sizeof(struct sockaddr): sizeof(sa_family_t) + 14 ascii
#                              = 2 + 14 = 16
#                              - sizeof(sa_family_t) - sizeof(in_port_t)
#                                    - sizeof(struct in_addr)
#                              = 16 - 2 - 2 - 4 = 8
export struct sockaddr_in
{
    let sin_family : mut sa_family_t;
    let sin_port   : mut in_port_t;
    let sin_addr   : mut in_addr;
    let sin_zero   : [8u] mut ascii;
}

# POSIX: Defines the following as Macros.
# netinet/in.h: Defines the following as enums with macros in them.
export let IPPROTO_IP       : sint = 0s as sint;
export let IPPROTO_ICMP     : sint = 1s as sint;
export let IPPROTO_IGMP     : sint = 2s as sint;
export let IPPROTO_IPIP     : sint = 4s as sint;
export let IPPROTO_TCP      : sint = 6s as sint;
export let IPPROTO_EGP      : sint = 8s as sint;
export let IPPROTO_PUP      : sint = 12s as sint;
export let IPPROTO_UDP      : sint = 17s as sint;
export let IPPROTO_IDP      : sint = 22s as sint;
export let IPPROTO_TP       : sint = 29s as sint;
export let IPPROTO_DCCP     : sint = 33s as sint;
export let IPPROTO_IPV6     : sint = 41s as sint;
export let IPPROTO_RSVP     : sint = 46s as sint;
export let IPPROTO_GRE      : sint = 47s as sint;
export let IPPROTO_ESP      : sint = 50s as sint;
export let IPPROTO_AH       : sint = 51s as sint;
export let IPPROTO_MTP      : sint = 92s as sint;
export let IPPROTO_BEETPH   : sint = 94s as sint;
export let IPPROTO_ENCAP    : sint = 98s as sint;
export let IPPROTO_PIM      : sint = 103s as sint;
export let IPPROTO_COMP     : sint = 108s as sint;
export let IPPROTO_SCTP     : sint = 132s as sint;
export let IPPROTO_UDPLITE  : sint = 136s as sint;
export let IPPROTO_RAW      : sint = 255s as sint;

# POSIX: Define the following macros:
# netinet/in.h: conforms.
export let INADDR_ANY       : u32 = 0x00000000u32;
export let INADDR_BROADCAST : u32 = 0xFFFFFFFFu32;
