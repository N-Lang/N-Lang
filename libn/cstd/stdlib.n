# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/primitives.n"a;

export let EXIT_SUCCESS : sint = 0s as sint;
export let EXIT_FAILURE : sint = 1s as sint;

export introduce func atof  : (str : mut ^ascii) -> f64;
export introduce func atoi  : (str : mut ^ascii) -> sint;
export introduce func atol  : (str : mut ^ascii) -> slong;
export introduce func atoll : (str : mut ^ascii) -> slonglong;
export introduce func strtod   : (str : mut ^ascii, endptr : mut ^mut ^mut ascii) -> f64;
export introduce func strtof   : (str : mut ^ascii, endptr : mut ^mut ^mut ascii) -> f32;
export introduce func strtol   : (str : mut ^ascii, endptr : mut ^mut ^mut ascii, base : mut sint) -> slong;
export introduce func strtoll  : (str : mut ^ascii, endptr : mut ^mut ^mut ascii, base : mut sint) -> slonglong;
export introduce func strtoul  : (str : mut ^ascii, endptr : mut ^mut ^mut ascii, base : mut sint) -> ulong;
export introduce func strtoull : (str : mut ^ascii, endptr : mut ^mut ^mut ascii, base : mut sint) -> ulonglong;

export introduce func rand : () -> sint;
export introduce func srand : (seed : mut uint) -> void;

export introduce func malloc : (size : mut size_t) -> mut ^ mut void;
export introduce func calloc : (count : mut size_t, size : mut size_t) -> mut ^ mut void;
export introduce func realloc : (ptr : mut ^ mut void, size : mut size_t) -> mut ^ mut void;
export introduce func free : (ptr : mut ^ mut void) -> void;

export introduce func abort : () -> void;
export introduce func exit : (status : mut sint) -> void;
export introduce func atexit : (fn : () -> void) -> sint;

export introduce func system : (command : mut ^ascii) -> sint;
