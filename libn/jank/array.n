# This is the super janky resizeable N-Array types that uses far too many
# void*s.
#
# essentially it is a resizeable wrapper for pointers to decayed-arrays.
#
# It has a short header before the pointer, and the pointer is to the 
# 0th element of the array, allowing the use of regular array indicing.
#
# Don't use it if you couldn't reasonably have written it yourself.

import "std/memory.n"a;

# The array header having meta data of the array. It resides in the space
# before the array.
struct arr_hdr
{
    let len : mut u; # length
    let cap : mut u; # capacity
    let isz : mut u; # item size
}

# Allocates an array of the given capacity and itemsize.
# Item size is the size of one element in the array.
# Capacity is the number of elements desired.
export func array_allocate : (cap : u, isize : u) -> ^ mut void
{
    let acap : mut u = cap;
    if acap == 0u
    {
        acap = 1u;
    }

    let ret : ^ mut void = allocate(sizeof(arr_hdr) + acap * isize);
    let arr : ^ mut arr_hdr = ret as ^ mut arr_hdr;
    arr.len = 0u;
    arr.cap = acap;
    arr.isz = isize;

    let rret : ^ mut void = ret +^ sizeof(arr_hdr);
    memory_fill(rret, acap * isize, 0u8);
    return rret;
}

# Pushes an item to the end of the array as if it is a stack.
# This resizes the array if necessary, possibly moving the array.
#
# The idiom for pushing should be:
#     array = array_push(array, item);
# because the possibly moved address is returned.
export func array_push :
    (arr : mut ^ mut void, nitem : ^ mut void) -> mut ^ mut void
{
    let hdr : ^ mut arr_hdr = (arr -^ sizeof(arr_hdr)) as ^ mut arr_hdr;
    if hdr.cap > hdr.len
    {
        memory_copy(arr +^ (hdr.len * hdr.isz), nitem, hdr.isz);
        hdr.len = hdr.len + 1u;
        return arr;
    }
    else
    {
        let buff : ^ mut void =
            allocate(sizeof(arr_hdr) + 2u * hdr.cap * hdr.isz);
        let nhdr : ^ mut arr_hdr = buff as ^ mut arr_hdr;
        nhdr.len = hdr.len;
        nhdr.cap = 2u * hdr.cap;
        nhdr.isz = hdr.isz;

        let narr : ^ mut void = buff +^ sizeof(arr_hdr);
        memory_fill(narr, nhdr.cap * nhdr.isz, 0u8);
        memory_copy(narr, arr, hdr.cap * hdr.isz);
        memory_copy(narr +^ (hdr.len * hdr.isz), nitem, hdr.isz);
        nhdr.len = nhdr.len + 1u;
        deallocate(hdr);
        return narr;
    }
}

# Removes the last thing from the end of the array, never shrinking the array.
export func array_pop : (arr : mut ^ mut void) -> void
{
    let hdr : ^ mut arr_hdr = (arr -^ sizeof(arr_hdr)) as ^ mut arr_hdr;
    hdr.len = hdr.len - 1u;
    memory_fill(arr +^ (hdr.len * hdr.isz), hdr.isz, 0u8);
}

# Gets the length of the array.
export func array_length : (arr : mut ^ void) -> u
{
    let hdr : ^ mut arr_hdr = (arr -^ sizeof(arr_hdr)) as ^ mut arr_hdr;
    return hdr.len;
}

# deallocates an array.
export func array_deallocate : (arr : mut ^ mut void) -> u
{
    let hdr : ^ mut arr_hdr = (arr -^ sizeof(arr_hdr)) as ^ mut arr_hdr;
    deallocate(hdr);
}
