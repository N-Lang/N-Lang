# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "std/astr.n"a;
import "std/io.n"a;
import "std/memory.n"a;

export introduce struct test_manager;

# Initialize this test manager.
# The newly initialized test manager will contain no tests.
export introduce func test_manager_init : (this : ^mut test_manager) -> void;

# Finalize this test manager.
export introduce func test_manager_fini : (this : ^mut test_manager) -> void;

# Add a test function to this test manager.
export introduce func test_manager_add : (
    this : ^mut test_manager,
    test : (^mut test_manager) -> void,
    test_name : ^ascii)
    -> void;

# Run all tests in the order in which they were added to this test manager.
export introduce func test_manager_run : (this : ^mut test_manager) -> u;

# Mark the current test function as failing and continue execution.
#
# Calling `test_manager_fail` followed by the execution of a return statement
# will mark the current test as failing and exit the test before further checks
# and errors will be encountered.
# EXAMPLE:
#   if (foo == null)
#   {
#       tm.fail();
#       return; # A required precondition was violated.
#               # The rest of this test depends on foo not being null.
#               # Exit now before a segmentation fault is encountered.
#   }
#   if (foo.bar != 5s32)
#   {
#       tm.fail();
#   }
#
export introduce func test_manager_fail : (this : ^mut test_manager) -> void;

# Mark the current test function as failing, print the provided astr to standard
# out, and continue execution.
#
# See `test_manager_fail` for information on how to end test execution after
# a precondition violation is encountered.
export introduce func test_manager_fail_astr :
    (this : ^mut test_manager, message : ^ascii) -> void;

################################################################################
###                                 INTERNAL                                 ###
################################################################################

export struct test_manager
{
    # Number of tests added to this test manager.
    let test_count : mut u;

    # List of all tests added to this test manager.
    let tests : mut ^mut (^mut test_manager) -> void; #!
    # List of the names of all tests added to this test manager.
    let test_names : mut ^mut ^mut ascii; #! (both pointers)

    # True if the current test being run is passing.
    # False if the current test being run has encountered a failure.
    #
    # This Boolean value is set to true just before each test is run, and may be
    # mutated from within a test function by one of the test_manager failure
    # functions.
    let is_current_test_passing : mut bool;

    func init = test_manager_init;
    func fini = test_manager_fini;

    func add = test_manager_add;

    func run = test_manager_run;

    func fail = test_manager_fail;
    func fail_astr = test_manager_fail_astr;
}

export func test_manager_init : (this : ^mut test_manager) -> void
{
    this.test_count = 0u;
    this.tests = null;
    this.test_names = null;
}

export func test_manager_fini : (this : ^mut test_manager) -> void
{
    # Deallocate buffers used to hold test names.
    loop i : mut u in [0u, this.test_count)
    {
        deallocate(this.test_names[i]);
    }

    # Deallocate lists containing test information if any tests were added to
    # the test manager.
    if this.test_count != 0u
    {
        deallocate(this.tests);
        deallocate(this.test_names);
    }
}

export func test_manager_add : (
    this : ^mut test_manager,
    test : (^mut test_manager) -> void,
    test_name : ^ascii)
    -> void
{
    let new_test_count : u = this.test_count + 1u;
    let new_test_index : u = this.test_count;

    let new_tests_buf_size : u = new_test_count * sizeof(test);
    let new_test_names_buf_size : u = new_test_count * sizeof(test_name);

    let test_name_length : u = astr_length(test_name);

    # Allocate/reallocate space for the test being added.
    if this.test_count == 0u
    {
        # The manager currently contains no tests.
        # Allocate the initial space for the first test that is being added.
        this.tests = allocate(new_tests_buf_size);
        this.test_names = allocate(new_test_names_buf_size);
    }
    else
    {
        # Reallocate buffers so that there is space for the existing tests plus
        # the test being added.
        this.tests = reallocate(this.tests, new_tests_buf_size);
        this.test_names = reallocate(this.test_names, new_test_names_buf_size);
    }

    # Update the values of test manager member variables such that they contain
    # information about the newly added test.
    this.test_count = new_test_count;

    this.tests[new_test_index] = test;

    let test_name_buf_size : u = (test_name_length + 1u) * sizeof(ascii);
    let test_name_buf : ^mut ascii = allocate(test_name_buf_size);
    astr_copy_n(test_name_buf, test_name, test_name_length);
    this.test_names[new_test_index] = test_name_buf;
}

let ASTR_TEST_RUN_TEST : ^ascii = $"[TEST      ]"a;
let ASTR_TEST_RUN_PASS : ^ascii = $"[      PASS]"a;
let ASTR_TEST_RUN_FAIL : ^ascii = $"[      FAIL]"a;

export func test_manager_run : (this : ^mut test_manager) -> u
{
    let number_of_failed_tests : mut u = 0u;

    loop i : mut u in [0u, this.test_count)
    {
        # Each test starts as passing.
        # If failure is signaled from within a test then this data member will
        # be set to `false`.
        this.is_current_test_passing = true;

        # Print test beginning.
        print(ASTR_TEST_RUN_TEST);
        printch(' 'a);
        println(this.test_names[i]);

        # Run the tests.
        this.tests[i](this);

        # Print test result.
        if this.is_current_test_passing
        {
            println(ASTR_TEST_RUN_PASS);
        }
        else
        {
            number_of_failed_tests = number_of_failed_tests + 1u;
            println(ASTR_TEST_RUN_FAIL);
        }
    }

    return number_of_failed_tests;
}

export func test_manager_fail : (this : ^mut test_manager) -> void
{
    this.is_current_test_passing = false;
}

export func test_manager_fail_astr :
    (this : ^mut test_manager, message : ^ascii) -> void
{
    test_manager_fail(this);
    println(message);
}
