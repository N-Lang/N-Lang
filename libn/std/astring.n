# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "std/astr.n"a;
import "std/panic.n"a;

export introduce struct astring;

export introduce func astring_init : (this : ^mut astring) -> void;
export introduce func astring_fini : (this : ^mut astring) -> void;

export introduce func astring_init_copy : (this : ^mut astring, other : ^astring) -> void;
export introduce func astring_init_astr : (this : ^mut astring, other : ^ascii) -> void;
export introduce func astring_init_astr_n : (this : ^mut astring, other : ^ascii, len : u) -> void;

export introduce func astring_assign : (this : ^mut astring, other : ^astring) -> void;
export introduce func astring_assign_astr : (this : ^mut astring, other : ^ascii) -> void;
export introduce func astring_assign_astr_n : (this : ^mut astring, other : ^ascii, len : u) -> void;

export introduce func astring_data : (this : ^astring) -> ^ascii;
export introduce func astring_length : (this : ^astring) -> u;
export introduce func astring_capacity : (this : ^astring) -> u;

introduce func __libn_astring_mdata : (this : ^ mut astring) -> ^ mut ascii;

export introduce func astring_is_empty : (this : ^astring) -> bool;

export introduce func astring_compare : (this : ^ astring, other : ^ astring) -> s;
export introduce func astring_compare_astr : (this : ^ astring, other : ^ ascii) -> s;
export introduce func astring_compare_astr_n : (this : ^ astring, other : ^ ascii, n : u) -> s;

export introduce func astring_at : (this : ^astring, idx : u) -> ascii;
export introduce func astring_set : (this : ^ mut astring, idx : u, ch : ascii) -> void;

export introduce func astring_reserve : (this : ^mut astring, nasciis : u) -> void;
export introduce func astring_shrink : (this : ^mut astring) -> void;
export introduce func astring_clear : (this : ^mut astring) -> void;

export introduce func astring_prepend : (this : ^mut astring, str : ^astring) -> void;
export introduce func astring_prepend_ascii : (this : ^mut astring, ch : ascii) -> void;
export introduce func astring_prepend_astr : (this : ^mut astring, str : ^ascii) -> void;
export introduce func astring_prepend_astr_n : (this : ^mut astring, str : ^ascii, n : u) -> void;

export introduce func astring_append : (this : ^mut astring, str : ^astring) -> void;
export introduce func astring_append_ascii : (this : ^mut astring, ch : ascii) -> void;
export introduce func astring_append_astr : (this : ^mut astring, str : ^ascii) -> void;
export introduce func astring_append_astr_n : (this : ^mut astring, str : ^ascii, len : u) -> void;

export introduce func astring_contains : (this : ^ astring, other : ^ astring) -> bool;
export introduce func astring_contains_astr : (this : ^ astring, other : ^ ascii) -> bool;
export introduce func astring_contains_astr_n : (this : ^ astring, other : ^ ascii, n : u) -> bool;

export introduce func astring_starts_with : (this : ^ astring, other : ^ astring) -> bool;
export introduce func astring_starts_with_astr : (this : ^ astring, other : ^ ascii) -> bool;
export introduce func astring_starts_with_astr_n : (this : ^ astring, other : ^ ascii, len : u) -> bool;

export introduce func astring_ends_with : (this : ^ astring, other : ^ astring) -> bool;
export introduce func astring_ends_with_astr : (this : ^ astring, other : ^ ascii) -> bool;
export introduce func astring_ends_with_astr_n : (this : ^ astring, other : ^ ascii, len : u) -> bool;

export introduce func astring_transform_upper : (this : ^mut astring) -> void;
export introduce func astring_transform_lower : (this : ^mut astring) -> void;
export introduce func astring_trim_front : (this : ^mut astring) -> void;
export introduce func astring_trim_back : (this : ^mut astring) -> void;

export struct astring
{
    let _data : mut ^mut ascii;
    let _length : mut u;
    let _capacity : mut u;

    func init              = astring_init;
    func init_copy         = astring_init_copy;
    func fini              = astring_fini;

    func init_astr         = astring_init_astr;
    func init_astr_n        = astring_init_astr_n;

    func assign            = astring_assign;
    func assign_astr       = astring_assign_astr;
    func assign_astr_n      = astring_assign_astr_n;

    func data              = astring_data;
    func length            = astring_length;
    func capacity          = astring_capacity;

    func __libn_mdata      = __libn_astring_mdata;

    func is_empty          = astring_is_empty;

    func compare           = astring_compare;
    func compare_astr      = astring_compare_astr;
    func compare_astr_n     = astring_compare_astr_n;

    func at                = astring_at;
    func set               = astring_set;

    func reserve           = astring_reserve;
    func shrink            = astring_shrink;
    func clear             = astring_clear;

    func prepend           = astring_prepend;
    func prepend_ascii     = astring_prepend_ascii;
    func prepend_astr      = astring_prepend_astr;
    func prepend_astr_n     = astring_prepend_astr_n;

    func append            = astring_append;
    func append_ascii      = astring_append_ascii;
    func append_astr       = astring_append_astr;
    func append_astr_n      = astring_append_astr_n;

    func contains          = astring_contains;
    func contains_astr     = astring_contains_astr;
    func contains_astr_n    = astring_contains_astr_n;

    func starts_with       = astring_starts_with;
    func starts_with_astr  = astring_starts_with_astr;
    func starts_with_astr_n = astring_starts_with_astr_n;

    func ends_with         = astring_ends_with;
    func ends_with_astr    = astring_ends_with_astr;
    func ends_with_astr_n   = astring_ends_with_astr_n;

    func transform_upper   = astring_transform_upper;
    func transform_lower   = astring_transform_lower;
    func trim_front        = astring_trim_front;
    func trim_back         = astring_trim_back;
}

################################################################################
###                                 INTERNAL                                 ###
################################################################################

let ASTRING_CAPACITY_MIN : u = 15u;

let ASTRING_DEFAULT_INIT_LENGTH : u = 0u;
let ASTRING_DEFAULT_INIT_CAPACITY : u = 15u;

func new_capacity : (length : u) -> u
{
    if length < ASTRING_CAPACITY_MIN
    {
        return ASTRING_CAPACITY_MIN;
    }
    return length + (length >> 1u);
}

func data_alloc : (capacity : u) -> ^ mut ascii
{
    return allocate((capacity + 1u) * sizeof(ascii));
}

func data_realloc : (data : ^ mut ascii, capacity : u) -> ^ mut ascii
{
    return reallocate(data, (capacity + 1u) * sizeof(ascii));
}

#(**
  * \brief Initialize an empty astring.
  *
  * \param this astring.
  *)#
export func astring_init : (this : ^mut astring) -> void
{
    this._data = data_alloc(ASTRING_DEFAULT_INIT_CAPACITY);
    this._length = ASTRING_DEFAULT_INIT_LENGTH;
    this._capacity = ASTRING_DEFAULT_INIT_CAPACITY;
    this._data[0u] = '\0'a;
}

#(**
  * \brief Initialize an astring as a copy of another astring.
  *
  * \param this astring.
  *
  * \param the astring to copy.
  *)#
export func astring_init_copy : (this : ^mut astring, other : ^astring) -> void
{
    let other_length : u = other.length();
    let capacity : u = new_capacity(other_length);

    this._data = data_alloc(capacity);
    memory_copy(this._data, other._data, other_length + 1u);

    this._length = other_length;
    this._capacity = capacity;
}

#(**
  * \brief Initialize an astring as a copy of an astr.
  *
  * \param this astring.
  *
  * \param the astr to copy.
  *)#
export func astring_init_astr : (this : ^mut astring, other : ^ascii) -> void
{
    let other_length : u = astr_length(other);
    let capacity : u = new_capacity(other_length);

    this._data = data_alloc(capacity);
    this._length = astr_copy_n(this._data, other, other_length);

    this._capacity = capacity;
}

#(**
  * \brief Initialize an astring as a copy of an astr, upto a specified length.
  *
  * \param this astring.
  *
  * \param the astr to copy.
  *
  * \param the number of characters to copy.
  *)#
export func astring_init_astr_n:
    (this : ^mut astring, other : ^ascii, len : u) -> void
{
    let capacity : u = new_capacity(len);

    this._data = data_alloc(capacity);
    this._length = astr_copy_n(this._data, other, len);

    this._capacity = capacity;
}

#(**
  * \brief Deinitialize an astring, freeing all necessary resources.
  *
  * \param this astring.
  *)#
export func astring_fini : (this : ^mut astring) -> void
{
    deallocate(this._data);
}

#(**
  * \brief reinitialize this astring, copying data from a second astring.
  *
  * \param this astring, which will be reinitialized.
  *
  * \param the astring to copy.
  *)#
export func astring_assign : (this : ^mut astring, other : ^astring) -> void
{
    this.fini();
    this.init_copy(other);
}

#(**
  * \brief reinitialize this astring, copying data from an astr.
  *
  * \param this astring.
  *
  * \param the astr to copy from.
  *)#
export func astring_assign_astr : (this : ^mut astring, other : ^ascii) -> void
{
    this.fini();
    this.init_astr(other);
}

#(**
  * \brief reinitialize this astring, copying data from an astr, upto a
  * specified length.
  *
  * \param this astring.
  *
  * \param the astr to copy from.
  *
  * \param the number of characters to copy.
  *)#
export func astring_assign_astr_n:
    (this : ^mut astring, other : ^ascii, len : u) -> void
{
    this.fini();
    this.init_astr_n(other, len);
}

#(**
  * \brief Get the data of the astring as an ``astr``, complete with null
  * terminator.
  *
  * The astr returned has the same lifefime as this astring.
  *
  * \param this astring.
  *
  * \return an ``astr`` with this's data.
  *)#
export func astring_data : (this : ^astring) -> ^ascii
{
    return this._data;
}

func __libn_astring_mdata : (this : ^ mut astring) -> ^ mut ascii
{
    return this._data;
}

#(**
  * \brief Gets the length of the astring in number of characters.
  *
  * \param this astring.
  *
  * \return the length in number of characters.
  *)#
export func astring_length : (this : ^astring) -> u
{
    return this._length;
}

#(**
  * \brief Gets the number of characters which could be held by this astring
  * before needing to automatically resize.
  *
  * The capacity is only the number of characters that can be held. It does not
  * include the space taken by a null terminator, as that space is
  * automatically accounted for.
  *
  * \param this astring.
  *
  * \return the capacity in number of characters.
  *)#
export func astring_capacity : (this : ^astring) -> u
{
    return this._capacity;
}

#(**
  * Tells if the astring currently has zero characters in it.
  *
  * \param this astring.
  *
  * \return true if it is empty.
  *)#
export func astring_is_empty : (this : ^astring) -> bool
{
    return this.length() == 0u;
}

#(**
  * \brief Compares this string to another and tests which is lexically before.
  *
  * \param this astring.
  *
  * \param the other astring.
  *
  * \return greater than 0 if this is the before other, equal to 0 if this is
  * equivelant to other, and less than 0 if this is after the other.
  *)#
export func astring_compare : (this : ^ astring, other : ^ astring) -> s
{
    return astr_compare(this.data(), other.data());
}

#(**
  * \brief Compares this string to an astr and tests which is lexically before.
  *
  * \param this astring.
  *
  * \param the astr to compare to
  *
  * \return greater than 0 if this is before the other, equal to 0 if this is
  * equivelant to other, and less than 0 if this is after the other.
  *)#
export func astring_compare_astr : (this : ^ astring, other : ^ ascii) -> s
{
    return astr_compare(this.data(), other);
}


#(**
  * \brief Compares this string to an astr and tests which is lexically before
  * up to n characters.
  *
  * \param this astring.
  *
  * \param the astr to compare to.
  *
  * \param the number of characters to compare.
  *
  * \return greater than 0 if this is before the other, equal to 0 if this is
  * equivelant to other, and less than 0 if this is after the other.
  *)#
export func astring_compare_astr_n : (this : ^ astring, other : ^ ascii, n : u) -> s
{
    return astr_compare_n(this.data(), other, n);
}

#(**
  * \brief Returns the character at a specific index.
  *
  * If the index is out of bounds, the null terminator is returned.
  *
  * \param this astring.
  *
  * \param the index into the astring.
  *
  * \return the desired character.
  *)#
export func astring_at : (this : ^astring, idx : u) -> ascii
{
    if idx >= this._length { panic(PANIC_INVALID_INDEX); }
    return this._data[idx];
}

#(**
  * \brief Set the character at a specific index, to the given character.
  *
  * If the character is a null terminator, the length of the astring is
  * adjusted appropriately.
  *
  * \param this astring.
  *
  * \param the index into the astring.
  *
  * \param the character to set.
  *)#
export func astring_set : (this : ^ mut astring, idx : u, ch : ascii) -> void
{
    if idx >= this._length { panic(PANIC_INVALID_INDEX); }
    if ch == '\0'a { this._length = idx; }
    this._data[idx] = ch;
}

#(**
  * \brief Extends the capacity to the given size, if it is not already at or
  * above that size.
  *
  * \param this astring.
  *
  * \param the new desired capacity.
  *)#
export func astring_reserve : (this : ^mut astring, nasciis : u) -> void
{
    let curr_capacity : u = this._capacity;

    if curr_capacity >= nasciis {return;}

    this._data = data_realloc(this._data, nasciis);
    this._capacity = nasciis;
}

#(**
  * \brief reduces the astring's ``capacity`` to a minimal size required to
  * store the data.
  *
  * \param this astring.
  *)#
export func astring_shrink : (this : ^ mut astring) -> void
{
    let new_cap : u = this.length();
    let new_buf : ^ mut ascii = data_alloc(new_cap);
    memory_copy(new_buf, this._data, this.length());
    new_buf[this.length()] = '\0'a;
    deallocate(this._data);
    this._data = new_buf;
}

#(**
  * \brief deletes all data from this astring, and reduces its capacity to
  * a minimum.
  *
  * \param this astring.
  *)#
export func astring_clear : (this : ^mut astring) -> void
{
    this.set(0u, '\0'a);
    this.shrink();
}

#(**
  * \brief Copy the other astring to the beginning of this astring, with the
  * data of this astring following it.
  *
  * \param this astring (destination).
  *
  * \param the astring to copy (source).
  *)#
export func astring_prepend : (this : ^mut astring, str : ^astring) -> void
{
    this.prepend_astr_n(str.data(), str.length());
}

#(**
  * \brief Copy a single character to the beginning of this astring.
  *
  * \param this astring.
  *
  * \param the character to prepend.
  *)#
export func astring_prepend_ascii : (this : ^mut astring, ch : ascii) -> void
{
    let old_len : u = this.length();

    this.reserve(old_len + 1u);
    memory_copy_overlapping(this._data +^ 1u, this._data, old_len);
    @this.__libn_mdata() = ch;
    if ch == '\0'a { this._length = 0u; }
    else
    {
        this._length = this._length + 1u;
        this.__libn_mdata()[this.length()] = '\0'a;
    }
}

#(**
  * \brief Copy the contents of the astr to the beginning of this astring, with
  * data of this astring following it.
  *
  * \param this astring (destination).
  *
  * \param the astr to copy (source).
  *)#
export func astring_prepend_astr : (this : ^mut astring, str : ^ascii) -> void
{
    let old_len : u = this.length();
    let str_len : u = astr_length(str);

    this.reserve(old_len + str_len);
    memory_copy_overlapping(this._data +^ str_len, this._data, old_len);
    memory_copy(this._data, str, str_len);
    this._length = old_len + str_len;
    this.__libn_mdata()[this.length()] = '\0'a;
}

#(**
  * \brief Copy the contents of the astr to the beginning of this astring upto
  * n many characters, with data data of this astring following it.
  *
  * \param this astring (destination).
  *
  * \param the astr to copy (source).
  *
  * \param the length to copy.
  *)#
export func astring_prepend_astr_n : (this : ^mut astring, str : ^ascii, n : u) -> void
{
    let old_len : u = this.length();
    let str_len : mut u = astr_length(str);

    if n < str_len { str_len = n; }

    this.reserve(old_len + n);
    memory_copy_overlapping(this._data +^ str_len, this._data, old_len);
    memory_copy(this._data, str, str_len);
    this._length = old_len + str_len;
    this.__libn_mdata()[this.length()] = '\0'a;
}

#(**
  * \brief Copy the other astring to the end of this astring, with the
  * data of this astring preceding it.
  *
  * \param this astring (destination).
  *
  * \param the astring to copy (source).
  *)#
export func astring_append : (this : ^mut astring, str : ^astring) -> void
{
    let old_len : u = this.length();
    let str_len : u = str._length;

    this.reserve(old_len + str_len);
    memory_copy(this._data +^ old_len, str.data(), str_len + 1u);
    this._length = old_len + str_len;
}

#(**
  * \brief Copy a single character to the end of this astring.
  *
  * \param this astring.
  *
  * \param the character to prepend.
  *)#
export func astring_append_ascii : (this : ^mut astring, ch : ascii) -> void
{
    this.reserve(this.length() + 1u);
    @(this.__libn_mdata() +^ this.length()) = ch;
    @(this.__libn_mdata() +^ (this.length() + 1u)) = '\0'a;
    this._length = this.length() + 1u;
}

#(**
  * \brief Copy the contents of the astr to the end of this astring, with
  * data of this astring preceding it.
  *
  * \param this astring (destination).
  *
  * \param the astr to copy (source).
  *)#
export func astring_append_astr : (this : ^mut astring, str : ^ascii) -> void
{
    let old_len : u = this.length();
    let str_len : u = astr_length(str);

    this.reserve(old_len + str_len);
    memory_copy(this._data +^ old_len, str, str_len);
    this._length = old_len + str_len;
    this.__libn_mdata()[this._length] = '\0'a;
}

#(**
  * \brief Copy the contents of the astr to the beginning of this astring upto
  * n many characters, with data data of this astring following it.
  *
  * \param this astring (destination).
  *
  * \param the astr to copy (source).
  *
  * \param the length to copy.
  *)#
export func astring_append_astr_n:
    (this : ^mut astring, str : ^ascii, len : u) -> void
{
    let old_len : u = this.length();

    this.reserve(old_len + len);
    this._length = astr_append_n(this.__libn_mdata(), str, len);
}

#(**
  * Check if this astring has a sub-sequence of characters which matches the
  * given search term astring.
  *
  * \param this astring.
  *
  * \param the search term astring.
  *
  * \return true if the search term is found within this astring.
  *)#
export func astring_contains : (this : ^ astring, term: ^ astring) -> bool
{
    return this.contains_astr_n(term.data(), term.length());
}

#(**
  * Check if this astring has a sub-sequence of characters which matches the
  * given search term astr.
  *
  * \param this astring.
  *
  * \param the search term astr.
  *
  * \return true if the search term is found within this astring.
  *)#
export func astring_contains_astr : (this : ^ astring, other : ^ ascii) -> bool
{
    return this.contains_astr_n(other, astr_length(other));
}

#(**
  * \brief Check if this astring has a sub-sequence of characters which is the
  * same as the given term.
  *
  * \param This astring.
  *
  * \param the search term.
  *
  * \param the number of characters.
  *
  * \return true if the string is contained.
  *)#
export func astring_contains_astr_n : (this : ^ astring, term : ^ ascii, n : u) -> bool
{
    let t_walk : mut ^ ascii = this.data();
    let len : mut u = astr_length(term);
    if n < len { len = n; }
    if len == 0u { return true; }
    if len > this.length() { return false; }
    let t_iter : u = this.length() - len;
    loop i : mut u in [0u, t_iter]
    {
        let t2_walk : mut ^ ascii = t_walk;
        let term_walk : mut ^ ascii = term;
        loop j : mut u in [0u, len)
        {
            if @t2_walk != @term_walk { break; }
            if @term_walk == '\0'a || j + 1u == len { return true; }
            t2_walk = t2_walk +^ 1u;
            term_walk = term_walk +^ 1u;
        }
        t_walk = t_walk +^ 1u;
    }
    return false;
}

#(**
  * \brief Checks if the leading characters of this astring match the
  * characters in the given search term.
  *
  * \param This astring.
  *
  * \param the term to check if it starts with.
  *
  * \return true if the term matches the beginning of this astring.
  *)#
export func astring_starts_with : (this : ^ astring, term : ^ astring) -> bool
{
    return this.starts_with_astr_n(term.data(), term.length());
}


#(**
  * \brief Checks if the leading characters of this astring match the
  * characters in the given search term.
  *
  * \param This astring.
  *
  * \param the term to check if it starts with.
  *
  * \return true if the term matches the beginning of this astring.
  *)#
export func astring_starts_with_astr : (this : ^ astring, term : ^ ascii) -> bool
{
    return this.starts_with_astr_n(term, astr_length(term));
}

#(**
  * \brief Checks if the leading characters of this astring match the
  * characters in the given search term, checking up to n characters of the
  * search term.
  *
  * \param This astring.
  *
  * \param the term to check if it starts with.
  *
  * \param the number of characters to check.
  *
  * \return true if the term matches the beginning of this astring.
  *)#
export func astring_starts_with_astr_n : (this : ^ astring, term : ^ ascii, n : u) -> bool
{
    let len : mut u = astr_length(term);
    if len > n { len = n; }
    if len == 0u { return true; }
    if len > this.length() { return false; }

    let this_walk : mut ^ ascii = this.data();
    let term_walk : mut ^ ascii = term;
    loop i : mut u in [0u, len)
    {
        if @this_walk != @term_walk { return false; }
        this_walk = this_walk +^ 1u;
        term_walk = term_walk +^ 1u;
    }
    return true;
}

#(**
  * \brief Checks if the trailing characters of this astring match the
  * characters in the given search term.
  *
  * \param This astring.
  *
  * \param the term to check if it ends with.
  *
  * \return true if the term matches the end of this astring.
  *)#
export func astring_ends_with : (this : ^ astring, term : ^ astring) -> bool
{
    return this.ends_with_astr_n(term.data(), term.length());
}

#(**
  * \brief Checks if the trailing characters of this astring match the
  * characters in the given search term.
  *
  * \param This astring.
  *
  * \param the term to check if it ends with.
  *
  * \return true if the term matches the end of this astring.
  *)#
export func astring_ends_with_astr : (this : ^ astring, term : ^ ascii) -> bool
{
    return this.ends_with_astr_n(term, astr_length(term));
}

#(**
  * \brief Test if this astring's trailing characters match the given search
  * term.
  *
  * The n characters are counted backwards from the end of the term.
  *
  * \param this astring.
  *
  * \param the search term.
  *
  * \param the number of characters.
  *
  * \return true if the end of this astring matches the search term.
  *)#
export func astring_ends_with_astr_n : (this : ^ astring, term : ^ ascii, n : u) -> bool
{
    let tlen : mut u = astr_length(term);
    let len : mut u = tlen;
    if len > n { len = n; }
    if len == 0u { return true; }
    if len > this.length() { return false; }

    let this_walk : mut ^ ascii = this.data() +^ (this.length() - 1u);
    let term_walk : mut ^ ascii = term +^ (tlen - 1u);
    loop i : mut u in [0u, len)
    {
        if @this_walk != @term_walk { return false; }
        this_walk = this_walk -^ 1u;
        term_walk = term_walk -^ 1u;
    }
    return true;
}

export func astring_transform_upper : (this : ^mut astring) -> void
{
    let ch : mut ^mut ascii = this.__libn_mdata();
    let end : ^ascii = this.data() +^ this.length();
    loop ch != end
    {
        @ch = ascii_to_upper(@ch);
        ch = ch +^ 1u;
    }
}

export func astring_transform_lower : (this : ^mut astring) -> void
{
    let ch : mut ^mut ascii = this.__libn_mdata();
    let end : ^ascii = this.data() +^ this.length();
    loop ch != end
    {
        @ch = ascii_to_lower(@ch);
        ch = ch +^ 1u;
    }
}

export func astring_trim_front : (this : ^mut astring) -> void
{
    let front : ^ascii = this.data();
    if !ascii_is_space(@front){return;}

    let ch : mut ^ascii = front;
    loop ascii_is_space(@ch){ch = ch +^ 1u;}
    let num_asciis : u = ch -^^ front;

    let new_length : u = this.length() - num_asciis;
    memory_copy_overlapping(this._data, this._data +^ num_asciis, new_length);
}

export func astring_trim_back : (this : ^mut astring) -> void
{
    let back : ^ascii = this.data() +^ this.length() -^ 1u;
    if !ascii_is_space(@back){return;}

    let ch : mut ^ascii = back;
    loop ascii_is_space(@ch){ch = ch -^ 1u;}
    let num_asciis : u = back -^^ ch;

    let new_length : u = this.length() - num_asciis;
}
