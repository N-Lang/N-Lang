import "std/character.n"a;
import "std/integer.n"a;
import "std/math.n"a;
import "std/memory.n"a;

export introduce func astr_length : (str : ^ascii) -> u;

export introduce func astr_copy_n : (dest : ^mut ascii, src : ^ascii, n : u) -> u;
export introduce func astr_copy : (dest : ^mut ascii, src : ^ascii) -> u;

export introduce func astr_compare : (lhs : ^ascii, rhs : ^ascii) -> s;
export introduce func astr_eqauals : (lhs : ^ascii, rhs : ^ascii) -> bool;
export introduce func astr_ne : (lhs : ^ascii, rhs : ^ascii) -> bool;

export introduce func astr_compare_icase : (lhs : ^ascii, rhs : ^ascii) -> s;
export introduce func astr_eq_icase : (lhs : ^ascii, rhs : ^ascii) -> bool;
export introduce func astr_ne_icase : (lhs : ^ascii, rhs : ^ascii) -> bool;

export introduce func astr_compare_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> s;
export introduce func astr_eq_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool;
export introduce func astr_ne_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool;

export introduce func astr_compare_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> s;
export introduce func astr_eq_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool;
export introduce func astr_ne_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool;

export introduce func astr_contains : (lsh : ^ ascii, rhs : ^ ascii) -> bool;
export introduce func astr_starts_with : (lsh : ^ ascii, rhs : ^ ascii) -> bool;
export introduce func astr_ends_with : (lsh : ^ ascii, rhs : ^ ascii) -> bool;

# astr_to_* functions return the number of chars from the input string traversed
# (positive) on success or 0u on failure.
export introduce func astr_to_u8 : (str : ^ascii, to : ^mut u8, base : u8) -> u;
export introduce func astr_to_u16 : (str : ^ascii, to : ^mut u16, base : u8) -> u;
export introduce func astr_to_u32 : (str : ^ascii, to : ^mut u32, base : u8) -> u;
export introduce func astr_to_u64 : (str : ^ascii, to : ^mut u64, base : u8) -> u;
export introduce func astr_to_u : (str : ^ascii, to : ^mut u, base : u8) -> u;
export introduce func astr_to_s8 : (str : ^ascii, to : ^mut s8, base : u8) -> u;
export introduce func astr_to_s16 : (str : ^ascii, to : ^mut s16, base : u8) -> u;
export introduce func astr_to_s32 : (str : ^ascii, to : ^mut s32, base : u8) -> u;
export introduce func astr_to_s64 : (str : ^ascii, to : ^mut s64, base : u8) -> u;
export introduce func astr_to_s : (str : ^ascii, to : ^mut s, base : u8) -> u;

################################################################################
###                                 INTERNAL                                 ###
################################################################################

#(**
  * \brief Determine the length (in characters) of the astr.
  *
  * \param this astr.
  *
  * \return the number of characters excluding the null terminator.
  *)#
export func astr_length : (str : ^ascii) -> u
{
    let walker : mut ^ascii = str;
    loop @walker != '\0'a
    {
        walker = walker +^ 1u;
    }
    return walker -^^ str;
}

#(**
  * \brief Copies n many characters from the source astr to the destination
  * astr.
  *
  * If there are fewer than n characters in the source buffer, then only as
  * many characters as are present in the source buffer are copied to the
  * destination buffer.
  * The destination buffer is always null terminated.
  * This behavior differs from libc's `strncpy`.
  *
  * \param the destination buffer. this must have an available capacity of at
  * least ``n + 1`` characters.
  *
  * \param the source buffer.
  *
  * \param the number of characters to copy.
  *
  * \return the number of characters actually copied.
  *)#
export func astr_copy_n : (dest : ^mut ascii, src : ^ascii, n : u) -> u
{
    let dest_walker : mut ^mut ascii = dest;
    let src_walker : mut ^ascii = src;
    let asciis_remaining : mut u = n;
    loop asciis_remaining != 0u && @src_walker != '\0'a
    {
        @dest_walker = @src_walker;
        src_walker = src_walker +^ 1u;
        dest_walker = dest_walker +^ 1u;
        asciis_remaining = asciis_remaining - 1u;
    }
    @dest_walker = '\0'a;
    return src_walker -^^ src;
}

#(**
  * \brief Copies all characters from the source astr to the destination astr.
  *
  * The destination buffer is always null terminated.
  *
  * \param the destination buffer. this must have an available capacity of at
  * least ``astr_length(src) + 1`` characters.
  *
  * \param the source buffer.
  *
  * \return the number of characters actually copied.
  *)#
export func astr_copy : (dest : ^mut ascii, src : ^ascii) -> u
{
    let dest_walker : mut ^mut ascii = dest;
    let src_walker : mut ^ascii = src;
    loop @src_walker != '\0'a
    {
        @dest_walker = @src_walker;
        src_walker = src_walker +^ 1u;
        dest_walker = dest_walker +^ 1u;
    }
    @dest_walker = '\0'a;
    return src_walker -^^ src;
}

#(**
  * \brief Copies n many characters from the source astr to the end of the
  * destination astr.
  *
  * If there are fewer than n characters in the source buffer, then only as
  * many characters as are present in the source buffer are copied to the
  * destination buffer.
  * The destination buffer is always null terminated.
  *
  * \param the destination buffer. this must have an available capacity of at
  * least ``astr_length(dest) + n + 1`` characters.
  *
  * \param the source buffer.
  *
  * \param the number of characters in the source buffer to copy.
  *
  * \return the total number of characters in the destination buffer.
  *)#
export func astr_append_n : (dest : ^mut ascii, src : ^ascii, n : u) -> u
{
    let dest_walker : mut ^mut ascii = dest;
    let src_walker : mut ^ascii = src;
    let asciis_remaining : mut u = n;
    loop @dest_walker != '\0'a { dest_walker = dest_walker +^ 1u; }
    loop asciis_remaining != 0u && @src_walker != '\0'a
    {
        @dest_walker = @src_walker;
        src_walker = src_walker +^ 1u;
        dest_walker = dest_walker +^ 1u;
        asciis_remaining = asciis_remaining - 1u;
    }
    @dest_walker = '\0'a;
    return dest_walker -^^ dest;
}

#(**
  * \brief Copies all characters from the source astr to the end of the
  * destination astr.
  *
  * The destination buffer is always null terminated.
  *
  * \param the destination buffer. this must have an available capacity of at
  * least ``astr_length(dest) + astr_length(src) + 1`` characters.
  *
  * \param the source buffer.
  *
  * \param the number of characters to copy.
  *
  * \return the number of characters actually copied.
  *)#
export func astr_append : (dest : ^mut ascii, src : ^ascii) -> u
{
    let dest_walker : mut ^mut ascii = dest;
    let src_walker : mut ^ascii = src;
    loop @dest_walker != '\0'a { dest_walker = dest_walker +^ 1u; }
    loop @src_walker != '\0'a
    {
        @dest_walker = @src_walker;
        src_walker = src_walker +^ 1u;
        dest_walker = dest_walker +^ 1u;
    }
    @dest_walker = '\0'a;
    return dest_walker -^^ dest;
}

#(**
  * \brief Compare two astrs with case sensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return Exactly 0 if the astrs are lexically equivelant. Greater than 0 if
  * the lhs is lexically before the rhs, and less than 0 if the lhs is
  * lexically after the rhs.
  *)#
export func astr_compare : (lhs : ^ascii, rhs : ^ascii) -> s
{
    let lhs_walker : mut ^ascii = lhs;
    let rhs_walker : mut ^ascii = rhs;
    loop @lhs_walker != '\0'a && @lhs_walker == @rhs_walker
    {
        lhs_walker = lhs_walker +^ 1u;
        rhs_walker = rhs_walker +^ 1u;
    }
    return @lhs_walker as s - @rhs_walker as s;
}

#(**
  * \brief Tests if two astrs are exactly equal.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return true if they are equal. false otherwise.
  *)#
export func astr_eq : (lhs : ^ascii, rhs : ^ascii) -> bool
{
    return astr_compare(lhs, rhs) == 0s;
}

#(**
  * \brief Tests if two astrs are unequal with case sensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return true if they are unequal. false otherwise.
  *)#
export func astr_ne : (lhs : ^ascii, rhs : ^ascii) -> bool
{
    return astr_compare(lhs, rhs) != 0s;
}

#(**
  * \brief Compare two astrs with case insensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return Exactly 0 if the astrs are lexically equivelant. Greater than 0 if
  * the lhs is lexically before the rhs, and less than 0 if the lhs is
  * lexically after the rhs.
  *)#
export func astr_compare_icase : (lhs : ^ascii, rhs : ^ascii) -> s
{
    let lhs_walker : mut ^ascii = lhs;
    let rhs_walker : mut ^ascii = rhs;
    loop @lhs_walker != '\0'a
        && ascii_to_lower(@lhs_walker) == ascii_to_lower(@rhs_walker)
    {
        lhs_walker = lhs_walker +^ 1u;
        rhs_walker = rhs_walker +^ 1u;
    }
    return ascii_to_lower(@lhs_walker) as s - ascii_to_lower(@rhs_walker) as s;
}

#(**
  * \brief Tests if two astrs are equal with case insensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return true if they are equal. false otherwise.
  *)#
export func astr_eq_icase : (lhs : ^ascii, rhs : ^ascii) -> bool
{
    return astr_compare_icase(lhs, rhs) == 0s;
}

#(**
  * \brief Tests if two astrs are unequal with case insensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return true if they are unequal. false otherwise.
  *)#
export func astr_ne_icase : (lhs : ^ascii, rhs : ^ascii) -> bool
{
    return astr_compare_icase(lhs, rhs) != 0s;
}

#(**
  * \brief Compare two astrs up to n many characters with case sensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return Exactly 0 if the astrs are lexically equivelant. Greater than 0 if
  * the lhs is lexically before the rhs, and less than 0 if the lhs is
  * lexically after the rhs.
  *)#
export func astr_compare_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> s
{
    let lhs_walker : mut ^ascii = lhs;
    let rhs_walker : mut ^ascii = rhs;
    let chars_remaining : mut u = n - 1u;
    loop chars_remaining != 0u
        && @lhs_walker != '\0'a && @lhs_walker == @rhs_walker
    {
        lhs_walker = lhs_walker +^ 1u;
        rhs_walker = rhs_walker +^ 1u;
        chars_remaining = chars_remaining - 1u;
    }
    return @lhs_walker as s - @rhs_walker as s;
}

#(**
  * \brief Tests if two astrs, up to n many characters, are equal with case
  * sensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return true if they are equal. false otherwise.
  *)#
export func astr_eq_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool
{
    return astr_compare_n(lhs, rhs, n) == 0s;
}

#(**
  * \brief Tests if two astrs, up to n many characters, are unequal with case
  * sensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return true if they are unequal. false otherwise.
  *)#
export func astr_ne_n : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool
{
    return astr_compare_n(lhs, rhs, n) != 0s;
}

#(**
  * \brief Compare two astrs with case insensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return Exactly 0 if the astrs are lexically equivelant. Greater than 0 if
  * the lhs is lexically before the rhs, and less than 0 if the lhs is
  * lexically after the rhs.
  *)#
export func astr_compare_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> s
{
    let lhs_walker : mut ^ascii = lhs;
    let rhs_walker : mut ^ascii = rhs;
    let chars_remaining : mut u = n - 1u;
    loop chars_remaining != 0u && @lhs_walker != '\0'a
        && ascii_to_lower(@lhs_walker) == ascii_to_lower(@rhs_walker)
    {
        lhs_walker = lhs_walker +^ 1u;
        rhs_walker = rhs_walker +^ 1u;
        chars_remaining = chars_remaining - 1u;
    }
    return ascii_to_lower(@lhs_walker) as s - ascii_to_lower(@rhs_walker) as s;
}

#(**
  * \brief Tests if two astrs, up to n many characters, are equal with case
  * insensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return true if they are equal. false otherwise.
  *)#
export func astr_eq_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool
{
    return astr_compare_n_icase(lhs, rhs, n) == 0s;
}

#(**
  * \brief Tests if two astrs, up to n many characters, are unequal with case
  * insensitivity.
  *
  * \param the left astr of the comparison.
  *
  * \param the right astr of the comparison.
  *
  * \return true if they are unequal. false otherwise.
  *)#
export func astr_ne_n_icase : (lhs : ^ascii, rhs : ^ascii, n : u) -> bool
{
    return astr_compare_n_icase(lhs, rhs, n) != 0s;
}

#(**
  * \brief Test if the first astr has as a sub-astr the second.
  *
  * \param the potential super-astr.
  *
  * \param the potential sub-astr.
  *
  * \return true if the first contains the second.
  *)#
export func astr_contains : (lhs : ^ ascii, rhs : ^ ascii) -> bool
{
    let lhs_walker : mut ^ ascii = lhs;

    loop @lhs_walker != '\0'a
    {
        let lhsi_walker : mut ^ ascii = lhs_walker;
        let rhs_walker : mut ^ ascii = rhs;
        loop true
        {
            if @rhs_walker == '\0'a
            {
                return true;
            }
            elif @lhsi_walker == '\0'a
            {
                return false;
            }
            elif @lhsi_walker == @rhs_walker
            {
                lhsi_walker = lhsi_walker +^ 1u;
                rhs_walker = rhs_walker +^ 1u;
            }
            else
            {
                break;
            }
        }
        lhs_walker = lhs_walker +^ 1u;
    }
    return @rhs == '\0'a; # zero length lhs. rhs is either the same or not.
}

#(**
  * \brief Checks if there is a sub-string of the first astr which starts at
  * the 0th index, and is equal to the second astr.
  *
  * \param The astr which possibly starts with the other.
  *
  * \param the possible prefix of the previous astr.
  *
  * \return true if the first starts with the second.
  *)#
export func astr_starts_with : (lhs : ^ ascii, rhs : ^ ascii) -> bool
{
    let lhs_walker : mut ^ ascii = lhs;
    let rhs_walker : mut ^ ascii = rhs;

    loop true
    {
        if @rhs_walker == '\0'a { return true; }
        if @lhs_walker == '\0'a { return false; }
        if @lhs_walker == @rhs_walker
        {
            lhs_walker = lhs_walker +^ 1u;
            rhs_walker = rhs_walker +^ 1u;
        }
        else { break; }
    }
    return false;
}

#(**
  * \brief Checks if there is a sub-string of the first astr which ends at
  * the last index, and is equal to the second astr.
  *
  * \param The astr which possibly ends with the other.
  *
  * \param the possible suffix of the previous astr.
  *
  * \return true if the first ends with the second.
  *)#
export func astr_ends_with : (lhs : ^ ascii, rhs : ^ ascii) -> bool
{
    let llen : u = astr_length(lhs);
    let rlen : u = astr_length(rhs);

    if rlen == 0u { return true; }
    if llen < rlen { return false; }

    let lhs_walker : mut ^ ascii = lhs +^ (astr_length(lhs) - 1u);
    let rhs_walker : mut ^ ascii = rhs +^ (astr_length(rhs) - 1u);

    loop i : mut u in [0u, rlen)
    {
        if @lhs_walker != @rhs_walker { return false; }
        lhs_walker = lhs_walker -^ 1u;
        rhs_walker = rhs_walker -^ 1u;
    }
    return true;
}

#### THAR BE DRAGONS. The following code is currently untested.

export func astr_to_u8 : (str : ^ascii, to : ^mut u8, base : u8) -> u
{
    let u64_to : mut u64;
    let ret : u = astr_to_u64(str, ?u64_to, base);
    @to = u64_to as typeof(@to);
    return ret;
}

export func astr_to_u16 : (str : ^ascii, to : ^mut u16, base : u8) -> u
{
    let u64_to : mut u64;
    let ret : u = astr_to_u64(str, ?u64_to, base);
    @to = u64_to as typeof(@to);
    return ret;
}

export func astr_to_u32 : (str : ^ascii, to : ^mut u32, base : u8) -> u
{
    let u64_to : mut u64;
    let ret : u = astr_to_u64(str, ?u64_to, base);
    @to = u64_to as typeof(@to);
    return ret;
}

export func astr_to_u64 : (str : ^ascii, to : ^mut u64, base : u8) -> u
{
    let walker : mut ^ascii = str;
    let tmp : mut u64 = 0s as u64;

    let is_negative : bool = @walker == '-'a;
    if (is_negative){return 0u;}

    let digit : mut s = ascii_to_digit_b36(@walker);
    loop digit >= 0s && digit < base as typeof(digit)
    {
        tmp = tmp * base as typeof(tmp);
        tmp = tmp + digit as typeof(tmp);
        walker = walker +^ 1u;
        digit = ascii_to_digit_b36(@walker);
    }

    if (walker == str)
    {
        # No digits were parsed.
        @to = 0s as u64;
        return 0u;
    }
    @to = tmp;
    return walker -^^ str;
}

export func astr_to_u : (str : ^ascii, to : ^mut u, base : u8) -> u
{
    let u64_to : mut u64;
    let ret : u = astr_to_u64(str, ?u64_to, base);
    @to = u64_to as typeof(@to);
    return ret;
}

export func astr_to_s8 : (str : ^ascii, to : ^mut s8, base : u8) -> u
{
    let s64_to : mut s64;
    let ret : u = astr_to_s64(str, ?s64_to, base);
    @to = s64_to as typeof(@to);
    return ret;
}

export func astr_to_s16 : (str : ^ascii, to : ^mut s16, base : u8) -> u
{
    let s64_to : mut s64;
    let ret : u = astr_to_s64(str, ?s64_to, base);
    @to = s64_to as typeof(@to);
    return ret;
}

export func astr_to_s32 : (str : ^ascii, to : ^mut s32, base : u8) -> u
{
    let s64_to : mut s64;
    let ret : u = astr_to_s64(str, ?s64_to, base);
    @to = s64_to as typeof(@to);
    return ret;
}

export func astr_to_s64 : (str : ^ascii, to : ^mut s64, base : u8) -> u
{
    let walker : mut ^ascii = str;
    let tmp : mut s64 = 0s as s64;

    let is_negative : bool = @walker == '-'a;
    if (is_negative){walker = walker +^ 1u;}

    let digit : mut s = ascii_to_digit_b36(@walker);
    loop digit >= 0s && digit < base as typeof(digit)
    {
        tmp = tmp * base as typeof(tmp);
        tmp = tmp + digit as typeof(tmp);
        walker = walker +^ 1u;
        digit = ascii_to_digit_b36(@walker);
    }

    if (is_negative){tmp = tmp * -1s as s64;}
    if ((walker == str) || ((walker == str +^ 1u) && is_negative))
    {
        # No digits were parsed.
        @to = 0s as s64;
        return 0u;
    }
    @to = tmp;
    return walker -^^ str;
}

export func astr_to_s : (str : ^ascii, to : ^mut s, base : u8) -> u
{
    let s64_to : mut s64;
    let ret : u = astr_to_s64(str, ?s64_to, base);
    @to = s64_to as typeof(@to);
    return ret;
}

export func __libn_u_to_astr : (n : u, str : ^ mut ascii) -> u
{
    if n == 0u { return 0u; }
    let ret : u = __libn_u_to_astr(n / 10u, str);
    str[ret] = (u_mod(n, 10u) + '0'a as u) as ascii;
    return ret + 1u;
}

export func u_to_astr : (n : u, str : ^ mut ascii) -> u
{
    if n == 0u
    {
        @str = '0'a;
        @(str +^ 1u) = '\0'a;
        return 1u;
    }
    else
    {
        let ret : u = __libn_u_to_astr(n, str);
        @(str +^ ret) = '\0'a;
        return ret;
    }
}

export func s_to_astr : (n : s, str : ^ mut ascii) -> u
{
    if n < 0s
    {
        @str = '-'a;
        let ret : u = 1u + __libn_u_to_astr(-n as u, str +^ 1u);
        @(str +^ ret) = '\0'a;
        return ret;
    }
    if n == 0s
    {
        @str = '0'a;
        @(str +^ 1u) = '\0'a;
        return 1u;
    }
    else
    {
        let ret : u = __libn_u_to_astr(n as u, str);
        @(str +^ ret) = '\0'a;
        return ret;
    }
}
