# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import "std/astr.n"a;
import "std/math.n"a;

import "cstd/primitives.n"a;
import "posix/sys/socket.n"a;
import "posix/netinet/in.n"a;
import "posix/unistd.n"a;

export introduce variant inet_address;

#(**
  * \brief Parses an inet address in 'numbers and dots' form from an astr.
  *
  * \param this inet_address.
  *
  * \param the astr to parse from.
  *
  * \return the number of characters which were parsed. 0 if this was not
  * a valid inet address.
  *)#
export introduce func inet_address_from_astr :
    (this : ^ mut inet_address, astr : ^ ascii) -> u;

#(**
  * \brief Write an inet address into an astr in 'numbers and dots' form.
  *
  * \param this inet_address.
  *
  * \param the astr to parse from.
  *
  * \return the number of characters which were written. 0 if there was not
  * enough space.
  *)#
export introduce func inet_address_to_astr : 
    (this : ^ inet_address, astr : mut ^ mut ascii, len : u) -> u;

#(**
  * \brief initialize this inet_address using the localhost address.
  *
  * \param this inet_address.
  *)#
export introduce func inet_address_init_localhost:
    (this : ^ mut inet_address) -> void;

export variant inet_address
{
    func from_astr = inet_address_from_astr;
    func to_astr = inet_address_to_astr;
    func init_localhost = inet_address_init_localhost;
    v4
    {
        let addr : [4u] mut u8;
    }

    # TODO: IPv6
}

export introduce variant inet_socket;

#(**
  * \brief make an outgoing TCP connection.
  *
  * \param this inet_socket
  *
  * \param the remote address to which to connect.
  *
  * \param the remote port to which to connect.
  *
  * \return true on success.
  *)#
export introduce func inet_socket_tcp_connect :
    (this : ^ mut inet_socket, addr : ^ inet_address, port : u16) -> bool;

#(**
  * \brief Read a number of bytes off a TCP socket.
  *
  * \param this inet_socket.
  *
  * \param the buffer to write into.
  *
  * \param the number of bytes to read.
  *
  * \return the number of bytes actually read. -1 if there was an error.
  *)#
export introduce func inet_socket_tcp_read :
    (this : ^ mut inet_socket, buf : ^ mut u8, max : u) -> s;

#(**
  * \brief write a number of bytes into a TCP socket.
  *
  * \param this inet socket.
  *
  * \param the buffer to write out.
  *
  * \param the number of bytes to write.
  *
  * \return the number of bytes actually written. -1 if there was an error.
  *)#
export introduce func inet_socket_tcp_write :
    (this : ^ inet_socket, buf : ^ u8, num : u) -> s;

#(**
  * \brief Opens a UDP socket on the given port number.
  *
  * \param this inet_socket.
  *
  * \param the local port number to open.
  *
  * \return true on success.
  *)#
export introduce func inet_socket_udp_open :
    (this : ^ mut inet_socket, lport : u16) -> bool;

#(**
  * \brief read from the UDP socket, and record the remote address and port
  * into this object's remote and remote_port fields.
  *
  * \param this inet_socket.
  *
  * \param the buffer to read into.
  *
  * \param the maximum number of bytes to read.
  *
  * \return the number of bytes actually read. -1 on failure.
  *)#
export introduce func inet_socket_udp_read :
    (this : ^ mut inet_socket, buf : ^ mut u8, max : u) -> s;

#(**
  * \brief write into the UDP socket, to the destination given by this
  * object's remote and remote port fields.
  *
  * \param this inet_socket.
  *
  * \param the buffer to write out.
  *
  * \param the number of bytes to write.
  *
  * \return the number of bytes actually written. -1 on failure.
  *)#
export introduce func inet_socket_udp_write :
    (this : ^ inet_socket, buf : ^ u8, num : u) -> s;

#(**
  * \brief Close this inet_socket.
  *
  * \param this inet_socket.
  *
  * \return true on success.
  *)#
export introduce func inet_socket_close : (this : ^ inet_socket) -> bool;

export variant inet_socket
{
    let remote : mut inet_address;
    let remote_port : mut u16;
    let fd : mut sint;

    iface read  : (^ mut inet_socket, ^ mut u8, u) -> s;
    iface write : (^ inet_socket, ^ u8, u) -> s;

    func close = inet_socket_close;

    tcp
    {
        func connect = inet_socket_tcp_connect;
        func read    = inet_socket_tcp_read;
        func write   = inet_socket_tcp_write;
    }

    udp
    {
        func open  = inet_socket_udp_open;
        func read  = inet_socket_udp_read;
        func write = inet_socket_udp_write;
    }
}

export introduce struct server_socket;

#(**
  * \brief Open a server socket, which accepts new connections on a given TCP
  * port.
  *
  * \param this server_socket.
  *
  * \param the port to listen on.
  *
  * \return true on success.
  *)#
export introduce func server_socket_open :
    (this : ^ mut server_socket, lport : u16) -> bool;

#(**
  * \brief Accept a new connection from this server socket.
  *
  * \param this server_socket.
  *
  * \param return by pointer, the new connection as an inet_socket.
  *
  * \return true on success.
  *)#
export introduce func server_socket_accept :
    (this : ^ server_socket, socket : ^ mut inet_socket) -> bool;

#(**
  * \brief close this server socket.
  *
  * \param this server socket.
  *
  * \return true on success.
  *)#
export introduce func server_socket_close:
    (this : ^ mut server_socket) -> bool;

export struct server_socket
{
    let fd : mut sint;
    func open   = server_socket_open;
    func accept = server_socket_accept;
    func close  = server_socket_close;
}

################################################################################
# INTERNAL                                                                     #
################################################################################

export func inet_address_from_astr :
    (this : ^ mut inet_address, astr : ^ ascii) -> u
{
    let nread : mut u = 0u;
    this.tag = inet_address.v4; # TODO handle v6
    nread = nread + astr_to_u8(astr, ?this.v4.addr[0u], 10u8);
    if astr[nread] == '.'a
    {
        nread = nread + 1u;
    }
    else
    {
        return 0u;
    }
    nread = nread + astr_to_u8(astr +^ nread, ?this.v4.addr[1u], 10u8);
    if astr[nread] == '.'a
    {
        nread = nread + 1u;
    }
    else
    {
        return 0u;
    }
    nread = nread + astr_to_u8(astr +^ nread, ?this.v4.addr[2u], 10u8);
    if astr[nread] == '.'a
    {
        nread = nread + 1u;
    }
    else
    {
        return 0u;
    }
    nread = nread + astr_to_u8(astr +^ nread, ?this.v4.addr[3u], 10u8);

    return nread;
}

# TODO: remove in favor of std/integer.n:u8_to_astr when merged.
func u8_to_astr_inet : (this : mut u8, buf : mut ^ mut ascii, len : u) -> u
{
    if this != 0u8 && len != 0u
    {
        let ret : u = u8_to_astr_inet(this / 10u8, buf, len - 1u);
        if ret == 1u && @buf == '0'a
        {
            buf[0u] = (u8_rem(this, 10u8) + ('0'a as u8)) as ascii;
            return 1u;
        }
        else
        {
            buf[ret] = (u8_rem(this, 10u8) + ('0'a as u8)) as ascii;
            return ret + 1u;
        }
    }
    else
    {
        buf[0u] = '0'a;
        return 1u;
    }
}

export func inet_address_to_astr :
    (this : ^ inet_address, astr : mut ^ mut ascii, len : u) -> u
{
    if len < 15u # 4*3digits + 3*1dots 
    {
        return 0u; # too short.
    }

    let nwrite : mut u = 0u;
    nwrite = u8_to_astr_inet(this.v4.addr[0u], astr, len);
    astr[nwrite] = '.'a;
    nwrite = nwrite + 1u;
    nwrite = nwrite +
        u8_to_astr_inet(this.v4.addr[1u], astr +^ nwrite, len - nwrite);
    astr[nwrite] = '.'a;
    nwrite = nwrite + 1u;
    nwrite = nwrite +
        u8_to_astr_inet(this.v4.addr[2u], astr +^ nwrite, len - nwrite);
    astr[nwrite] = '.'a;
    nwrite = nwrite + 1u;
    nwrite = nwrite +
        u8_to_astr_inet(this.v4.addr[3u], astr +^ nwrite, len - nwrite);
    astr[nwrite] = '\0'a;
    return nwrite;
}
export func inet_address_init_localhost : (this : ^ mut inet_address) -> void
{
    this.tag = inet_address.v4;
    this.v4.addr[0u] = 127u8;
    this.v4.addr[1u] =   0u8;
    this.v4.addr[2u] =   0u8;
    this.v4.addr[3u] =   1u8;
}

func port_host_to_net : (port : u16) -> u16
{
    return ((port >> 8u) & 0x00FFu16) | ((port << 8u) & 0xFF00u16);
}

func inet_address_to_in_addr_t : (addr : ^ inet_address) -> in_addr_t
{
    return (((addr.v4.addr[0u] as u32) <<  0u) & 0x000000FFu32) |
           (((addr.v4.addr[1u] as u32) <<  8u) & 0x0000FF00u32) |
           (((addr.v4.addr[2u] as u32) << 16u) & 0x00FF0000u32) |
           (((addr.v4.addr[3u] as u32) << 24u) & 0xFF000000u32);
}

export func inet_socket_tcp_connect :
    (this : ^ mut inet_socket, remote : ^ inet_address, rport : u16) -> bool
{
    let sin : mut sockaddr_in;
    sin.sin_family = AF_INET as sa_family_t;
    sin.sin_port = port_host_to_net(rport);
    sin.sin_addr.s_addr = inet_address_to_in_addr_t(remote);

    this.tag = inet_socket.tcp;
    this.remote = @remote;
    this.remote_port = rport;

    this.fd = socket(AF_INET, SOCK_STREAM as sint, IPPROTO_TCP);
    if this.fd < 0s as sint { return false; }

    return connect(this.fd, ?sin as ^ sockaddr, sizeof(sin) as socklen_t)
        == (0s as sint);
}

export func inet_socket_tcp_read :
    (this : ^ mut inet_socket, buf : ^ mut u8, max : u) -> s
{
    return read(this.fd, buf, max) as s;
}

export func inet_socket_tcp_write :
    (this : ^ inet_socket, buf : ^ u8, num : u) -> s
{
    return write(this.fd, buf, num) as s;
}

func u32_to_in_addr_t : (addr : u32) -> in_addr_t
{
    return (((addr << 24u) & 0xFF000000u32) |
            ((addr <<  8u) & 0x00FF0000u32) |
            ((addr >>  8u) & 0x0000FF00u32) |
            ((addr >> 24u) & 0x000000FFu32)) as in_addr_t;
}

export func inet_socket_udp_open :
    (this : ^ mut inet_socket, lport : u16) -> bool
{
    let sin : mut sockaddr_in;
    sin.sin_family = AF_INET as sa_family_t;
    sin.sin_port = port_host_to_net(lport);
    sin.sin_addr.s_addr = u32_to_in_addr_t(INADDR_ANY);

    this.tag = inet_socket.udp;

    this.fd = socket(AF_INET, SOCK_DGRAM as sint, IPPROTO_UDP);
    if this.fd < 0s as sint { return false; }

    return bind(this.fd, ?sin as ^ mut sockaddr, sizeof(sin) as socklen_t)
        == (0s as sint);
}

func port_net_to_host : (port : u16) -> u16
{
    return ((port >> 8u) & 0x00FFu16) | ((port << 8u) & 0xFF00u16);
}

func in_addr_t_to_inet_address : (a : in_addr_t, b : ^ mut inet_address) -> void
{
    b.v4.addr[0u] = ((a as u32 & 0x000000FFu32) >>  0u) as u8;
    b.v4.addr[1u] = ((a as u32 & 0x0000FF00u32) >>  8u) as u8;
    b.v4.addr[2u] = ((a as u32 & 0x00FF0000u32) >> 16u) as u8;
    b.v4.addr[3u] = ((a as u32 & 0xFF000000u32) >> 24u) as u8;
}

export func inet_socket_udp_read :
    (this : ^ mut inet_socket, buf : ^ mut u8, max : u) -> s
{
    let sin : mut sockaddr_in;
    let sin_len : mut socklen_t = sizeof(sin) as socklen_t;
    let len : s = recvfrom(this.fd, buf, max as size_t, 0s as sint,
                           ?sin as ^ mut sockaddr, ?sin_len) as s;
    in_addr_t_to_inet_address(sin.sin_addr.s_addr, ?this.remote);
    this.remote_port = port_net_to_host(sin.sin_port);
    return len;
}

export func inet_socket_udp_write:
    (this : ^ inet_socket, buf : ^ u8, num : u) -> s
{
    # TODO: send_to using ip and port
    let sin : mut sockaddr_in;
    sin.sin_family = AF_INET as sa_family_t;
    sin.sin_port = port_host_to_net(this.remote_port);
    sin.sin_addr.s_addr = inet_address_to_in_addr_t(?this.remote);

    return sendto(this.fd, buf, num, 0s as sint, ?sin as ^ sockaddr,
                  sizeof(sin) as socklen_t) as s;
}

export func inet_socket_close : (this : ^ inet_socket) -> bool
{
    if shutdown(this.fd, SHUT_RDWR) == (-1s as sint)
    {
        close(this.fd);
        return false;
    }
    if close(this.fd) == (-1s as sint)
    {
        return false;
    }
    return true;
}

let LISTEN_BACKLOG : sint = 128s as sint;

export func server_socket_open :
    (this : ^ mut server_socket, lport : u16) -> bool
{
    let sin : mut sockaddr_in;
    sin.sin_family = AF_INET as sa_family_t;
    sin.sin_port = port_host_to_net(lport);
    sin.sin_addr.s_addr = u32_to_in_addr_t(INADDR_ANY);

    this.fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if this.fd < 0s as sint { return false; }

    if bind(this.fd, ?sin as ^ mut sockaddr, sizeof(sin) as socklen_t)
        == 0s as sint
    {
        return listen(this.fd, LISTEN_BACKLOG) == 0s as sint;
    }
    else
    {
        return false;
    }
}

export func server_socket_accept :
    (this : ^ server_socket, socket: ^ mut inet_socket) -> bool
{
    let sin : mut sockaddr_in;
    let len : mut socklen_t = sizeof(sin) as socklen_t;
    socket.fd = accept(this.fd, ?sin as ^ mut sockaddr, ?len);
    if socket.fd < 0s as sint
    {
        return false;
    }
    in_addr_t_to_inet_address(sin.sin_addr.s_addr, ?socket.remote);
    socket.remote_port = port_net_to_host(sin.sin_port);
    socket.tag = inet_socket.tcp;

    return true;
}

export func server_socket_close : (this : ^ mut server_socket) -> bool
{
    return close(this.fd) >= 0s as sint;
}
