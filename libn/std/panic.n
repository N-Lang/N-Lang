# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/stdlib.n"a;

import "posix/unistd.n"a;

# Signal that an unrecoverable error has occurred.
# The message `msg` will be written to the standard error stream followed by
# program abortion.
# This function does not return.
export introduce func panic : (msg : ^ascii) -> void;

export let PANIC_INVALID_ARGUMENT : ^ascii =
    $"Invalid argument."a;
export let PANIC_INVALID_INDEX : ^ascii =
    $"Invalid index."a;
export let PANIC_INVALID_INIT : ^ascii =
    $"Invalid init call."a;
export let PANIC_INVALID_FINI : ^ascii =
    $"Invalid fini call."a;
export let PANIC_UNEXPECTED_CONTROL_FLOW : ^ascii =
    $"Unexpected control flow."a;

################################################################################
###                                 INTERNAL                                 ###
################################################################################

let PANIC_BASE_STR : [8u]ascii = "panic: "a;

export func panic : (msg : ^ascii) -> void
{
    if msg != null
    {
        let len : mut u = 0u;
        let walker : mut ^ ascii = msg;
        loop @walker != '\0'a { len = len + 1u; walker = walker +^ 1u; }

        write(STDERR_FILENO, $PANIC_BASE_STR as ^mut void, countof(PANIC_BASE_STR) - 1u);
        write(STDERR_FILENO, msg, len);
        let newline : ascii = '\n'a;
        write(STDERR_FILENO, ?newline, 1u);
    }
    abort();
}
