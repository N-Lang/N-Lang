# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/stdlib.n"a;
import "std/panic.n"a;

# Default allocation function used by the N standard library.
export introduce func default_allocate : (size : u) -> ^mut void;
# Default reallocation function used by the N standard library.
export introduce func default_reallocate : (mem : ^mut void, size : u) -> ^mut void;
# Default deallocation function used by the N standard library.
export introduce func default_deallocate : (mem : ^mut void) -> void;

# Memory management functions used by `allocate`, `reallocate`, and
# `deallocate`. These functions are designed to be kept with their default
# values or assigned once at program startup. The behavior of these functions
# should adhere to the behavior contract indicated in the function descriptions.
#
# NOTE: Libraries should *never* assign to these variables.
#{

# Returns a pointer to N contiguous bytes of valid memory on success where N is
# the provided parameter of type `u`.
#
# Returns `null` on failure.
export let allocate_func : mut (u) -> ^mut void = default_allocate;
# Returns a pointer to N contiguous bytes of valid memory on success where N is
# the provided parameter of type `u`. The memory region pointed to by the
# provided parameter of type `^mut void` is copied into the front of the new
# memory region, truncating if the new memory region has size smaller than that
# of the provided memory region.
#
# Returns `null` on failure.
export let reallocate_func : mut (^mut void, u) -> ^mut void = default_reallocate;
# releases memory allocated by `allocate_func` or `reallocate_func` back to the
# underlying system.
export let deallocate_func : mut (^mut void) -> void = default_deallocate;

#}

# Global allocation function for the N standard library.
#
# Returns a pointer to `size` contiguous bytes allocated by `allocate_func` on
# success.
#
# Panics on failure.
#
# NOTE: Assume the memory returned by `allocate` is uninitialized.
# NOTE: This function panics if `allocate_func` fails to return valid memory,
# thus memory returned from `allocate` can always assumed to be valid.
export introduce func allocate : (size : u) -> ^mut void;
# Global reallocation function for the N standard library.
#
# Returns a pointer to `size` contiguous bytes allocated by `reallocate_func` on
# success. Bytes from the provided `mem` pointer are copied into the front of
# of the new memory region, truncating if `size` is smaller than the size of
# the previous memory region.
#
# Panics on failure.
#
# NOTE: Assume additional memory at the back of the newly provided buffer
# returned by `reallocate` is uninitialized in the case that `size` is larger
# than the size of the previous memory region.
# NOTE: This function panics if `reallocate_func` fails to return valid memory,
# thus memory returned from `reallocate` can always assumed to be valid.
export introduce func reallocate : (mem : ^mut void, size : u) -> ^mut void;
# Global deallocation function for the N standard library. This function
# releases memory allocated by `allocate` or `reallocate` back to the underlying
# system.
#
# NOTE: Dereferencing a pointer provided to `deallocate` after deallocation has
# occurred will result in undefined behavior.
export introduce func deallocate : (mem : ^mut void) -> void;

# Copy `nbytes` bytes of non-overlapping memory from `src` to `dest`.
export introduce func memory_copy : (dest : ^mut void, src : ^void, nbytes : u) -> void;

# Copy `nbytes` bytes of overlapping or non-overlapping memory from `src` to
# `dest`.
export introduce func memory_copy_overlapping : (
    dest : ^mut void,
    src : ^void,
    nbytes : u
) -> void;

# Fill `nbytes` bytes of memory starting at `mem` with `value`.
export introduce func memory_fill : (mem : ^mut void, nbytes : u, value : u8) -> void;

# Swap the first `nbytes` bytes of memory regions `mem1` and `mem2` in place.
export introduce func memory_swap : (mem1 : ^mut void, mem2 : ^void, nbytes : u) -> void;

################################################################################
###                                 INTERNAL                                 ###
################################################################################

let PANIC_ALLOCATE_FAILURE : ^ascii = $"allocate failure"a;
let PANIC_REALLOCATE_FAILURE : ^ascii = $"reallocate failure"a;

export func default_allocate : (size : u) -> ^mut void
{
    return malloc(size);
}

export func default_reallocate : (mem : ^mut void, size : u) -> ^mut void
{
    return realloc(mem, size);
}

export func default_deallocate : (mem : ^mut void) -> void
{
    free(mem);
}

export func allocate : (size : u) -> ^mut void
{
    let new_mem : ^mut void = allocate_func(size);
    if new_mem == null && size != 0u
    {
        panic(PANIC_ALLOCATE_FAILURE);
    }
    return new_mem;
}

export func reallocate : (mem : ^mut void, size : u) -> ^mut void
{
    let new_mem : ^mut void = reallocate_func(mem, size);
    if new_mem == null && size != 0u
    {
        panic(PANIC_REALLOCATE_FAILURE);
    }
    return new_mem;
}

export func deallocate : (mem : ^mut void) -> void
{
    deallocate_func(mem);
}

export func memory_copy : (dest : ^mut void, src : ^void, nbytes : u) -> void
{
    let dest_byte : mut ^mut u8 = dest as ^mut u8;
    let src_byte : mut ^u8 = src as ^u8;
    let bytes_remaining : mut u = nbytes;
    loop bytes_remaining != 0u
    {
        @dest_byte = @src_byte;
        dest_byte = dest_byte +^ 1u;
        src_byte = src_byte +^ 1u;
        bytes_remaining = bytes_remaining - 1u;
    }
}

export func memory_copy_overlapping : (
    dest : ^mut void,
    src : ^void,
    nbytes : u
) -> void
{
    let tmp : ^mut void = allocate(nbytes);
    defer{deallocate(tmp);}
    memory_copy(tmp, src, nbytes);
    memory_copy(dest, tmp, nbytes);
}

export func memory_fill : (mem : ^mut void, nbytes : u, value : u8) -> void
{
    let byte : mut ^mut u8 = mem as ^mut u8;
    let countdown : mut u = nbytes;
    loop countdown != 0u
    {
        @byte = value;
        byte = byte +^ 1u;
        countdown = countdown - 1u;
    }
}

export func memory_swap : (mem1 : ^mut void, mem2 : ^void, nbytes : u) -> void
{
    let mem1_byte : mut ^mut u8 = mem1 as ^mut u8;
    let mem2_byte : mut ^mut u8 = mem2 as ^mut u8;
    let tmp_byte : mut ^mut u8;
    let countdown : mut u = nbytes;
    loop countdown != 0u
    {
        @tmp_byte = @mem1_byte;
        @mem1_byte = @mem2_byte;
        @mem2_byte = @tmp_byte;
        mem1_byte = mem1_byte +^ 1u;
        mem2_byte = mem2_byte +^ 1u;
        countdown = countdown - 1u;
    }
}
