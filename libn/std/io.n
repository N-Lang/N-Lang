# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/stdio.n"a;
import "posix/unistd.n"a;
import "std/memory.n"a;
import "std/astring.n"a;
import "std/math.n"a;

# Print the provided str to stdout.
# Returns a non-negative integer on success.
# Returns -1s on failure.
export introduce func print  : (cstr : ^ascii) -> s;
# Print the provided str to stdout followed by a newline character.
# Returns a non-negative integer on success.
# Returns -1s on failure.
export introduce func println : (cstr : ^ascii) -> s;
# Print the provided character to stdout.
# Returns a non-negative integer on success.
# Returns -1s on failure.
export introduce func printch : (ch : ascii) -> s;

#(**
  * Print an unsigned integer on standard output.
  *
  * \param the number
  *
  * \return a nonnegative integer on success on -1s on failure.
  *)#
export introduce func print_u : (n : u) -> s;

#(**
  * Print a signed integer on stadard output.
  *
  * \param the number
  * 
  * \return a nonnegative integer on success or -1s on failure.
  *)#
export introduce func print_s : (n : s) -> s;

# Print the provided str to stderr.
# Returns a non-negative integer on success.
# Returns -1s on failure.
export introduce func eprint : (cstr : ^ascii) -> s;
# Print the provided str to stderr followed by a newline character.
# Returns a non-negative integer on success.
# Returns -1 on failure.
export introduce func eprintln : (cstr : ^ascii) -> s;
# Print the provided character to stderr.
# Returns a non-negative integer on success.
# Returns -1s on failure.
export introduce func eprintch : (ch : ascii) -> s;

#(**
  * Print an unsigned integer on standard error.
  *
  * \param the number
  * 
  * \return a nonnegative integer on success or -1s on failure.
  *)#
export introduce func eprint_u : (n : u) -> s;

#(**
  * Print a signed integer on standard error.
  *
  * \param the number
  *
  * \return a nonnegative integer on success or -1s on failure.
  *)#
export introduce func eprint_s : (n : s) -> s;


# Get the size of a file specified by `path`.
# Returns the non-negative size of the file on success.
# Returns -1s on failure.
export introduce func file_size : (path : ^ascii) -> s;

# Read a file into the provided astring.
# Returns the number of bytes read on success.
# Returns -1s on failure.
export introduce func read_file_into_astring : (path : ^ascii, str : ^mut astring) -> s;

################################################################################
###                                 INTERNAL                                 ###
################################################################################

export func print  : (cstr : ^ascii) -> s
{
    let len : u = astr_length(cstr);
    if -1s == write(STDOUT_FILENO, cstr, len)
    {
        return -1s;
    }
    return 0s;
}

export func println : (cstr : ^ascii) -> s
{
    if -1s == print(cstr)
    {
        return -1s;
    }
    if -1s == write(STDOUT_FILENO, $"\n"a, 1u)
    {
        return -1s;
    }
    return 0s;
}

export func printch : (ch : ascii) -> s
{
    if -1s == write(STDOUT_FILENO, ?ch, 1u)
    {
        return -1s;
    }
    return 0s;
}

func __libn_print_u : (num : u) -> s
{
    if num != 0u
    {
        if __libn_print_u(num / 10u) != -1s
        {
            return printch(('0'a as u + u_mod(num, 10u)) as ascii);
        }
        else { return -1s; }
    }
    else { return 0s; }
}

export func print_u : (num : u) -> s
{
    if num == 0u { return printch('0'a); }
    else { return __libn_print_u(num); }
}

export func print_s : (num : s) -> s
{
    if num < 0s
    {
        printch('-'a);
        print_u(-num as u);
    }
    else
    {
        print_u(num as u);
    }
}

export func eprint : (cstr : ^ascii) -> s
{
    let len : u = astr_length(cstr);
    if -1s == write(STDERR_FILENO, cstr, len)
    {
        return -1s;
    }
    return 0s;
}

export func eprintln : (cstr : ^ascii) -> s
{
    if -1s == eprint(cstr)
    {
        return -1s;
    }
    if -1s == write(STDERR_FILENO, $"\n"a, 1u)
    {
        return -1s;
    }
    return 0s;
}

export func eprintch : (ch : ascii) -> s
{
    if -1s == write(STDERR_FILENO, ?ch, 1u)
    {
        return -1s;
    }
    return 0s;
}

func __libn_eprint_u : (num : u) -> s
{
    if num != 0u
    {
        if __libn_eprint_u(num / 10u) != -1s
        {
            return eprintch(('0'a as u + u_mod(num, 10u)) as ascii);
        }
        else { return -1s; }
    }
    else { return 0s; }
}

export func eprint_u : (num : u) -> s
{
    if num == 0u { return eprintch('0'a); }
    else { return __libn_eprint_u(num); }
}

export func eprint_s : (num : s) -> s
{
    if num < 0s
    {
        eprintch('-'a);
        eprint_u(-num as u);
    }
    else
    {
        eprint_u(num as u);
    }
}

export func file_size : (path : ^ascii) -> s
{
    # In the future it would be nice if we could use `stat` here and just grab
    # the size of the file off of the`.st_size` member from the `struct stat`.
    # Until then we'll have to use the old fseek ftell trick.
    let fp : ^mut FILE = fopen(path, $"rb"a);
    if fp == null
    {
        return -1s;
    }
    defer{fclose(fp);}
    if 0s as sint != fseek(fp, 0s as slong, SEEK_END)
    {
        return -1s;
    }
    return ftell(fp) as s;
}

export func read_file_into_astring : (path : ^ascii, str : ^mut astring) -> s
{
    let fsize : s = file_size(path);
    if fsize == -1s{return -1s;}

    let fp : ^mut FILE = fopen(path, $"rb"a);
    if fp == null
    {
        return -1s;
    }
    defer{fclose(fp);}

    str.reserve(fsize as u);
    let numread : s = fread(str._data, sizeof(ascii) as size_t, fsize as size_t, fp) as s;
    str._length = numread as u;
    if numread != fsize
    {
        str.clear();
        return -1s;
    }

    return fsize;
}

