#ifndef __N_CORE__
#define __N_CORE__

#include <stdint.h>

typedef uint8_t   n_u8;
typedef uint16_t  n_u16;
typedef uint32_t  n_u32;
typedef uint64_t  n_u64;
typedef uintptr_t n_u;
typedef int8_t    n_s8;
typedef int16_t   n_s16;
typedef int32_t   n_s32;
typedef int64_t   n_s64;
typedef intptr_t  n_s;

typedef float  n_f32;
typedef double n_f64;

typedef _Bool n_bool;
#define n_true  1
#define n_false 0

typedef char n_ascii;

#define n_null ((void *)0)

#endif // __N_CORE__
