import "std/testing.n"a;
import "std/astr.n"a;

func check_len : (tm : ^ mut test_manager, str : ^ ascii, len : u) -> void
{
    if astr_length(str) != len
    {
        tm.fail_astr($"unexpected astr length"a);
    }
}

func check_eq : (tm : ^ mut test_manager, actual : ^ ascii, expected : ^ ascii)
    -> void
{
    if astr_compare(actual, expected) != 0s
    {
        tm.fail_astr($"actual astr does not match expected."a);
    }
}

func check : (tm : ^ mut test_manager, expr : bool) -> void
{
    if !expr
    {
        tm.fail_astr($"failed boolean check"a);
    }
}

export func test_astr_length : (tm : ^ mut test_manager) -> void
{
    check_len(tm, $""a, 0u);
    check_len(tm, $"foo"a, 3u);
    check_len(tm, $"foo goo"a, 7u);
}

export func test_astr_copy_n : (tm : ^ mut test_manager) -> void
{
    let buf : mut ^ mut ascii = allocate(11u); defer { deallocate(buf); }
    astr_copy_n(buf, $""a, 0u);
    check_len(tm, buf, 0u);
    check_eq(tm, buf, $""a);
    astr_copy_n(buf, $""a, 5u);
    check_len(tm, buf, 0u);
    check_eq(tm, buf, $""a);

    astr_copy_n(buf, $"abc"a, 3u);
    check_len(tm, buf, 3u);
    check_eq(tm, buf, $"abc"a);

    astr_copy_n(buf, $"1234567890"a, 10u);
    check_len(tm, buf, 10u);
    check_eq(tm, buf, $"1234567890"a);
}

export func test_astr_copy : (tm : ^ mut test_manager) -> void
{
    let buf : mut ^ mut ascii = allocate(11u); defer { deallocate(buf); }
    astr_copy(buf, $""a);
    check_len(tm, buf, 0u);
    check_eq(tm, buf, $""a);

    astr_copy(buf, $""a);
    check_len(tm, buf, 0u);
    check_eq(tm, buf, $""a);

    astr_copy(buf, $"abc"a);
    check_len(tm, buf, 3u);
    check_eq(tm, buf, $"abc"a);

    astr_copy(buf, $"1234567890"a);
    check_len(tm, buf, 10u);
    check_eq(tm, buf, $"1234567890"a);
}

export func test_astr_append_n : (tm : ^ mut test_manager) -> void
{
    let buf : mut ^ mut ascii = allocate(14u); defer { deallocate(buf); }
    @buf = '\0'a;
    astr_append_n(buf, $""a, 0u);
    check_len(tm, buf, 0u);
    check_eq(tm, buf, $""a);

    astr_append_n(buf, $""a, 5u);
    check_len(tm, buf, 0u);
    check_eq(tm, buf, $""a);

    astr_append_n(buf, $"abc"a, 3u);
    check_len(tm, buf, 3u);
    check_eq(tm, buf, $"abc"a);

    astr_append_n(buf, $"1234567890"a, 10u);
    check_len(tm, buf, 13u);
    check_eq(tm, buf, $"abc1234567890"a);
}

export func test_astr_append : (tm : ^ mut test_manager) -> void
{
    let buf : mut ^ mut ascii = allocate(14u); defer { deallocate(buf); }
    @buf = '\0'a;
    astr_append(buf, $""a);
    check_len(tm, buf, 0u);
    check_eq(tm, buf, $""a);

    astr_append(buf, $""a);
    check_len(tm, buf, 0u);
    check_eq(tm, buf, $""a);

    astr_append(buf, $"abc"a);
    check_len(tm, buf, 3u);
    check_eq(tm, buf, $"abc"a);

    astr_append(buf, $"1234567890"a);
    check_len(tm, buf, 13u);
    check_eq(tm, buf, $"abc1234567890"a);
}

export func test_astr_compare : (tm : ^ mut test_manager) -> void
{
    check(tm, astr_compare($""a, $""a) == 0s);
    check(tm, astr_compare($"abc"a, $""a) > 0s);
    check(tm, astr_compare($""a, $"abc"a) < 0s);
    check(tm, astr_compare($"abc"a, $"a"a) > 0s);
    check(tm, astr_compare($"a"a, $"abc"a) < 0s);
    check(tm, astr_compare($"abc"a, $"ab"a) > 0s);
    check(tm, astr_compare($"ab"a, $"abc"a) < 0s);
    check(tm, astr_compare($"abc"a, $"abc"a) == 0s);
    check(tm, astr_compare($"bbc"a, $"abc"a) > 0s);
    check(tm, astr_compare($"abc"a, $"bbc"a) < 0s);
    check(tm, astr_compare($"acc"a, $"abc"a) > 0s);
    check(tm, astr_compare($"abc"a, $"acc"a) < 0s);
    check(tm, astr_compare($"ABC"a, $"abc"a) < 0s);
    check(tm, astr_compare($"abc"a, $"ABC"a) > 0s);
}

export func test_astr_eq : (tm : ^ mut test_manager) -> void
{
    check(tm, astr_eq($""a, $""a));
    check(tm, !astr_eq($""a, $"abc"a));
    check(tm, astr_eq($"abc"a, $"abc"a));
    check(tm, !astr_eq($"acc"a, $"abc"a));
    check(tm, !astr_eq($"abc"a, $"acc"a));
    check(tm, !astr_eq($"ABC"a, $"abc"a));
    check(tm, !astr_eq($"abc"a, $"ABC"a));
}

export func test_astr_ne : (tm : ^ mut test_manager) -> void
{
    check(tm, !astr_ne($""a, $""a));
    check(tm, astr_ne($""a, $"abc"a));
    check(tm, !astr_ne($"abc"a, $"abc"a));
    check(tm, astr_ne($"acc"a, $"abc"a));
    check(tm, astr_ne($"abc"a, $"acc"a));
}

export func test_astr_compare_icase : (tm : ^ mut test_manager) -> void
{
    check(tm, astr_compare_icase($""a, $""a) == 0s);
    check(tm, astr_compare_icase($"abc"a, $""a) > 0s);
    check(tm, astr_compare_icase($""a, $"abc"a) < 0s);
    check(tm, astr_compare_icase($"abc"a, $"a"a) > 0s);
    check(tm, astr_compare_icase($"a"a, $"abc"a) < 0s);
    check(tm, astr_compare_icase($"abc"a, $"ab"a) > 0s);
    check(tm, astr_compare_icase($"ab"a, $"abc"a) < 0s);
    check(tm, astr_compare_icase($"abc"a, $"abc"a) == 0s);
    check(tm, astr_compare_icase($"bbc"a, $"abc"a) > 0s);
    check(tm, astr_compare_icase($"abc"a, $"bbc"a) < 0s);
    check(tm, astr_compare_icase($"acc"a, $"abc"a) > 0s);
    check(tm, astr_compare_icase($"abc"a, $"acc"a) < 0s);
    check(tm, astr_compare_icase($"ABC"a, $"abc"a) == 0s);
    check(tm, astr_compare_icase($"abc"a, $"ABC"a) == 0s);
}

export func test_astr_eq_icase : (tm : ^ mut test_manager) -> void
{
    check(tm, astr_eq_icase($""a, $""a));
    check(tm, !astr_eq_icase($""a, $"abc"a));
    check(tm, astr_eq_icase($"abc"a, $"abc"a));
    check(tm, !astr_eq_icase($"acc"a, $"abc"a));
    check(tm, !astr_eq_icase($"abc"a, $"acc"a));
    check(tm, astr_eq_icase($"ABC"a, $"abc"a));
    check(tm, astr_eq_icase($"abc"a, $"ABC"a));
}

export func test_astr_ne_icase : (tm : ^ mut test_manager) -> void
{
    check(tm, !astr_ne_icase($""a, $""a));
    check(tm, astr_ne_icase($""a, $"abc"a));
    check(tm, !astr_ne_icase($"abc"a, $"abc"a));
    check(tm, astr_ne_icase($"acc"a, $"abc"a));
    check(tm, astr_ne_icase($"abc"a, $"acc"a));
    check(tm, !astr_ne_icase($"ABC"a, $"abc"a));
    check(tm, !astr_ne_icase($"abc"a, $"ABC"a));
}

export func test_astr_compare_n : (tm : ^ mut test_manager) -> void
{
    check(tm, astr_compare_n($""a, $""a, 0u) == 0s);
    check(tm, astr_compare_n($""a, $""a, 3u) == 0s);
    check(tm, astr_compare_n($"abc"a, $""a, 3u) > 0s);
    check(tm, astr_compare_n($""a, $"abc"a, 3u) < 0s);
    check(tm, astr_compare_n($"abc"a, $"a"a, 3u) > 0s);
    check(tm, astr_compare_n($"a"a, $"abc"a, 3u) < 0s);
    check(tm, astr_compare_n($"abc"a, $"ab"a, 3u) > 0s);
    check(tm, astr_compare_n($"ab"a, $"abc"a, 3u) < 0s);
    check(tm, astr_compare_n($"abc"a, $"abc"a, 3u) == 0s);
    check(tm, astr_compare_n($"abc"a, $"abc"a, 3u) == 0s);
    check(tm, astr_compare_n($"abc"a, $"abc"a, 2u) == 0s);
    check(tm, astr_compare_n($"abc"a, $"abc"a, 2u) == 0s);
    check(tm, astr_compare_n($"abd"a, $"abc"a, 3u) > 0s);
    check(tm, astr_compare_n($"bbc"a, $"abc"a, 1u) > 0s);
    check(tm, astr_compare_n($"abc"a, $"bbc"a, 1u) < 0s);
    check(tm, astr_compare_n($"abb"a, $"abc"a, 1u) == 0s);
    check(tm, astr_compare_n($"abc"a, $"abb"a, 1u) == 0s);
    check(tm, astr_compare_n($"abc"a, $"abd"a, 3u) < 0s);
    check(tm, astr_compare_n($"abd"a, $"abc"a, 2u) == 0s);
    check(tm, astr_compare_n($"abc"a, $"abd"a, 2u) == 0s);
    check(tm, astr_compare_n($"bbc"a, $"abc"a, 3u) > 0s);
    check(tm, astr_compare_n($"abc"a, $"bbc"a, 3u) < 0s);
    check(tm, astr_compare_n($"bbc"a, $"abc"a, 2u) > 0s);
    check(tm, astr_compare_n($"abc"a, $"bbc"a, 2u) < 0s);
    check(tm, astr_compare_n($"acc"a, $"abc"a, 3u) > 0s);
    check(tm, astr_compare_n($"abc"a, $"acc"a, 3u) < 0s);
    check(tm, astr_compare_n($"acc"a, $"abc"a, 2u) > 0s);
    check(tm, astr_compare_n($"abc"a, $"acc"a, 2u) < 0s);
    check(tm, astr_compare_n($"ABC"a, $"abc"a, 3u) < 0s);
    check(tm, astr_compare_n($"abc"a, $"ABC"a, 3u) > 0s);
}

export func test_astr_compare_n_icase : (tm : ^ mut test_manager) -> void
{
    check(tm, astr_compare_n_icase($""a, $""a, 0u) == 0s);
    check(tm, astr_compare_n_icase($""a, $""a, 3u) == 0s);
    check(tm, astr_compare_n_icase($"abc"a, $""a, 3u) > 0s);
    check(tm, astr_compare_n_icase($""a, $"abc"a, 3u) < 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"a"a, 3u) > 0s);
    check(tm, astr_compare_n_icase($"a"a, $"abc"a, 3u) < 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"ab"a, 3u) > 0s);
    check(tm, astr_compare_n_icase($"ab"a, $"abc"a, 3u) < 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"abc"a, 3u) == 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"abc"a, 3u) == 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"abc"a, 2u) == 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"abc"a, 2u) == 0s);
    check(tm, astr_compare_n_icase($"abd"a, $"abc"a, 3u) > 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"abd"a, 3u) < 0s);
    check(tm, astr_compare_n_icase($"bbc"a, $"abc"a, 1u) > 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"bbc"a, 1u) < 0s);
    check(tm, astr_compare_n_icase($"abb"a, $"abc"a, 1u) == 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"abb"a, 1u) == 0s);
    check(tm, astr_compare_n_icase($"abd"a, $"abc"a, 2u) == 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"abd"a, 2u) == 0s);
    check(tm, astr_compare_n_icase($"bbc"a, $"abc"a, 3u) > 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"bbc"a, 3u) < 0s);
    check(tm, astr_compare_n_icase($"bbc"a, $"abc"a, 2u) > 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"bbc"a, 2u) < 0s);
    check(tm, astr_compare_n_icase($"acc"a, $"abc"a, 3u) > 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"acc"a, 3u) < 0s);
    check(tm, astr_compare_n_icase($"acc"a, $"abc"a, 2u) > 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"acc"a, 2u) < 0s);
    check(tm, astr_compare_n_icase($"ABC"a, $"abc"a, 3u) == 0s);
    check(tm, astr_compare_n_icase($"abc"a, $"ABC"a, 3u) == 0s);
}

export func test_astr_contains : (tm : ^ mut test_manager) -> void
{
    check(tm, astr_contains($""a, $""a));
    check(tm, astr_contains($"abc"a, $"a"a));
    check(tm, astr_contains($"abc"a, $"b"a));
    check(tm, astr_contains($"abc"a, $"c"a));
    check(tm, astr_contains($"abc"a, $"ab"a));
    check(tm, astr_contains($"abc"a, $"bc"a));
    check(tm, astr_contains($"abc"a, $"abc"a));
    check(tm, !astr_contains($"abc"a, $"abcd"a));
    check(tm, !astr_contains($"abc"a, $"d"a));
    check(tm, !astr_contains($"abc"a, $"de"a));
    check(tm, !astr_contains($"abc"a, $"def"a));
    check(tm, !astr_contains($"abc"a, $"defg"a));
}

export func test_astr_starts_with : (tm : ^ mut test_manager) -> void
{
    check(tm, astr_starts_with($""a, $""a));
    check(tm, astr_starts_with($"abc"a, $""a));
    check(tm, astr_starts_with($"abc"a, $"a"a));
    check(tm, astr_starts_with($"abc"a, $"ab"a));
    check(tm, astr_starts_with($"abc"a, $"abc"a));
    check(tm, !astr_starts_with($"abc"a, $"abx"a));
    check(tm, !astr_starts_with($"abc"a, $"axx"a));
    check(tm, !astr_starts_with($"abc"a, $"abcd"a));
    check(tm, !astr_starts_with($"abc"a, $"bcd"a));
    check(tm, !astr_starts_with($"abc"a, $"bc"a));
    check(tm, !astr_starts_with($"abc"a, $"d"a));
    check(tm, !astr_starts_with($"abc"a, $"de"a));
    check(tm, !astr_starts_with($"abc"a, $"def"a));
    check(tm, !astr_starts_with($"abc"a, $"defg"a));
}

export func test_astr_ends_with : (tm : ^ mut test_manager) -> void
{
    check(tm, astr_ends_with($""a, $""a));
    check(tm, astr_ends_with($"abc"a, $""a));
    check(tm, astr_ends_with($"abc"a, $"c"a));
    check(tm, astr_ends_with($"abc"a, $"bc"a));
    check(tm, astr_ends_with($"abc"a, $"abc"a));
    check(tm, !astr_ends_with($"abc"a, $"zabc"a));
    check(tm, !astr_ends_with($"abc"a, $"abcd"a));
    check(tm, !astr_ends_with($"abc"a, $"bcd"a));
    check(tm, !astr_ends_with($"abc"a, $"cd"a));
    check(tm, !astr_ends_with($"abc"a, $"d"a));
    check(tm, !astr_ends_with($"abc"a, $"xbc"a));
    check(tm, !astr_ends_with($"abc"a, $"xxc"a));
}

export func test_astr_to_u64 : (tm : ^ mut test_manager) -> void
{
    let x : mut u64;
    astr_to_u64($"0"a, ?x, 10u8);
    check(tm, x == 0u64);
    astr_to_u64($"1"a, ?x, 10u8);
    check(tm, x == 1u64);
    astr_to_u64($"2"a, ?x, 10u8);
    check(tm, x == 2u64);
    astr_to_u64($"3"a, ?x, 10u8);
    check(tm, x == 3u64);
    astr_to_u64($"4"a, ?x, 10u8);
    check(tm, x == 4u64);
    astr_to_u64($"5"a, ?x, 10u8);
    check(tm, x == 5u64);
    astr_to_u64($"6"a, ?x, 10u8);
    check(tm, x == 6u64);
    astr_to_u64($"7"a, ?x, 10u8);
    check(tm, x == 7u64);
    astr_to_u64($"8"a, ?x, 10u8);
    check(tm, x == 8u64);
    astr_to_u64($"9"a, ?x, 10u8);
    check(tm, x == 9u64);

    astr_to_u64($"1234567890"a, ?x, 10u8);
    check(tm, x == 1234567890u64);
}

export func test_astr_to_s64 : (tm : ^ mut test_manager) -> void
{
    let x : mut s64;
    astr_to_s64($"0"a, ?x, 10u8);
    check(tm, x == 0s64);
    astr_to_s64($"1"a, ?x, 10u8);
    check(tm, x == 1s64);
    astr_to_s64($"2"a, ?x, 10u8);
    check(tm, x == 2s64);
    astr_to_s64($"3"a, ?x, 10u8);
    check(tm, x == 3s64);
    astr_to_s64($"4"a, ?x, 10u8);
    check(tm, x == 4s64);
    astr_to_s64($"5"a, ?x, 10u8);
    check(tm, x == 5s64);
    astr_to_s64($"6"a, ?x, 10u8);
    check(tm, x == 6s64);
    astr_to_s64($"7"a, ?x, 10u8);
    check(tm, x == 7s64);
    astr_to_s64($"8"a, ?x, 10u8);
    check(tm, x == 8s64);
    astr_to_s64($"9"a, ?x, 10u8);
    check(tm, x == 9s64);

    astr_to_s64($"1234567890"a, ?x, 10u8);
    check(tm, x == 1234567890s64);
}

export func test_u_to_astr : (tm : ^ mut test_manager) -> void
{
    let x : ^ mut ascii = allocate(16u);
    let y : mut u = u_to_astr(0u, x);
    check_eq(tm, x, $"0"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = u_to_astr(1u, x);
    check_eq(tm, x, $"1"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = u_to_astr(2u, x);
    check_eq(tm, x, $"2"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = u_to_astr(3u, x);
    check_eq(tm, x, $"3"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = u_to_astr(4u, x);
    check_eq(tm, x, $"4"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = u_to_astr(5u, x);
    check_eq(tm, x, $"5"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = u_to_astr(6u, x);
    check_eq(tm, x, $"6"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = u_to_astr(7u, x);
    check_eq(tm, x, $"7"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = u_to_astr(8u, x);
    check_eq(tm, x, $"8"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = u_to_astr(9u, x);
    check_eq(tm, x, $"9"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = u_to_astr(1234567890u, x);
    check_eq(tm, x, $"1234567890"a);
    check_len(tm, x, 10u);
    check(tm, y == 10u);

    deallocate(x);
}

export func test_s_to_astr : (tm : ^ mut test_manager) -> void
{
    let x : ^ mut ascii = allocate(16u);
    let y : mut u = s_to_astr(0s, x);
    check_eq(tm, x, $"0"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = s_to_astr(1s, x);
    check_eq(tm, x, $"1"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = s_to_astr(2s, x);
    check_eq(tm, x, $"2"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = s_to_astr(3s, x);
    check_eq(tm, x, $"3"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = s_to_astr(4s, x);
    check_eq(tm, x, $"4"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = s_to_astr(5s, x);
    check_eq(tm, x, $"5"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = s_to_astr(6s, x);
    check_eq(tm, x, $"6"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = s_to_astr(7s, x);
    check_eq(tm, x, $"7"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = s_to_astr(8s, x);
    check_eq(tm, x, $"8"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = s_to_astr(9s, x);
    check_eq(tm, x, $"9"a);
    check_len(tm, x, 1u);
    check(tm, y == 1u);

    y = s_to_astr(1234567890s, x);
    check_eq(tm, x, $"1234567890"a);
    check_len(tm, x, 10u);
    check(tm, y == 10u);

    y = s_to_astr(-1234567890s, x);
    check_eq(tm, x, $"-1234567890"a);
    check_len(tm, x, 11u);
    check(tm, y == 11u);

    deallocate(x);
}
