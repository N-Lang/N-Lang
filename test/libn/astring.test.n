import "std/testing.n"a;
import "std/astring.n"a;
import "std/math.n"a;

func check_astring_data :
    (tm : ^mut test_manager, string : ^astring, data : ^ascii) -> void
{
    if !astr_eq(string.data(), data)
    {
        tm.fail_astr($"Unexpected astring data."a);
    }
}

func check_astring_length :
    (tm : ^mut test_manager, string : ^astring, length : u) -> void
{
    if string.length() != length
    {
        tm.fail_astr($"Unexpected astring length."a);
    }
}

func check : (tm : ^ mut test_manager, chk : bool) -> void
{
    if !chk
    {
        tm.fail_astr($"Failed Check"a);
    }
}

export func test_astring_init : (tm : ^mut test_manager) -> void
{
    let string : mut astring;
    string.init();
    defer{string.fini();}
    check_astring_data(tm, ?string, $""a);
    check_astring_length(tm, ?string, astr_length($""a));
}

export func test_astring_init_copy : (tm : ^mut test_manager) -> void
{
    let orig : mut astring;
    orig.init_astr($"foo"a);
    defer{orig.fini();}

    let string : mut astring;
    string.init_copy(?orig);
    defer{string.fini();}
    check_astring_data(tm, ?string, orig.data());
    check_astring_length(tm, ?string, orig.length());

    let string2 : mut astring;
    let lstr : ^ ascii = $"A really really really really really long string."a;
    orig.assign_astr(lstr);
    string2.init_copy(?orig);
    defer{string2.fini();}
    check_astring_data(tm, ?string2, lstr);
    check_astring_length(tm, ?string2, astr_length(lstr));
}

export func test_astring_init_astr : (tm : ^mut test_manager) -> void
{
    let string0 : mut astring;
    string0.init_astr($""a);
    defer{string0.fini();}
    check_astring_data(tm, ?string0, $""a);
    check_astring_length(tm, ?string0, astr_length($""a));

    let string1 : mut astring;
    string1.init_astr($"foo"a);
    defer{string1.fini();}
    check_astring_data(tm, ?string1, $"foo"a);
    check_astring_length(tm, ?string1, astr_length($"foo"a));

    let string2 : mut astring;
    let lstr : ^ ascii = $"A really really really really really long string."a;
    string2.init_astr(lstr);
    defer{string2.fini();}
    check_astring_data(tm, ?string2, lstr);
    check_astring_length(tm, ?string2, astr_length(lstr));
}

export func test_astring_init_astr_n : (tm : ^mut test_manager) -> void
{
    let string1 : mut astring;
    string1.init_astr_n($"foo foo"a, 3u);
    defer{string1.fini();}
    check_astring_data(tm, ?string1, $"foo"a);
    check_astring_length(tm, ?string1, 3u);

    let string2 : mut astring;
    let lstr : ^ ascii = $"A really really really really really long string."a;
    let rstr : ^ mut ascii = allocate(41u); astr_copy_n(rstr, lstr, 40u);
    string2.init_astr_n(lstr, 40u);
    defer{string2.fini();}
    check_astring_data(tm, ?string2, rstr);
    check_astring_length(tm, ?string2, astr_length(rstr));
    deallocate(rstr);
}

export func test_astring_assign : (tm : ^mut test_manager) -> void
{
    let orig : mut astring;
    orig.init_astr($"foobar"a);
    defer{orig.fini();}

    let string : mut astring;
    string.init();
    defer{string.fini();}

    string.assign(?orig);

    check_astring_data(tm, ?string, orig.data());
    check_astring_length(tm, ?string, orig.length());

    let string2 : mut astring;
    defer{string2.fini();}
    string2.init();
    let lstr : ^ ascii = $"A really really really really really long string."a;
    orig.assign_astr(lstr);
    string2.assign(?orig);
    check_astring_data(tm, ?string2, lstr);
    check_astring_length(tm, ?string2, astr_length(lstr));
}

export func test_astring_assign_astr : (tm : ^mut test_manager) -> void
{
    let string : mut astring;
    string.init();
    defer{string.fini();}

    string.assign_astr($"foobar"a);
    check_astring_data(tm, ?string, $"foobar"a);
    check_astring_length(tm, ?string, astr_length($"foobar"a));

    let string2 : mut astring;
    defer{string2.fini();}
    string2.init();
    let lstr : ^ ascii = $"A really really really really really long string."a;
    string2.assign_astr(lstr);
    check_astring_data(tm, ?string2, lstr);
    check_astring_length(tm, ?string2, astr_length(lstr));
}

export func test_astring_assign_astr_n : (tm : ^mut test_manager) -> void
{
    let string1 : mut astring;
    string1.init();
    string1.assign_astr_n($"foo foo"a, 3u);
    defer{string1.fini();}
    check_astring_data(tm, ?string1, $"foo"a);
    check_astring_length(tm, ?string1, 3u);

    let string2 : mut astring;
    string2.init();
    let lstr : ^ ascii = $"A really really really really really long string."a;
    let rstr : ^ mut ascii = allocate(41u); astr_copy_n(rstr, lstr, 40u);
    string2.assign_astr_n(lstr, 40u);
    defer{string2.fini();}
    check_astring_data(tm, ?string2, rstr);
    check_astring_length(tm, ?string2, astr_length(rstr));
    deallocate(rstr);
}

export func test_astring_length : (tm : ^ mut test_manager) -> void
{
    let str0 : mut astring; defer { str0.fini(); }
    str0.init_astr($""a);
    if str0.length() != 0u
    {
        tm.fail_astr($"Failed astring length for empty string."a);
    }

    let str1 : mut astring; defer { str1.fini(); }
    str1.init_astr($"1234"a);
    if str1.length() != 4u
    {
        tm.fail_astr($"Failed astring length for short string."a);
    }

    let str2 : mut astring; defer { str2.fini(); }
    str2.init_astr($"12345678901234567890123456789012345678901234567890"a);
    if str2.length() != 50u
    {
        tm.fail_astr($"Failed astring length for long string."a);
    }
}

export func test_astring_data : (tm : ^ mut test_manager) -> void
{
    let str0 : mut astring; defer { str0.fini(); }
    str0.init_astr($""a);
    if astr_ne(str0.data(), $""a)
    {
        tm.fail_astr($"Failed astring data for empty string."a);
    }

    let str1 : mut astring; defer { str1.fini(); }
    str1.init_astr($"1234"a);
    if astr_ne(str1.data(), $"1234"a)
    {
        tm.fail_astr($"Failed astring data for short string."a);
    }

    let str2 : mut astring; defer { str2.fini(); }
    str2.init_astr($"12345678901234567890123456789012345678901234567890"a);
    if astr_ne(str2.data(),
        $"12345678901234567890123456789012345678901234567890"a)
    {
        tm.fail_astr($"Failed astring data for long string."a);
    }
}

export func test_astring_is_empty : (tm : ^mut test_manager) -> void
{
    let empty : mut astring;
    empty.init_astr($""a);
    defer{empty.fini();}
    if (!empty.is_empty())
    {
        tm.fail_astr($"Empty astring is not concidered empty."a);
    }

    let non_empty : mut astring;
    non_empty.init_astr($"foobar"a);
    defer{non_empty.fini();}
    if (non_empty.is_empty())
    {
        tm.fail_astr($"Non-empty astring is concidered empty."a);
    }
}

export func test_astring_at : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; shrt.init_astr($"abcd"a);
    defer { shrt.fini(); }
    check(tm, shrt.at(0u) == 'a'a);
    check(tm, shrt.at(1u) == 'b'a);
    check(tm, shrt.at(2u) == 'c'a);
    check(tm, shrt.at(3u) == 'd'a);

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"a);
    loop i : mut u in [0u, 26u)
    {
        check(tm, lng.at(i) == (i + 'a'a as u) as ascii);
        check(tm, lng.at(i + 26u) == (i + 'a'a as u) as ascii);
    }
}

export func test_astring_set : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; shrt.init_astr($"abcd"a);
    defer { shrt.fini(); }
    shrt.set(0u, '1'a);
    check(tm, shrt.at(0u) == '1'a);
    shrt.set(1u, '2'a);
    check(tm, shrt.at(1u) == '2'a);
    shrt.set(2u, '3'a);
    check(tm, shrt.at(2u) == '3'a);
    shrt.set(3u, '4'a);
    check(tm, shrt.at(3u) == '4'a);

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"a);
    loop i : mut u in [0u, 26u)
    {
        let chr : ascii = (u_mod(i, 10u) + '0'a as u) as ascii;
        lng.set(i, chr);
        check(tm, lng.at(i) == chr);
        lng.set(i + 26u, chr);
        check(tm, lng.at(i + 26u) == chr);
    }
}

export func test_astring_reserve_capacity : (tm : ^ mut test_manager) -> void
{
    # 1. check that reserve and capacity work as expected.
    let str : mut astring; defer { str.fini(); }
    str.init_astr($"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"a);
    let init_cap : u = str.capacity();
    str.reserve(init_cap + 52u);
    check(tm, str.capacity() == init_cap + 52u);
    str.reserve(init_cap);
    check(tm, str.capacity() == init_cap + 52u);

    # 2. test the space is useable. This is testable only by absence of a
    # segmentation fault, or by a memory check through valgrind or -fsanitize
    let astr : ^ mut ascii = str.data() as ^ mut ascii;
    astr_copy_n(astr +^ str.length(),
        $"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"a, 52u);
}

export func test_astring_prepend : (tm : ^ mut test_manager) -> void
{
    let pre : mut astring; defer { pre.fini(); }
    pre.init_astr($"The "a);

    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"abc"a);
    shrt.prepend(?pre);
    check_astring_data(tm, ?shrt, $"The abc"a);
    check_astring_length(tm, ?shrt, astr_length($"The abc"a));

    let lng : mut astring; defer { lng.fini(); }
    let lstr : ^ ascii = $    "really really really really really long string."a;
    let rstr : ^ ascii = $"The really really really really really long string."a;
    lng.init_astr(lstr);
    lng.prepend(?pre);
    check_astring_data(tm, ?lng, rstr);
    check_astring_length(tm, ?lng, astr_length(rstr));
}

export func test_astring_prepend_ascii : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"bcde"a);
    shrt.prepend_ascii('a'a);
    check_astring_data(tm, ?shrt, $"abcde"a);
    check_astring_length(tm, ?shrt, astr_length($"abcde"a));

    let lng : mut astring; defer { lng.fini(); }
    let lstr : ^ ascii = $ " really really really really really long string."a;
    let rstr : ^ ascii = $"A really really really really really long string."a;
    lng.init_astr(lstr);
    lng.prepend_ascii('A'a);
    check_astring_data(tm, ?lng, rstr);
    check_astring_length(tm, ?lng, astr_length(rstr));
}

export func test_astring_prepend_astr : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"bcde"a);
    shrt.prepend_astr($"a"a);
    check_astring_data(tm, ?shrt, $"abcde"a);
    check_astring_length(tm, ?shrt, astr_length($"abcde"a));

    let lng : mut astring; defer { lng.fini(); }
    let lstr : ^ ascii = $" really really really really really long string."a;
    let rstr : ^ ascii = $"The really really really really really long string."a;
    lng.init_astr(lstr);
    lng.prepend_astr($"The"a);
    check_astring_data(tm, ?lng, rstr);
    check_astring_length(tm, ?lng, astr_length(rstr));
}

export func test_astring_prepend_astr_n : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"bcde"a);
    shrt.prepend_astr_n($"abc"a, 1u);
    check_astring_data(tm, ?shrt, $"abcde"a);
    check_astring_length(tm, ?shrt, astr_length($"abcde"a));

    let lng : mut astring; defer { lng.fini(); }
    let lstr : ^ ascii = $" really really really really really long string."a;
    let rstr : ^ ascii = $"The really really really really really long string."a;
    lng.init_astr(lstr);
    lng.prepend_astr_n($"The really"a, 3u);
    check_astring_data(tm, ?lng, rstr);
    check_astring_length(tm, ?lng, astr_length(rstr));
}

export func test_astring_append : (tm : ^ mut test_manager) -> void
{
    let post : mut astring; defer { post.fini(); }
    post.init_astr($"string."a);

    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"abc"a);
    shrt.append(?post);
    check_astring_data(tm, ?shrt, $"abcstring."a);
    check_astring_length(tm, ?shrt, astr_length($"abcstring."a));

    let lng : mut astring; defer { lng.fini(); }
    let lstr : ^ ascii = $"The really really really really really long "a;
    let rstr : ^ ascii = $"The really really really really really long string."a;
    lng.init_astr(lstr);
    lng.append(?post);
    check_astring_data(tm, ?lng, rstr);
    check_astring_length(tm, ?lng, astr_length(rstr));
}

export func test_astring_append_ascii : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"abc"a);
    shrt.append_ascii('d'a);
    check_astring_data(tm, ?shrt, $"abcd"a);
    check_astring_length(tm, ?shrt, astr_length($"abcd"a));

    let lng : mut astring; defer { lng.fini(); }
    let lstr : ^ ascii = $"A really really really really really long string"a;
    let rstr : ^ ascii = $"A really really really really really long string."a;
    lng.init_astr(lstr);
    lng.append_ascii('.'a);
    check_astring_data(tm, ?lng, rstr);
    check_astring_length(tm, ?lng, astr_length(rstr));
}

export func test_astring_append_astr : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"abc"a);
    shrt.append_astr($"123"a);
    check_astring_data(tm, ?shrt, $"abc123"a);
    check_astring_length(tm, ?shrt, astr_length($"abc123"a));

    let lng : mut astring; defer { lng.fini(); }
    let lstr : ^ ascii = $"A really really really really really long"a;
    let rstr : ^ ascii = $"A really really really really really long string."a;
    lng.init_astr(lstr);
    lng.append_astr($" string."a);
    check_astring_data(tm, ?lng, rstr);
    check_astring_length(tm, ?lng, astr_length(rstr));
}

export func test_astring_append_astr_n : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"abc"a);
    shrt.append_astr_n($"1234"a, 3u);
    check_astring_data(tm, ?shrt, $"abc123"a);
    check_astring_length(tm, ?shrt, astr_length($"abc123"a));

    let lng : mut astring; defer { lng.fini(); }
    let lstr : ^ ascii = $"The really really really really really long"a;
    let rstr : ^ ascii = $"The really really really really really long string."a;
    lng.init_astr(lstr);
    lng.append_astr_n($" string.   "a, 8u);
    check_astring_data(tm, ?lng, rstr);
    check_astring_length(tm, ?lng, astr_length(rstr));
}

export func test_astring_contains : (tm : ^ mut test_manager) -> void
{
    let chk : mut astring; defer { chk.fini(); } chk.init();
    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"racecar"a);

    chk.assign_astr($""a);
    check(tm, shrt.contains(?chk));
    chk.assign_astr($"race"a);
    check(tm, shrt.contains(?chk));
    chk.assign_astr($"slow"a);
    check(tm, !shrt.contains(?chk));
    chk.assign_astr($"cec"a);
    check(tm, shrt.contains(?chk));
    chk.assign_astr($"car"a);
    check(tm, shrt.contains(?chk));
    chk.assign_astr($"swiper no swiping"a);
    check(tm, !shrt.contains(?chk));

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"the quick brown fox jumped over the lazy dog. the swift brown fox jumped over the sleeping dog."a);

    chk.assign_astr($""a);
    check(tm, lng.contains(?chk));
    chk.assign_astr($"the quick brown fox"a);
    check(tm, lng.contains(?chk));
    chk.assign_astr($"over the lazy dog."a);
    check(tm, lng.contains(?chk));
    chk.assign_astr($"over the sleeping dog."a);
    check(tm, lng.contains(?chk));
    chk.assign_astr($"swiper no swiping!"a);
    check(tm, !lng.contains(?chk));
    chk.assign_astr($"swiper no swiping! swiper no swiping! swiper no swiping! swiper no swiping! swiper no swiping! swiper no swiping! swiper no swiping!"a);
    check(tm, !lng.contains(?chk));
    chk.assign(?lng);
    check(tm, lng.contains(?chk));
}

export func test_astring_contains_astr : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"racecar"a);

    check(tm, shrt.contains_astr($""a));
    check(tm, shrt.contains_astr($"race"a));
    check(tm, !shrt.contains_astr($"slow"a));
    check(tm, shrt.contains_astr($"cec"a));
    check(tm, shrt.contains_astr($"car"a));
    check(tm, !shrt.contains_astr($"swiper no swiping"a));

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"the quick brown fox jumped over the lazy dog. the swift brown fox jumped over the sleeping dog."a);

    check(tm, lng.contains_astr($""a));
    check(tm, lng.contains_astr($"the quick brown fox"a));
    check(tm, lng.contains_astr($"over the lazy dog."a));
    check(tm, lng.contains_astr($"over the sleeping dog."a));
    check(tm, !lng.contains_astr($"swiper no swiping!"a));
    check(tm, !lng.contains_astr($"swiper no swiping! swiper no swiping! swiper no swiping! swiper no swiping! swiper no swiping! swiper no swiping! swiper no swiping!"a));
    check(tm, lng.contains_astr(lng.data()));
}

export func test_astring_contains_astr_n : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer { shrt.fini(); }
    shrt.init_astr($"racecar"a);

    check(tm, shrt.contains_astr_n($""a, 0u));
    check(tm, shrt.contains_astr_n($""a, 1u));
    check(tm, shrt.contains_astr_n($"race"a, 0u));
    check(tm, shrt.contains_astr_n($"race"a, 1u));
    check(tm, shrt.contains_astr_n($"race"a, 2u));
    check(tm, shrt.contains_astr_n($"race"a, 3u));
    check(tm, shrt.contains_astr_n($"race"a, 4u));
    check(tm, shrt.contains_astr_n($"race"a, 5u));
    check(tm, shrt.contains_astr_n($"slow"a, 0u));
    check(tm, !shrt.contains_astr_n($"slow"a, 1u));
    check(tm, !shrt.contains_astr_n($"slow"a, 2u));
    check(tm, !shrt.contains_astr_n($"slow"a, 3u));
    check(tm, !shrt.contains_astr_n($"slow"a, 4u));
    check(tm, !shrt.contains_astr_n($"slow"a, 5u));
    check(tm, shrt.contains_astr_n($"cec"a, 0u));
    check(tm, shrt.contains_astr_n($"cec"a, 1u));
    check(tm, shrt.contains_astr_n($"cec"a, 2u));
    check(tm, shrt.contains_astr_n($"cec"a, 3u));
    check(tm, shrt.contains_astr_n($"cec"a, 4u));
    check(tm, shrt.contains_astr_n($"car"a, 0u));
    check(tm, shrt.contains_astr_n($"car"a, 1u));
    check(tm, shrt.contains_astr_n($"car"a, 2u));
    check(tm, shrt.contains_astr_n($"car"a, 3u));
    check(tm, shrt.contains_astr_n($"car"a, 4u));
    check(tm, shrt.contains_astr_n($"swiper no swiping"a, 0u));
    check(tm, !shrt.contains_astr_n($"swiper no swiping"a, 1u));
    check(tm, !shrt.contains_astr_n($"swiper no swiping"a, 10u));
    check(tm, !shrt.contains_astr_n($"swiper no swiping"a, 100u));

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"The quick brown fox jumped over the lazy dog. The swift brown fox jumped over the sleeping dog."a);

    check(tm, lng.contains_astr_n($""a, 0u));
    check(tm, lng.contains_astr_n($""a, 1u));
    check(tm, lng.contains_astr_n($"The quick brown fox"a, 0u));
    check(tm, lng.contains_astr_n($"The quick brown fox"a, 1u));
    check(tm, lng.contains_astr_n($"The quick brown fox"a, 10u));
    check(tm, lng.contains_astr_n($"The quick brown fox"a, 100u));
    check(tm, lng.contains_astr_n($"over the lazy dog."a, 0u));
    check(tm, lng.contains_astr_n($"over the lazy dog."a, 1u));
    check(tm, lng.contains_astr_n($"over the lazy dog."a, 10u));
    check(tm, lng.contains_astr_n($"over the lazy dog."a, 100u));
    check(tm, lng.contains_astr_n($"over the sleeping dog."a, 0u));
    check(tm, lng.contains_astr_n($"over the sleeping dog."a, 1u));
    check(tm, lng.contains_astr_n($"over the sleeping dog."a, 10u));
    check(tm, lng.contains_astr_n($"over the sleeping dog."a, 100u));
    check(tm, lng.contains_astr_n($"Swiper no swiping!"a, 0u));
    check(tm, !lng.contains_astr_n($"Swiper no swiping!"a, 1u));
    check(tm, !lng.contains_astr_n($"Swiper no swiping!"a, 10u));
    check(tm, !lng.contains_astr_n($"Swiper no swiping!"a, 100u));
    check(tm, lng.contains_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 0u));
    check(tm, !lng.contains_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 1u));
    check(tm, !lng.contains_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 10u));
    check(tm, !lng.contains_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 100u));
    check(tm, !lng.contains_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 1000u));
    check(tm, lng.contains_astr_n(lng.data(), lng.length()));
}

export func test_astring_starts_with : (tm : ^ mut test_manager) -> void
{
    let chck : mut astring; defer{ chck.fini(); } chck.init();
    let shrt : mut astring; defer{ shrt.fini(); } shrt.init_astr($"abcdefg"a);
    chck.assign_astr($""a);
    check(tm, shrt.starts_with(?chck));
    chck.assign_astr($"abcd"a);
    check(tm, shrt.starts_with(?chck));
    chck.assign_astr($"wxyz"a);
    check(tm, !shrt.starts_with(?chck));
    chck.assign_astr($"1234567890987654321"a);
    check(tm, !shrt.starts_with(?chck));

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"The quick brown fox jumped over the lazy dog. The swift brown fox jumped over the sleeping dog."a);

    chck.assign_astr($""a);
    check(tm, lng.starts_with(?chck));
    chck.assign_astr($"The quick brown fox"a);
    check(tm, lng.starts_with(?chck));
    chck.assign_astr($"the lazy dog."a);
    check(tm, !lng.starts_with(?chck));
    chck.assign_astr($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a);
    check(tm, !lng.starts_with(?chck));
    chck.assign(?lng);
    check(tm, lng.starts_with(?chck));
}

export func test_astring_starts_with_astr : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer{ shrt.fini(); } shrt.init_astr($"abcdefg"a);
    check(tm, shrt.starts_with_astr($""a));
    check(tm, shrt.starts_with_astr($"abcd"a));
    check(tm, !shrt.starts_with_astr($"wxyz"a));
    check(tm, !shrt.starts_with_astr($"1234567890987654321"a));

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"The quick brown fox jumped over the lazy dog. The swift brown fox jumped over the sleeping dog."a);

    check(tm, lng.starts_with_astr($""a));
    check(tm, lng.starts_with_astr($"The quick brown fox"a));
    check(tm, !lng.starts_with_astr($"the lazy dog."a));
    check(tm, !lng.starts_with_astr($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a));
    check(tm, lng.starts_with_astr(lng.data()));
}
export func test_astring_starts_with_astr_n : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer{ shrt.fini(); } shrt.init_astr($"abcdefg"a);
    check(tm, shrt.starts_with_astr_n($""a, 0u));
    check(tm, shrt.starts_with_astr_n($""a, 3u));
    check(tm, shrt.starts_with_astr_n($"abcd"a, 0u));
    check(tm, shrt.starts_with_astr_n($"abcd"a, 3u));
    check(tm, !shrt.starts_with_astr_n($"wxyz"a, 3u));
    check(tm, !shrt.starts_with_astr_n($"1234567890987654321"a, 20u));

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"The quick brown fox jumped over the lazy dog. The swift brown fox jumped over the sleeping dog."a);

    check(tm, lng.starts_with_astr_n($""a, 0u));
    check(tm, lng.starts_with_astr_n($""a, 3u));
    check(tm, lng.starts_with_astr_n($"The quick brown fox"a, 0u));
    check(tm, lng.starts_with_astr_n($"The quick brown fox"a, 1u));
    check(tm, lng.starts_with_astr_n($"The quick brown fox"a, 10u));
    check(tm, lng.starts_with_astr_n($"The quick brown fox"a, 100u));
    check(tm, lng.starts_with_astr_n($"the lazy dog."a, 0u));
    check(tm, !lng.starts_with_astr_n($"the lazy dog."a, 1u));
    check(tm, !lng.starts_with_astr_n($"the lazy dog."a, 10u));
    check(tm, !lng.starts_with_astr_n($"the lazy dog."a, 100u));
    check(tm, lng.starts_with_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 0u));
    check(tm, !lng.starts_with_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 1u));
    check(tm, !lng.starts_with_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 10u));
    check(tm, !lng.starts_with_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 100u));
    check(tm, !lng.starts_with_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 1000u));
    check(tm, lng.starts_with_astr_n(lng.data(), lng.length()));
}

export func test_astring_ends_with : (tm : ^ mut test_manager) -> void
{
    let chck : mut astring; defer{ chck.fini(); } chck.init();
    let shrt : mut astring; defer{ shrt.fini(); } shrt.init_astr($"abcdefg"a);
    chck.assign_astr($""a);
    check(tm, shrt.ends_with(?chck));
    chck.assign_astr($"defg"a);
    check(tm, shrt.ends_with(?chck));
    chck.assign_astr($"xefg"a);
    check(tm, !shrt.ends_with(?chck));
    chck.assign_astr($"wxyz"a);
    check(tm, !shrt.ends_with(?chck));
    chck.assign_astr($"1234567890987654321"a);
    check(tm, !shrt.ends_with(?chck));

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"The quick brown fox jumped over the lazy dog. The swift brown fox jumped over the sleeping dog."a);

    chck.assign_astr($""a);
    check(tm, lng.ends_with(?chck));
    chck.assign_astr($"the sleeping dog."a);
    check(tm, lng.ends_with(?chck));
    chck.assign_astr($"the lazy dog."a);
    check(tm, !lng.ends_with(?chck));
    chck.assign_astr($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a);
    check(tm, !lng.ends_with(?chck));
    chck.assign(?lng);
    check(tm, lng.ends_with(?chck));
}

export func test_astring_ends_with_astr : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer{ shrt.fini(); } shrt.init_astr($"abcdefg"a);
    check(tm, shrt.ends_with_astr($""a));
    check(tm, shrt.ends_with_astr($"defg"a));
    check(tm, !shrt.ends_with_astr($"xefg"a));
    check(tm, !shrt.ends_with_astr($"wxyz"a));
    check(tm, !shrt.ends_with_astr($"1234567890987654321"a));

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"The quick brown fox jumped over the lazy dog. The swift brown fox jumped over the sleeping dog."a);

    check(tm, lng.ends_with_astr($""a));
    check(tm, lng.ends_with_astr($"the sleeping dog."a));
    check(tm, !lng.ends_with_astr($"the lazy dog."a));
    check(tm, !lng.ends_with_astr($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a));
    check(tm, lng.ends_with_astr(lng.data()));
}

export func test_astring_ends_with_astr_n : (tm : ^ mut test_manager) -> void
{
    let shrt : mut astring; defer{ shrt.fini(); } shrt.init_astr($"abcdefg"a);
    check(tm, shrt.ends_with_astr_n($""a, 0u));
    check(tm, shrt.ends_with_astr_n($""a, 3u));
    check(tm, shrt.ends_with_astr_n($"defg"a, 0u));
    check(tm, shrt.ends_with_astr_n($"defg"a, 3u));
    check(tm, shrt.ends_with_astr_n($"defg"a, 4u));
    check(tm, shrt.ends_with_astr_n($"xefg"a, 3u));
    check(tm, !shrt.ends_with_astr_n($"wxyz"a, 3u));
    check(tm, !shrt.ends_with_astr_n($"1234567890987654321"a, 20u));

    let lng : mut astring; defer { lng.fini(); }
    lng.init_astr($"The quick brown fox jumped over the lazy dog. The swift brown fox jumped over the sleeping dog."a);

    check(tm, lng.ends_with_astr_n($""a, 0u));
    check(tm, lng.ends_with_astr_n($""a, 3u));
    check(tm, lng.ends_with_astr_n($"the sleeping dog."a, 0u));
    check(tm, lng.ends_with_astr_n($"the sleeping dog."a, 1u));
    check(tm, lng.ends_with_astr_n($"the sleeping dog."a, 10u));
    check(tm, lng.ends_with_astr_n($"the sleeping dog."a, 100u));
    check(tm, lng.ends_with_astr_n($"the lazy dog."a, 0u));
    check(tm, lng.ends_with_astr_n($"the lazy dog."a, 1u));
    check(tm, !lng.ends_with_astr_n($"the lazy dog."a, 10u));
    check(tm, !lng.ends_with_astr_n($"the lazy dog."a, 100u));
    check(tm, lng.ends_with_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 0u));
    check(tm, !lng.ends_with_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 1u));
    check(tm, !lng.ends_with_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 10u));
    check(tm, !lng.ends_with_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 100u));
    check(tm, !lng.ends_with_astr_n($"Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping! Swiper no swiping!"a, 1000u));
    check(tm, lng.ends_with_astr_n(lng.data(), lng.length()));
}
