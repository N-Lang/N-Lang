import "std/inet.n"a;
import "std/testing.n"a;

func check : (tm : ^ mut test_manager, success : bool) -> void
{
    if !success
    {
        tm.fail_astr($"failed check"a);
    }
}

export func test_inet_address_v4 : (tm : ^ mut test_manager) -> void
{
    let taddr : mut inet_address;
    let buff : [16u] mut ascii;

    check(tm, taddr.from_astr($"127.0.0.1"a) == 9u);
    check(tm, taddr.tag == inet_address.v4);
    check(tm, taddr.v4.addr[0u] == 127u8);
    check(tm, taddr.v4.addr[1u] ==   0u8);
    check(tm, taddr.v4.addr[2u] ==   0u8);
    check(tm, taddr.v4.addr[3u] ==   1u8);
    check(tm, taddr.to_astr($buff, 15u) == 9u);
    check(tm, astr_eq($buff, $"127.0.0.1"a));

    check(tm, taddr.from_astr($"10.17.12.11"a) == 11u);
    check(tm, taddr.tag == inet_address.v4);
    check(tm, taddr.v4.addr[0u] ==  10u8);
    check(tm, taddr.v4.addr[1u] ==  17u8);
    check(tm, taddr.v4.addr[2u] ==  12u8);
    check(tm, taddr.v4.addr[3u] ==  11u8);
    check(tm, taddr.to_astr($buff, 15u) == 11u);
    check(tm, astr_eq($buff, $"10.17.12.11"a));

    check(tm, taddr.from_astr($"255.255.255.255"a) == 15u);
    check(tm, taddr.tag == inet_address.v4);
    check(tm, taddr.v4.addr[0u] == 255u8);
    check(tm, taddr.v4.addr[1u] == 255u8);
    check(tm, taddr.v4.addr[2u] == 255u8);
    check(tm, taddr.v4.addr[3u] == 255u8);
    check(tm, taddr.to_astr($buff, 15u) == 15u);
    check(tm, astr_eq($buff, $"255.255.255.255"a));

    taddr.init_localhost();
    check(tm, taddr.tag == inet_address.v4);
    check(tm, taddr.v4.addr[0u] == 127u8);
    check(tm, taddr.v4.addr[1u] ==   0u8);
    check(tm, taddr.v4.addr[2u] ==   0u8);
    check(tm, taddr.v4.addr[3u] ==   1u8);
    check(tm, taddr.to_astr($buff, 15u) == 9u);
    check(tm, astr_eq($buff, $"127.0.0.1"a));
}

export func test_tcp : (tm : ^ mut test_manager) -> void
{
    let localhost : mut inet_address;
    localhost.init_localhost();
    let server : mut server_socket;
    check(tm, server.open(8181u16));

    let conn : mut inet_socket;
    check(tm, conn.tcp.connect(?localhost, 8181u16));

    let resp : mut inet_socket;
    check(tm, server.accept(?resp));

    let buf : [20u] mut ascii;
    check(tm, conn.write($"beep beep"a as ^ u8, 10u) == 10s);
    check(tm, resp.read($buf as ^ mut u8, 20u) == 10s);
    check(tm, astr_eq($buf, $"beep beep"a));

    check(tm, resp.write($"boop boop"a as ^ u8, 10u) == 10s);
    check(tm, conn.read($buf as ^ mut u8, 20u) == 10s);
    check(tm, astr_eq($buf, $"boop boop"a));

    check(tm, conn.close());
    check(tm, resp.close());
    check(tm, server.close());
}

export func test_udp : (tm : ^ mut test_manager) -> void
{
    let con1 : mut inet_socket;
    let con2 : mut inet_socket;

    check(tm, con1.udp.open(8181u16));
    check(tm, con2.udp.open(8182u16));

    con1.remote.init_localhost();
    con1.remote_port = 8182u16;
    con2.remote.init_localhost();
    con2.remote_port = 8181u16;

    let buf : [20u] mut ascii;
    return;
    check(tm, con1.udp.write($"beep beep"a as ^ u8, 10u) == 10s);
    check(tm, con2.udp.read($buf as ^ mut u8, 20u) == 10s);
    check(tm, astr_eq($buf, $"beep beep"a));

    check(tm, con2.udp.write($"boop boop"a as ^ u8, 10u) == 10s);
    check(tm, con1.udp.read($buf as ^ mut u8, 20u) == 10s);
    check(tm, astr_eq($buf, $"boop boop"a));

    check(tm, con1.close());
    check(tm, con2.close());
}
