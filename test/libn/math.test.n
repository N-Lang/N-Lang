import "std/testing.n"a;
import "std/math.n"a;

let ERR_ABS_OF_POSITIVE_NOT_IDENTITY : ^ascii =
    $"Absolute value of a positive number is not the same number."a;
let ERR_ABS_OF_NEGATIVE_NOT_INVERSE : ^ascii =
    $"Absolute value of a negative number is not the number's inverse."a;
let ERR_ABS_OF_NEGATIVE_ZERO_NOT_POSITIVE_ZERO : ^ascii =
    $"Absolute value of negative zero is not positive zero."a;

export func test_s8_abs : (tm : ^mut test_manager) -> void
{
    if (s8_abs(123s8) != 123u8)
    {
        tm.fail_astr(ERR_ABS_OF_POSITIVE_NOT_IDENTITY);
    }
    if (s8_abs(-123s8) != 123u8)
    {
        tm.fail_astr(ERR_ABS_OF_NEGATIVE_NOT_INVERSE);
    }
}

export func test_s16_abs : (tm : ^mut test_manager) -> void
{
    if (s16_abs(123s16) != 123u16)
    {
        tm.fail_astr(ERR_ABS_OF_POSITIVE_NOT_IDENTITY);
    }
    if (s16_abs(-123s16) != 123u16)
    {
        tm.fail_astr(ERR_ABS_OF_NEGATIVE_NOT_INVERSE);
    }
}

export func test_s32_abs : (tm : ^mut test_manager) -> void
{
    if (s32_abs(123s32) != 123u32)
    {
        tm.fail_astr(ERR_ABS_OF_POSITIVE_NOT_IDENTITY);
    }
    if (s32_abs(-123s32) != 123u32)
    {
        tm.fail_astr(ERR_ABS_OF_NEGATIVE_NOT_INVERSE);
    }
}

export func test_s64_abs : (tm : ^mut test_manager) -> void
{
    if (s64_abs(123s64) != 123u64)
    {
        tm.fail_astr(ERR_ABS_OF_POSITIVE_NOT_IDENTITY);
    }
    if (s64_abs(-123s64) != 123u64)
    {
        tm.fail_astr(ERR_ABS_OF_NEGATIVE_NOT_INVERSE);
    }
}

export func test_s_abs : (tm : ^mut test_manager) -> void
{
    if (s_abs(123s) != 123u)
    {
        tm.fail_astr(ERR_ABS_OF_POSITIVE_NOT_IDENTITY);
    }
    if (s_abs(-123s) != 123u)
    {
        tm.fail_astr(ERR_ABS_OF_NEGATIVE_NOT_INVERSE);
    }
}

export func test_f32_abs : (tm : ^mut test_manager) -> void
{
    if (f32_abs(123.125f32) != 123.125f32)
    {
        tm.fail_astr(ERR_ABS_OF_POSITIVE_NOT_IDENTITY);
    }
    if (f32_abs(-123.125f32) != 123.125f32)
    {
        tm.fail_astr(ERR_ABS_OF_NEGATIVE_NOT_INVERSE);
    }
    if (f32_abs(-0.0f32) != 0.0f32)
    {
        tm.fail_astr(ERR_ABS_OF_NEGATIVE_ZERO_NOT_POSITIVE_ZERO);
    }
}

export func test_f64_abs : (tm : ^mut test_manager) -> void
{
    if (f64_abs(123.125f64) != 123.125f64)
    {
        tm.fail_astr(ERR_ABS_OF_POSITIVE_NOT_IDENTITY);
    }
    if (f64_abs(-123.125f64) != 123.125f64)
    {
        tm.fail_astr(ERR_ABS_OF_NEGATIVE_NOT_INVERSE);
    }
    if (f64_abs(-0.0f64) != 0.0f64)
    {
        tm.fail_astr(ERR_ABS_OF_NEGATIVE_ZERO_NOT_POSITIVE_ZERO);
    }
}
