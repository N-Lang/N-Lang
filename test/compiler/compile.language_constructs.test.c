/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "compiler.testutils.h"

EMU_DECLARE(compile__variable_declarations);
EMU_DECLARE(compile__expressions);
EMU_DECLARE(compile__if_elif_else);
EMU_DECLARE(compile__loop);
EMU_DECLARE(compile__defer_statements);
EMU_DECLARE(compile__nested_scope);

EMU_GROUP(compile__language_constructs)
{
    EMU_ADD(compile__variable_declarations);
    EMU_ADD(compile__expressions);
    EMU_ADD(compile__if_elif_else);
    EMU_ADD(compile__loop);
    EMU_ADD(compile__defer_statements);
    EMU_ADD(compile__nested_scope);
    EMU_END_GROUP();
}
