#include "compiler.testutils.h"
#include <compiler/nir.h>

static char const* const LOREM_IPSUM = "lorem ipsum dolor sit amet";

//============================================================================//
//      CHARACTER                                                             //
//============================================================================//
EMU_TEST(parse_n_ascii__should_parse_all_printible_ascii_characters_except_single_and_double_quote)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii(" ", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("!", &c), 0);
    EMU_EXPECT_LE(parse_n_ascii("\"", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("#", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("$", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("%", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("&", &c), 0);
    EMU_EXPECT_NE(parse_n_ascii("\'", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("(", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(")", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("*", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("+", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(",", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("-", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(".", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("/", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("0", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("1", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("2", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("3", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("4", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("5", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("6", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("7", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("8", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("9", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(":", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(";", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("<", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("=", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(">", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("\?", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("@", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("A", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("B", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("C", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("D", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("E", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("F", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("G", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("H", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("I", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("J", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("K", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("L", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("M", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("N", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("O", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("P", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("Q", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("R", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("S", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("T", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("U", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("V", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("W", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("X", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("Y", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("Z", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("[", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("\\\\", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("]", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("^", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("_", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("`", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("b", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("c", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("d", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("e", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("f", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("g", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("h", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("i", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("j", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("k", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("l", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("m", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("n", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("o", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("p", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("q", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("r", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("s", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("t", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("u", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("v", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("w", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("x", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("y", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("z", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("{", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("|", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("}", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("~", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_null)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\0", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_alert)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_backspace)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\b", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_escape)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\e", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_formfeed)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\f", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_newline)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\n", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_carriage_return)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\r", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_horizontal_tab)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\t", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_vertical_tab)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\v", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_backslash)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\\\", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_single_quote)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\\'", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_double_quote)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\\"", &c), 0);
    EMU_END_TEST();
}

EMU_GROUP(parse_n_ascii__should_parse_all_escape_characters)
{
    EMU_ADD(parse_n_ascii__should_parse_null);
    EMU_ADD(parse_n_ascii__should_parse_alert);
    EMU_ADD(parse_n_ascii__should_parse_backspace);
    EMU_ADD(parse_n_ascii__should_parse_escape);
    EMU_ADD(parse_n_ascii__should_parse_formfeed);
    EMU_ADD(parse_n_ascii__should_parse_newline);
    EMU_ADD(parse_n_ascii__should_parse_carriage_return);
    EMU_ADD(parse_n_ascii__should_parse_horizontal_tab);
    EMU_ADD(parse_n_ascii__should_parse_vertical_tab);
    EMU_ADD(parse_n_ascii__should_parse_backslash);
    EMU_ADD(parse_n_ascii__should_parse_single_quote);
    EMU_ADD(parse_n_ascii__should_parse_double_quote);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_ascii__should_correctly_populate_the_length_of_the_string)
{
    n_ascii c;
    int len;

    len = parse_n_ascii("t", &c);
    EMU_ASSERT_GT_INT(len, 0);
    EMU_ASSERT_EQ_UINT(strlen("t"), (unsigned)len);

    len = parse_n_ascii("\\t", &c);
    EMU_ASSERT_EQ(strlen("\\t"), len);

    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_not_parse_unescaped_single_quote)
{
    n_ascii c;
    EMU_ASSERT_EQ(-1, parse_n_ascii("\'", &c));
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_not_parse_unescaped_double_quote)
{
    n_ascii c;
    EMU_ASSERT_EQ(-1, parse_n_ascii("\"", &c));
    EMU_END_TEST();
}

EMU_GROUP(parse_n_ascii__should_not_parse_unescaped_quotes)
{
    EMU_ADD(parse_n_ascii__should_not_parse_unescaped_single_quote);
    EMU_ADD(parse_n_ascii__should_not_parse_unescaped_double_quote);
    EMU_END_GROUP();
}

EMU_GROUP(parse_n_ascii)
{
    EMU_ADD(parse_n_ascii__should_parse_all_printible_ascii_characters_except_single_and_double_quote);
    EMU_ADD(parse_n_ascii__should_parse_all_escape_characters);
    EMU_ADD(parse_n_ascii__should_correctly_populate_the_length_of_the_string);
    EMU_ADD(parse_n_ascii__should_not_parse_unescaped_quotes);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_ascii_literal__should_fail_to_parse_garbage)
{
    n_ascii c;
    EMU_EXPECT_EQ(parse_n_ascii_literal(LOREM_IPSUM, &c), -1);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_all_printible_ascii_characters_except_single_and_double_quote)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\' \'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'!\'a", &c), 0);
    EMU_EXPECT_LE_INT(parse_n_ascii_literal("\'\"\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'#\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'$\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'%\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'&\'a", &c), 0);
    EMU_EXPECT_LE_INT(parse_n_ascii_literal("\'\'\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'(\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\')\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'*\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'+\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\',\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'-\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'.\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'/\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'0\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'1\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'2\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'3\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'4\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'5\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'6\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'7\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'8\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'9\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\':\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\';\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'<\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'=\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'>\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\?\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'@\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'A\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'B\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'C\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'D\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'E\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'F\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'G\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'H\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'I\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'J\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'K\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'L\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'M\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'N\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'O\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'P\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'Q\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'R\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'S\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'T\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'U\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'V\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'W\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'X\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'Y\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'Z\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'[\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\\\\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\']\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'^\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'_\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'`\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'a\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'b\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'c\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'d\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'e\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'f\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'g\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'h\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'i\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'j\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'k\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'l\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'m\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'n\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'o\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'p\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'q\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'r\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'s\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'t\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'u\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'v\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'w\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'x\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'y\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'z\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'{\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'|\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'}\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'~\'a", &c), 0);

    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_null)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\0\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_alert)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\a\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_backspace)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\b\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_escape)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\e\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_formfeed)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\f\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_newline)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\n\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_carriage_return)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\r\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_horizontal_tab)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\t\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_vertical_tab)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\v\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_backslash)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\\\\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_single_quote)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\\'\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_double_quote)
{
    n_ascii c;
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\\"\'a", &c), 0);
    EMU_END_TEST();
}

EMU_GROUP(parse_n_ascii_literal__should_parse_all_escape_characters)
{
    EMU_ADD(parse_n_ascii_literal__should_parse_null);
    EMU_ADD(parse_n_ascii_literal__should_parse_alert);
    EMU_ADD(parse_n_ascii_literal__should_parse_backspace);
    EMU_ADD(parse_n_ascii_literal__should_parse_escape);
    EMU_ADD(parse_n_ascii_literal__should_parse_formfeed);
    EMU_ADD(parse_n_ascii_literal__should_parse_newline);
    EMU_ADD(parse_n_ascii_literal__should_parse_carriage_return);
    EMU_ADD(parse_n_ascii_literal__should_parse_horizontal_tab);
    EMU_ADD(parse_n_ascii_literal__should_parse_vertical_tab);
    EMU_ADD(parse_n_ascii_literal__should_parse_backslash);
    EMU_ADD(parse_n_ascii_literal__should_parse_single_quote);
    EMU_ADD(parse_n_ascii_literal__should_parse_double_quote);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_ascii_literal__should_fail_when_opening_quote_is_missing)
{
    n_ascii c;
    EMU_EXPECT_EQ(-1, parse_n_ascii_literal("t\'a", &c));
    EMU_EXPECT_EQ(-1, parse_n_ascii_literal("\\t\'a", &c));
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_fail_when_closing_quote_is_missing)
{
    n_ascii c;
    EMU_EXPECT_EQ(-1, parse_n_ascii_literal("\'t", &c));
    EMU_EXPECT_EQ(-1, parse_n_ascii_literal("\'\\t", &c));
    EMU_END_TEST();
}

EMU_GROUP(parse_n_ascii_literal__should_fail_when_quotes_are_missing)
{
    EMU_ADD(parse_n_ascii_literal__should_fail_when_opening_quote_is_missing);
    EMU_ADD(parse_n_ascii_literal__should_fail_when_closing_quote_is_missing);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_ascii_literal__should_fail_when_type_suffix_is_missing)
{
    n_ascii c;
    EMU_EXPECT_EQ(-1, parse_n_ascii_literal("\'k\'", &c));
    EMU_EXPECT_EQ(-1, parse_n_ascii_literal("\'\\n\'", &c));
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_correctly_populate_the_length_of_the_string)
{
    n_ascii c;
    int len;

    len = parse_n_ascii_literal("\'t\'a", &c);
    EMU_ASSERT_GT_INT(len, 0);
    EMU_ASSERT_EQ_UINT(strlen("\'t\'a"), (unsigned)len);

    len = parse_n_ascii_literal("\'\\t\'a", &c);
    EMU_ASSERT_GT_INT(len, 0);
    EMU_ASSERT_EQ_UINT(strlen("\'\\t\'a"), (unsigned)len);

    EMU_END_TEST();
}

EMU_GROUP(parse_n_ascii_literal)
{
    EMU_ADD(parse_n_ascii_literal__should_fail_to_parse_garbage);
    EMU_ADD(parse_n_ascii_literal__should_parse_all_printible_ascii_characters_except_single_and_double_quote);
    EMU_ADD(parse_n_ascii_literal__should_parse_all_escape_characters);
    EMU_ADD(parse_n_ascii_literal__should_fail_when_quotes_are_missing);
    EMU_ADD(parse_n_ascii_literal__should_fail_when_type_suffix_is_missing);
    EMU_ADD(parse_n_ascii_literal__should_correctly_populate_the_length_of_the_string);
    EMU_END_GROUP();
}

//============================================================================//
//      STRING                                                                //
//============================================================================//
EMU_TEST(parse_n_string_literal__should_fail_to_parse_garbage)
{
    nstr_t nstr_defer_fini nstr;
    nstr_init(&nstr);
    EMU_EXPECT_LE_INT(parse_n_astr_literal(LOREM_IPSUM, &nstr), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_string_literal__should_fail_when_opening_double_quote_is_missing)
{
    nstr_t nstr_defer_fini nstr;
    nstr_init(&nstr);
    EMU_EXPECT_EQ_INT(-1, parse_n_astr_literal("foobar\"a", &nstr));
    EMU_END_TEST();
}

EMU_TEST(parse_n_string_literal__should_fail_when_closing_double_quote_is_missing)
{
    nstr_t nstr_defer_fini nstr;
    nstr_init(&nstr);
    EMU_EXPECT_EQ_INT(-1, parse_n_astr_literal("\"foobara", &nstr));
    EMU_END_TEST();
}

EMU_TEST(parse_n_string_literal__should_fail_when_type_is_missing)
{
    nstr_t nstr_defer_fini nstr;
    nstr_init(&nstr);
    EMU_EXPECT_EQ_INT(-1, parse_n_astr_literal("\"foobar\"", &nstr));
    EMU_END_TEST();
}

EMU_GROUP(parse_n_string_literal__should_fail_when_quotes_are_missing)
{
    EMU_ADD(parse_n_string_literal__should_fail_when_opening_double_quote_is_missing);
    EMU_ADD(parse_n_string_literal__should_fail_when_closing_double_quote_is_missing);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_string_literal__should_correctly_populate_the_length_of_the_string)
{
    nstr_t nstr_defer_fini nstr;
    nstr_init(&nstr);
    int len;

    len = parse_n_astr_literal("\"abcde\"a", &nstr);
    EMU_EXPECT_EQ_UINT(cstr_len("\"abcde\"a"), (unsigned)len);
    EMU_EXPECT_TRUE(cstr_eq("abcde", nstr.data));

    len = parse_n_astr_literal("\"\\a\\b\\t\\v\"a", &nstr);
    EMU_EXPECT_EQ_UINT(cstr_len("\"\\a\\b\\t\\v\"a"), (unsigned)len);
    EMU_EXPECT_TRUE(cstr_eq("\a\b\t\v", nstr.data));

    len = parse_n_astr_literal("\"\\\'\\\"\"a", &nstr);
    EMU_EXPECT_EQ_UINT(cstr_len("\"\\\'\\\"\"a"), (unsigned)len);
    EMU_EXPECT_TRUE(cstr_eq("\'\"", nstr.data));

    EMU_END_TEST();
}

EMU_GROUP(parse_n_string_literal)
{
    EMU_ADD(parse_n_string_literal__should_fail_to_parse_garbage);
    EMU_ADD(parse_n_string_literal__should_fail_when_quotes_are_missing);
    EMU_ADD(parse_n_string_literal__should_fail_when_type_is_missing);
    EMU_ADD(parse_n_string_literal__should_correctly_populate_the_length_of_the_string);
    EMU_END_GROUP();
}

//============================================================================//

#define PRINT_SIZEOF(type) \
    EMU_PRINT_INDENT(); \
    printf("sizeof %-30s : %zu\n", _EMU_STRINGIFY(type), sizeof(type));

EMU_GROUP(nir_h)
{
    PRINT_SIZEOF(struct identifier);
    PRINT_SIZEOF(struct qualifiers);
    PRINT_SIZEOF(struct data_type);
    PRINT_SIZEOF(struct member_var);
    PRINT_SIZEOF(struct member_func);
    PRINT_SIZEOF(struct struct_def);
    PRINT_SIZEOF(struct func_sig);
    PRINT_SIZEOF(struct literal);
    PRINT_SIZEOF(struct expr);
    PRINT_SIZEOF(struct variable_decl);
    PRINT_SIZEOF(struct function_decl);
    PRINT_SIZEOF(struct if_stmt);
    PRINT_SIZEOF(struct loop_stmt);
    PRINT_SIZEOF(struct alias_stmt);
    PRINT_SIZEOF(struct stmt);
    PRINT_SIZEOF(struct symbol_table);
    PRINT_SIZEOF(struct symbol);
    PRINT_SIZEOF(struct scope);
    PRINT_SIZEOF(struct module);

    EMU_ADD(parse_n_ascii);
    EMU_ADD(parse_n_ascii_literal);
    EMU_ADD(parse_n_string_literal);

    EMU_END_GROUP();
}
