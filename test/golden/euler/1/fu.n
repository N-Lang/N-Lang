# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Multiples of 3 and 5
# ====================
# 
# Problem 1
# ---------
# 
# If we list all the natural numbers below 10 that are multiples of 3 or 5,
# we get 3, 5, 6 and 9. The sum of these multiples is 23.
# 
# Find the sum of all the multiples of 3 or 5 below 1000.

import "std/math.n"a;
import "std/io.n"a;
import "std/memory.n"a;

func print_u64 : (v : u64) -> void
{
    let a : mut u64 = v;
    let count : mut u = 0u;
    loop a > 0u64
    {
        a = a / 10u64;
        count = count + 1u;
    }

    let array : ^mut ascii = default_allocate(count);

    a = v;
    let j : mut u = 0u;
    loop a > 0u64
    {
        array[j] = (u64_rem(a, 10u64) + '0'a as u64) as ascii;
        a = a / 10u64;
        j = j + 1u;
    }
    loop i : mut u in [0u, count)
    {
        printch(array[count - i - 1u]);
    }

    default_deallocate(array);

    printch('\n'a);
}

func entry : () -> u
{
    let sum : mut u64 = 0u64;
    # Naive Solution
    #
    # loop i : mut u32 in [1u32, 1000u32)
    # {
    #     if (u32_mod(i, 3u32) == 0u32 || u32_mod(i, 5u32) == 0u32)
    #     {
    #         sum = sum + i as mut u64;
    #     }
    # }
    #
    # We can do better:
    #
    # sum [3,6,..1000) + sum [5,10,..1000) - sum [lcm(3,5),2*lcm(3,5),..1000)
    #
    # sum [1..n] = n * (n + 1) / 2
    #
    # This gives us the answer in O(1) time
    sum = sum + 3u64 * (999u64/3u64) * ((999u64/3u64) + 1u64) / 2u64;
    sum = sum + 5u64 * (999u64/5u64) * ((999u64/5u64) + 1u64) / 2u64;
    sum = sum - 15u64 * (999u64/15u64) * ((999u64/15u64) + 1u64) / 2u64;
    print_u64(sum);
    return 0u;
}
