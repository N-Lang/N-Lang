import "cstd/stdio.n"a;
func entry : () -> void
{
    loop true
    {
        defer
        {
            puts($"This should be printed!"a);
        }

        break;

        defer
        {
            puts($"This should never be printed!"a);
        }
    }
}
