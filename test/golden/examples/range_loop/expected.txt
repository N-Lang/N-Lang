Range-loop syntax examples:

loop i : mut u8 in [0u, 3u]
0
1
2
3

loop i : mut u8 in [0u, 3u)
0
1
2

loop i : mut u8 in (0u, 3u]
1
2
3

loop i : mut u8 in (0u, 3u)
1
2
