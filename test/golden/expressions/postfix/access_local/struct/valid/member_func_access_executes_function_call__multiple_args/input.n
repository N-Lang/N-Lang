introduce struct foo;
introduce func foo_add_two_bar : (this : ^ foo, first : u, second : u) -> u;
struct foo
{
    let bar : mut u;
    func add_two_bar = foo_add_two_bar;
}

func foo_add_two_bar : (this : ^ foo, first : u, second : u) -> u
{
    return this.bar + first + second;
}

func entry : () -> void
{
    let myfoo : foo;
    let myu : u = myfoo.add_two_bar(10u, 35u);
}
