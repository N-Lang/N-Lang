func foo : () -> void
{
}

func goo : () -> s
{
    return 1s;
}

func hoo : (arg : s) -> s
{
    return arg + 1s;
}

func eoo : (bar : s, baz : s) -> s
{
    return bar + baz + 1s;
}

func entry : () -> void
{
    foo();
    goo();
    hoo(1s);
    eoo(1s, 2s);
}
