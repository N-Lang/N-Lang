import "std/memory.n"a;

introduce struct linkedlist;

introduce func linkedlist_init : (this : ^ mut linkedlist, d : s) -> void;
introduce func linkedlist_append : (this : ^ mut linkedlist, d : s) -> void;

struct linkedlist
{
    let data : mut s;
    let next : mut ^ mut linkedlist;

    func init = linkedlist_init;
    func append = linkedlist_append;
}

func linkedlist_init : (this : ^ mut linkedlist, d : s) -> void
{
    this.data = d;
    this.next = null;
}

func linkedlist_append : (this : ^ mut linkedlist, d : s) -> void
{
    if this.next == null
    {
        this.next = allocate(sizeof(linkedlist)); this.next.init(d);
    }
    else { this.next.append(d); }
}

func entry : () -> s
{
    let l : mut linkedlist; l.init(0s);

    loop i : mut s in [1s, 10s]
    {
        l.append(i);
    }

    let winning : mut bool = true;
    let lp : mut ^ linkedlist = ?l;

    loop i : mut s in [0s, 10s]
    {
        winning = winning && lp.data == i;
        lp = lp.next;
    }

    if winning { return 0s; }
    else { return -1s; }
}
