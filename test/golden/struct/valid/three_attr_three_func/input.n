introduce struct jawn;

introduce func do_jawn1 : (this : ^ jawn) -> void;
introduce func do_jawn2 : (this : ^ jawn, a : u) -> s;
introduce func do_jawn3 : (this : ^ jawn, b : ^ ascii) -> ^ ascii;

struct jawn
{
    let a : bool;
    let b : s;
    let c : ^ ascii;

    func do_jawn1 = do_jawn1;
    func do_jawn2 = do_jawn2;
    func do_jawn3 = do_jawn3;
}
