import "std/testing.n"a;

func test_foo : (tm : ^mut test_manager) -> void
{
    tm.fail_astr($"Foo failed."a);
}

func test_bar : (tm : ^mut test_manager) -> void
{
    # This test passes.
}

func test_baz : (tm : ^mut test_manager) -> void
{
    tm.fail_astr($"Baz failed."a);
}

func entry : () -> u
{
    let tm : mut test_manager;
    tm.init();
    defer
    {
        tm.fini();
    }

    tm.add(test_foo, $"foo"a);
    tm.add(test_bar, $"bar"a);
    tm.add(test_baz, $"baz"a);

    return tm.run();
}
