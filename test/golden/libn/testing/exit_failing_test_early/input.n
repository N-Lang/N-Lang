import "std/testing.n"a;

func test_foo : (tm : ^mut test_manager) -> void
{
    let foo : ^s32 = null;

    if foo == null
    {
        # Oh no, an error has occurred and a pointer that should never ever be
        # null is in fact null. Since one of the assumptions about the nature
        # of foo is incorrect, the test must be exited before we dereference a
        # null pointer.
        tm.fail_astr($"foo should not have been a null pointer."a);
        # This return statement is used to exit the test early.
        # By placing it after the call to the failure function we signal that
        # the test has failed, provide the user an error message, and exit
        # before we run into a segmentation fault.
        return;
    }

    if @foo != 10s32
    {
        tm.fail_astr($"@foo should have been 10s32."a);
    }
}

func entry : () -> u
{
    let tm : mut test_manager;
    tm.init();
    defer
    {
        tm.fini();
    }

    tm.add(test_foo, $"foo"a);

    return tm.run();
}
