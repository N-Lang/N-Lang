import "std/testing.n"a;

func test_foo : (tm : ^mut test_manager) -> void
{
    tm.fail();
}

func entry : () -> u
{
    let tm : mut test_manager;
    tm.init();
    defer
    {
        tm.fini();
    }

    tm.add(test_foo, $"foo"a);

    return tm.run();
}
