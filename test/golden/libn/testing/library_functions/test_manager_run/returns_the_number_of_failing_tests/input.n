import "std/testing.n"a;

func test_foo : (tm : ^mut test_manager) -> void
{
    tm.fail();
}

func test_bar : (tm : ^mut test_manager) -> void
{
    tm.fail();
}

func test_baz : (tm : ^mut test_manager) -> void
{
    # This test passes.
}

func test_qux : (tm : ^mut test_manager) -> void
{
    tm.fail();
}

func entry : () -> u
{
    let tm : mut test_manager;
    tm.init();
    defer
    {
        tm.fini();
    }

    tm.add(test_foo, $"foo"a);
    tm.add(test_bar, $"bar"a);
    tm.add(test_baz, $"baz"a);
    tm.add(test_qux, $"qux"a);

    return tm.run();
}
