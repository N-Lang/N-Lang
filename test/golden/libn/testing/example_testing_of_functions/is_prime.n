# This function was written by a Joe Computerman, a first year CS student.
# Joe is new to programming so someone should write a test to make sure Joe's
# function works as expected.
export func is_prime : (val : u64) -> bool
{
    if (val == 2u64 ||
        val == 3u64 ||
        val == 5u64 ||
        val == 7u64 ||
        val == 11u64)
        # TODO: Add the rest of the cases.
        #           - Joe Computerman
    {
        return true;
    }
    return false;
}
