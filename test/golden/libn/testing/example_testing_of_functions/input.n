import "std/testing.n"a;
import "is_prime.n"a;

func test_is_prime_does_not_return_true_for_1 : (tm : ^mut test_manager) -> void
{
    if (is_prime(1u64))
    {
        tm.fail();
    }
}

func test_is_prime_returns_true_for_2 : (tm : ^mut test_manager) -> void
{
    if (!is_prime(2u64))
    {
        tm.fail();
    }
}

func test_is_prime_does_not_return_true_for_even_numbers_other_than_2 :
    (tm : ^mut test_manager) -> void
{
    if (is_prime(4u64))
    {
        tm.fail_astr($"4 should not be prime."a);
    }
    if (is_prime(6u64))
    {
        tm.fail_astr($"6 should not be prime."a);
    }
    if (is_prime(100u64))
    {
        tm.fail_astr($"100 should not be prime."a);
    }
    if (is_prime(1234567890u64))
    {
        tm.fail_astr($"1234567890 should not be prime."a);
    }
}

func test_is_prime_returns_true_for_odd_prime_numbers :
    (tm : ^mut test_manager) -> void
{
    if (!is_prime(3u64))
    {
        tm.fail_astr($"3 should be prime."a);
    }
    if (!is_prime(5u64))
    {
        tm.fail_astr($"5 should be prime."a);
    }
    if (!is_prime(7u64))
    {
        tm.fail_astr($"7 should be prime."a);
    }
    if (!is_prime(131u64))
    {
        tm.fail_astr($"131 should be prime."a);
    }
}

func entry : () -> u
{
    let tm : mut test_manager;
    tm.init();
    defer
    {
        tm.fini();
    }

    tm.add(
        test_is_prime_does_not_return_true_for_1,
        $"test_is_prime_does_not_return_true_for_1"a);
    tm.add(
        test_is_prime_returns_true_for_2,
        $"test_is_prime_returns_true_for_2"a);
    tm.add(
        test_is_prime_does_not_return_true_for_even_numbers_other_than_2,
        $"test_is_prime_does_not_return_true_for_even_numbers_other_than_2"a);
    tm.add(
        test_is_prime_returns_true_for_odd_prime_numbers,
        $"test_is_prime_returns_true_for_odd_prime_numbers"a);

    return tm.run();
}
