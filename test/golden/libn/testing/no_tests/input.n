import "std/testing.n"a;

func entry : () -> u
{
    let tm : mut test_manager;
    tm.init();
    defer
    {
        tm.fini();
    }

    return tm.run();
}
