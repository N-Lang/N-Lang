# This is a commented line!
let foo : u;

## Here is another comment, with two #s on the same line.
func bar : () -> void
{
    ### # And another commented line # just # to # be # sure #
    return;
    ## Closing comment!
}
