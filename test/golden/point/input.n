introduce struct point;

introduce func distance : (a : ^ point, b : ^ point) -> s;

struct point
{
    let x : s;
    let y : s;

    func distance_from = distance;
}

