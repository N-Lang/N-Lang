alias alias_func0 : () -> void;
alias alias_func1 : () -> s32;
alias alias_func3 : (u) -> void;
alias alias_func4 : (u) -> s32;
alias alias_func5 : (u, s64, ascii) -> s32;
