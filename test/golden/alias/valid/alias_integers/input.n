alias alias_u8 : u8;
alias alias_u16 : u16;
alias alias_u32 : u32;
alias alias_u64 : u64;
alias alias_u : u;

alias alias_s8 : s8;
alias alias_s16 : s16;
alias alias_s32 : s32;
alias alias_s64 : s64;
alias alias_s : s;
