import "std/io.n"a;

enum jawn
{
    jawn_a = 0i;
    jawn_b;
    jawn_c;
    jawn_d;
}

func entry : () -> void
{
    let j : mut jawn = jawn.jawn_a;
    j = jawn.jawn_b;
    let k : jawn = j;

    if j == k
    {
        println($"success"a);
    }
}
