variant foo
{
    sometag
    {
        let member : mut u;
    }
}

func entry : () -> void
{
    let myfoo : foo;
    myfoo.sometag.member = 10u;
}
