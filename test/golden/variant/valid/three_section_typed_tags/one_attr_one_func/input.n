introduce variant jawn;

introduce func do_jawn : (this : ^ jawn) -> void;
introduce func do_jawn_tag_1 : (this : ^ jawn) -> void;
introduce func do_jawn_tag_2 : (this : ^ jawn) -> void;
introduce func do_jawn_tag_3 : (this : ^ jawn) -> void;

variant jawn : s32
{
    let a : u;
    func do_jawn = do_jawn;

    tag_1 = 3s32
    {
        let b : u;
        func do_jawn_tag_1 = do_jawn_tag_1;
    }

    tag_2 = 5s32
    {
        let c: u8;
        func do_jawn_tag_2 = do_jawn_tag_2;
    }

    tag_3 = 7s32
    {
        let b : u16;
        func do_jawn_tag_3 = do_jawn_tag_3;
    }
}
