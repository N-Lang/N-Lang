
/* variant */ struct jawn;

static  void  do_jawn(/* variant */struct jawn const * const(this));
static  void  do_jawn_tag_1(/* variant */struct jawn const * const(this));
static  void  do_jawn_tag_2(/* variant */struct jawn const * const(this));
static  void  do_jawn_tag_3(/* variant */struct jawn const * const(this));
/* variant */ struct jawn
{
    n_u  const (a);
    n_s32  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_u  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u8  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_u16  const (b);
        } tag_3;
    };
};

