variant jawn : u {
    let a : bool;
    let b : s;
    let c : ^ ascii;

    tag_1 = 3u
    {
        let d : bool;
        let e : u;
        let f : ^ ascii;
    }

    tag_2 = 5u
    {
        let d : ascii;
        let e : s;
        let f : u64;
    }

    tag_3 = 7u
    {
        let d : s32;
        let e : u16;
        let f : ^ ascii;
    }
}
