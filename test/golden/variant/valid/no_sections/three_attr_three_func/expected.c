
/* variant */ struct jawn;

static  void  do_jawn1(/* variant */struct jawn const * const(this));
static  n_s const  do_jawn2(/* variant */struct jawn const * const(this), n_u const (a));
static  n_ascii const * const do_jawn3(/* variant */struct jawn const * const(this), n_ascii const * const(b));
/* variant */ struct jawn
{
    n_bool  const (a);
    n_s  const (b);
    n_ascii  const *  const(c);
    n_u16  (tag);

    /* tags */ union
    {
    };
};

