
/* variant */ struct jawn;

static  void  do_jawn(/* variant */struct jawn const * const(this));
static  void  do_jawn_tag_1(/* variant */struct jawn const * const(this));
/* variant */ struct jawn
{
    n_u  const (a);
    n_u16  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_u  const (b);
        } tag_1;
    };
};

