
/* variant */ struct jawn
{
    n_bool  const (a);
    n_s  const (b);
    n_ascii  const *  const(c);
    n_u16  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_bool  const (d);
            n_u  const (e);
            n_ascii  const *  const(f);
        } tag1;
    };
};

