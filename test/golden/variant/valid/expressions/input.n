import "std/io.n"a;

introduce variant jawn;

introduce func jawn_do_thing : (this : ^ jawn) -> void;
introduce func jawn_tag_1_do_thing : (this : ^ jawn) -> void;
introduce func jawn_tag_2_do_thing : (this : ^ jawn) -> void;

introduce func jawn_tag_1_print : (this : ^ jawn) -> void;
introduce func jawn_tag_2_print : (this : ^ jawn) -> void;

variant jawn
{
    let a : mut ^ascii;

    func do_thing = jawn_do_thing;
    iface print : (^jawn) -> void;

    tag_1
    {
        let b : mut ^ascii;
        func print = jawn_tag_1_print;
        func do_thing = jawn_tag_1_do_thing;
    }

    tag_2
    {
        let c : mut ^ascii;
        func print = jawn_tag_2_print;
        func do_thing = jawn_tag_2_do_thing;
    }
}

func jawn_do_thing : (this : ^jawn) -> void
{
    println($"jawn do thing"a);
    println(this.a);
}

func jawn_tag_1_do_thing : (this : ^jawn) -> void
{
    println($"jawn tag 1 do thing"a);
    println(this.tag_1.b);
}

func jawn_tag_2_do_thing : (this : ^jawn) -> void
{
    println($"jawn tag 2 do thing"a);
    println(this.tag_2.c);
}

func jawn_tag_1_print : (this : ^jawn) -> void
{
    let place : mut u = 0u;
    loop this.a[place] != '\0'a
    {
        printch(this.a[place]);
        place = place + 1u;
    }
    place = 0u;
    printch('\n'a);
    loop this.tag_1.b[place] != '\0'a
    {
        printch(this.tag_1.b[place]);
        place = place + 1u;
    }
    printch('\n'a);
}

func jawn_tag_2_print : (this : ^jawn) -> void
{
    let place : mut u = 0u;
    loop this.a[place] != '\0'a
    {
        printch(this.a[place]);
        place = place + 1u;
    }
    place = 0u;
    printch('\n'a);
    loop this.tag_2.c[place] != '\0'a
    {
        printch(this.tag_2.c[place]);
        place = place + 1u;
    }
    printch('\n'a);
}

func entry : () -> void
{
    let j : mut jawn;
    j.tag = jawn.tag_1;
    j.a = $"jawn a"a;
    j.tag_1.b = $"jawn b"a;

    j.do_thing();

    j.print();
    j.tag_1.do_thing();

    j.tag = jawn.tag_2;
    j.tag_2.c = $"jawn c"a;
    j.print();
    j.tag_2.do_thing();
}
