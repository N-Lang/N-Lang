variant jawn : u16
{
    let a : u;

    tag_1a = 5u16, tag_1b
    {
        let b : s;
    }

    tag_2a = 7u16, tag_2b
    {
        let c : u;
    }

    tag_3a = 9u16, tag_3b
    {
        let d : s;
    }
}
