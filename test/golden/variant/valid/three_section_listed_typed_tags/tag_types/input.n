variant jawn1 : u8
{
    let a : u;

    tag_1a = 5u8, tag_1b = 6u8
    {
        let b : s;
    }

    tag_2a = 7u8, tag_2b = 8u8
    {
        let c : u;
    }

    tag_3a = 9u8, tag_3b = 10u8
    {
        let d : s;
    }
}

variant jawn2 : u16
{
    let a : u;

    tag_1a = 5u16, tag_1b = 6u16
    {
        let b : s;
    }

    tag_2a = 7u16, tag_2b = 8u16
    {
        let c : u;
    }

    tag_3a = 9u16, tag_3b = 10u16
    {
        let d : s;
    }
}

variant jawn3 : u32
{
    let a : u;

    tag_1a = 5u32, tag_1b = 6u32
    {
        let b : s;
    }

    tag_2a = 7u32, tag_2b = 8u32
    {
        let c : u;
    }

    tag_3a = 9u32, tag_3b = 10u32
    {
        let d : s;
    }
}

variant jawn4 : u64
{
    let a : u;

    tag_1a = 5u64, tag_1b = 6u64
    {
        let b : s;
    }

    tag_2a = 7u64, tag_2b = 8u64
    {
        let c : u;
    }

    tag_3a = 9u64, tag_3b = 10u64
    {
        let d : s;
    }
}

variant jawn5 : u
{
    let a : u;

    tag_1a = 5u, tag_1b = 6u
    {
        let b : s;
    }

    tag_2a = 7u, tag_2b = 8u
    {
        let c : u;
    }

    tag_3a = 9u, tag_3b = 10u
    {
        let d : s;
    }
}

variant jawn6 : s8
{
    let a : u;

    tag_1a = 5s8, tag_1b = 6s8
    {
        let b : s;
    }

    tag_2a = 7s8, tag_2b = 8s8
    {
        let c : u;
    }

    tag_3a = 9s8, tag_3b = 10s8
    {
        let d : s;
    }
}

variant jawn7: s16
{
    let a : u;

    tag_1a = 5s16, tag_1b = 6s16
    {
        let b : s;
    }

    tag_2a = 7s16, tag_2b = 8s16
    {
        let c : u;
    }

    tag_3a = 9s16, tag_3b = 10s16
    {
        let d : s;
    }
}

variant jawn8 : s32
{
    let a : u;

    tag_1a = 5s32, tag_1b = 6s32
    {
        let b : s;
    }

    tag_2a = 7s32, tag_2b = 8s32
    {
        let c : u;
    }

    tag_3a = 9s32, tag_3b = 10s32
    {
        let d : s;
    }
}

variant jawn9 : s64
{
    let a : u;

    tag_1a = 5s64, tag_1b = 6s64
    {
        let b : s;
    }

    tag_2a = 7s64, tag_2b = 8s64
    {
        let c : u;
    }

    tag_3a = 9s64, tag_3b = 10s64
    {
        let d : s;
    }
}

variant jawn10 : s
{
    let a : u;

    tag_1a = 5s, tag_1b = 6s
    {
        let b : s;
    }

    tag_2a = 7s, tag_2b = 8s
    {
        let c : u;
    }

    tag_3a = 9s, tag_3b = 10s
    {
        let d : s;
    }
}
