
/* variant */ struct jawn;

static  void  do_jawn1(/* variant */struct jawn const * const(this));
static  n_s const  do_jawn2(/* variant */struct jawn const * const(this), n_u const (a));
static  n_ascii const * const do_jawn3(/* variant */struct jawn const * const(this), n_ascii const * const(b));
static  void  do_jawn1_tag_1(/* variant */struct jawn const * const(this));
static  n_u const  do_jawn2_tag_1(/* variant */struct jawn const * const(this), n_s const (a));
static  n_ascii const * const do_jawn3_tag_1(/* variant */struct jawn const * const(this), n_ascii const * const(b));
static  void  do_jawn1_tag_2(/* variant */struct jawn const * const(this));
static  n_bool const  do_jawn2_tag_2(/* variant */struct jawn const * const(this), n_u const (a));
static  n_s const  do_jawn3_tag_2(/* variant */struct jawn const * const(this), n_ascii const * const(b));
static  void  do_jawn1_tag_3(/* variant */struct jawn const * const(this));
static  n_bool const  do_jawn2_tag_3(/* variant */struct jawn const * const(this), n_s64 const (a));
static  n_ascii const * const do_jawn3_tag_3(/* variant */struct jawn const * const(this), n_bool const (b));
/* variant */ struct jawn
{
    n_bool  const (a);
    n_s  const (b);
    n_ascii  const *  const(c);
    n_u16  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_bool  const (d);
            n_u  const (e);
            n_ascii  const *  const(f);
        } tag_1a;
        /* tag */ struct
        {
            n_bool  const (d);
            n_u  const (e);
            n_ascii  const *  const(f);
        } tag_1b;
        /* tag */ struct
        {
            n_s32  const (d);
            n_bool  const (e);
            n_ascii  const (f);
        } tag_2a;
        /* tag */ struct
        {
            n_s32  const (d);
            n_bool  const (e);
            n_ascii  const (f);
        } tag_2b;
        /* tag */ struct
        {
            n_s8  const (d);
            n_u16  const (e);
            n_s32  const (f);
        } tag_3a;
        /* tag */ struct
        {
            n_s8  const (d);
            n_u16  const (e);
            n_s32  const (f);
        } tag_3b;
    };
};

