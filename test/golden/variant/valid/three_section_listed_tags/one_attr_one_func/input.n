introduce variant jawn;

introduce func do_jawn : (this : ^ jawn) -> void;
introduce func do_jawn_tag_1 : (this : ^ jawn) -> void;
introduce func do_jawn_tag_2 : (this : ^ jawn) -> void;
introduce func do_jawn_tag_3 : (this : ^ jawn) -> void;

variant jawn
{
    let a : u;
    func do_jawn = do_jawn;

    tag_1a, tab_1b
    {
        let b : u;
        func do_jawn_tag_1 = do_jawn_tag_1;
    }

    tag_2a, tag_2b
    {
        let c: u8;
        func do_jawn_tag_2 = do_jawn_tag_2;
    }

    tag_3a, tag_3b
    {
        let b : u16;
        func do_jawn_tag_3 = do_jawn_tag_3;
    }
}
