import "std/memory.n"a;

introduce variant linkedlist;

introduce func linkedlist_init : (this : ^ mut linkedlist, d : s) -> void;
introduce func linkedlist_append : (this : ^ mut linkedlist, d : s) -> void;

variant linkedlist
{
    let data : mut s;
    mid
    {
        let next : mut ^ mut linkedlist;
    }
    end { }

    func init = linkedlist_init;
    func append = linkedlist_append;
}

func linkedlist_init : (this : ^ mut linkedlist, d : s) -> void
{
    this.data = d;
    this.tag = linkedlist.end;
}

func linkedlist_append : (this : ^ mut linkedlist, d : s) -> void
{
    if this.tag == linkedlist.end
    {
        this.tag = linkedlist.mid;
        this.mid.next = allocate(sizeof(linkedlist)); this.mid.next.init(d);
    }
    else { this.mid.next.append(d); }
}

func entry : () -> s
{
    let l : mut linkedlist; l.init(0s);

    loop i : mut s in [1s, 10s]
    {
        l.append(i);
    }

    let winning : mut bool = true;
    let lp : mut ^ linkedlist = ?l;

    loop i : mut s in [0s, 10s]
    {
        winning = winning && lp.data == i;
        lp = lp.mid.next;
    }

    if winning { return 0s; }
    else { return -1s; }
}
