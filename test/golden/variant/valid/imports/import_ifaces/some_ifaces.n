import "std/io.n"a;

export introduce variant jawn;

export introduce func jawn_tag_1_print : (this : ^ jawn) -> void;
export introduce func jawn_tag_2_print : (this : ^ jawn) -> void;

export variant jawn
{
    let a : mut ^ ascii;
    iface print : (^ jawn) -> void;
    tag_1
    {
        let b : mut ^ ascii;
        func print = jawn_tag_1_print;
    }
    tag_2
    {
        let c : mut ^ ascii;
        func print = jawn_tag_2_print;
    }
}

export func jawn_tag_1_print : (this : ^ jawn) -> void
{
    println(this.a);
    println(this.tag_1.b);
}

export func jawn_tag_2_print : (this : ^ jawn) -> void
{
    println(this.a);
    println(this.tag_2.c);
}
