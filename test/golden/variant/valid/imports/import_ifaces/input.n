import "some_ifaces.n"a;

let j : mut jawn;

func entry : () -> void
{
    j.a = $"jawn a"a;
    j.tag = jawn.tag_1;
    j.tag_1.b = $"jawn b"a;
    j.print();
    j.tag = jawn.tag_2;
    j.tag_2.c = $"jawn c"a;
    j.print();

}
