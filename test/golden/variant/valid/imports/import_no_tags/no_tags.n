import "std/io.n"a;

export introduce variant jawn;

export introduce func jawn_print : (this : ^ jawn) -> void;

export variant jawn
{
    let a : mut ^ ascii;
    func print = jawn_print;
}

func jawn_print : (this : ^ jawn) -> void
{
    println(this.a);
}
