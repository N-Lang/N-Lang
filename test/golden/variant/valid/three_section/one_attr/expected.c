
/* variant */ struct jawn
{
    n_u  const (a);
    n_u16  (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
            n_s  const (b);
        } tag_1;
        /* tag */ struct
        {
            n_u  const (c);
        } tag_2;
        /* tag */ struct
        {
            n_s  const (d);
        } tag_3;
    };
};

