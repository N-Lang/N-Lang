
/* variant */ struct jawn;

static  void  jawn_tag_1_do_1(/* variant */struct jawn const * const(this));
static  void  jawn_tag_2_do_1(/* variant */struct jawn const * const(this));
static  void  jawn_tag_3_do_1(/* variant */struct jawn const * const(this));
/* variant */ struct jawn
{
    n_u const (a);
    n_u16 (tag);

    /* tags */ union
    {
        /* tag */ struct
        {
        } tag_1;
        /* tag */ struct
        {
        } tag_2;
        /* tag */ struct
        {
        } tag_3;
    };
};

