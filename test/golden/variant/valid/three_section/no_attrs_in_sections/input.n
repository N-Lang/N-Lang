introduce variant jawn;

introduce func jawn_tag_1_do_1 : (this : ^ jawn) -> void;
introduce func jawn_tag_2_do_1 : (this : ^ jawn) -> void;
introduce func jawn_tag_3_do_1 : (this : ^ jawn) -> void;

variant jawn
{
    let a : u;

    tag_1
    {
        func do_1 = jawn_tag_1_do_1;
    }
    tag_2
    {
        func do_1 = jawn_tag_2_do_1;
    }
    tag_3
    {
        func do_1 = jawn_tag_3_do_1;
    }
}
