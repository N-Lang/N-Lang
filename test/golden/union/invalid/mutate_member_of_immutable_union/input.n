union foo
{
    let member : mut u;
}

func entry : () -> void
{
    let myfoo : foo;
    myfoo.member = 10u;
}
