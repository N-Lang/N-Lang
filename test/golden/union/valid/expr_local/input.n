import "std/io.n"a;

union jawn
{
    let a : mut bool;
    let b : mut ^ ascii;
}

func entry : () -> void
{
    let j : mut jawn;
    j.a = true;
    if j.a
    {
        j.b = $"success"a;
    }
    else
    {
        j.b = $"failure"a;
    }
    println(j.b);
}
