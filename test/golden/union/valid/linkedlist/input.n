import "std/memory.n"a;

union linkedlist
{
    let data : mut s;
    let next : mut ^ mut linkedlist;
}

func linkedlist_init_data : (this : ^ mut linkedlist, d : s) -> void
{
    this.data = d;
}

func linkedlist_init_list : (this : ^ mut linkedlist, next : ^ mut linkedlist) -> void
{
    this.next = next;
}

func entry : () -> s
{
    let l : mut ^ mut linkedlist = allocate(sizeof(linkedlist));
    linkedlist_init_data(l, 0x1337s);

    loop i : mut s in [1s, 10s]
    {
        let new : ^ mut linkedlist = allocate(sizeof(linkedlist));
        linkedlist_init_list(new, l);
        l = new;
    }

    loop i : mut s in [1s, 10s]
    {
        l = l.next;
    }

    if l.data == 0x1337s { return 0s; } else { return -1s; }
}
