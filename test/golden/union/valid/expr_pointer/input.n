import "std/io.n"a;

union jawn
{
    let a : mut bool;
    let b : mut ^ ascii;
}

func entry : () -> void
{
    let k : mut jawn;
    let j : ^ mut jawn = ?k;
    j.a = true;
    if j.a
    {
        j.b = $"success"a;
    }
    else
    {
        j.b = $"failure"a;
    }
    println(j.b);
}
