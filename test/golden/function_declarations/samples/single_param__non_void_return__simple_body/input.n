func foo : (some_param : s) -> s
{
    return some_param;
}

func goo : (some_param : u) -> u
{
    return some_param + 1u;
}
