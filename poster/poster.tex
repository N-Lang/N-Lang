%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% baposter Portrait Poster
% LaTeX Template
% Version 1.0 (15/5/13)
%
% Created by:
% Brian Amberg (baposter@brian-amberg.de)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[paperwidth=36in, paperheight=48in]{baposter}

\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{relsize} % Used for making text smaller in some places
\usepackage{wrapfig}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{attgram}
\usepackage{subfig}
\usepackage{multicol}
\usepackage{vwcol}
\usepackage{alltt}
\usetikzlibrary{arrows,positioning}

\definecolor{bordercol}{RGB}{40,40,40} % Border color of content boxes
% \definecolor{headercol1}{RGB}{186,215,230} % Background color for the header in the content boxes (left side)
\definecolor{headercol1}{RGB}{210,120,180}
% \definecolor{headercol2}{RGB}{247, 189, 1}
% \definecolor{headercol2}{RGB}{80,80,80} % Background color for the header in the content boxes (right side)
\definecolor{headercol2}{RGB}{200, 90, 65}
\definecolor{headerfontcol}{RGB}{0,0,0} % Text color for the header text in the content boxes
% \definecolor{boxcolor}{RGB}{186,215,230} % Background color for the content in the content boxes
\definecolor{boxcolor}{RGB}{150, 210, 220}

\begin{document}

\begin{poster}{
grid=false,
borderColor=bordercol, % Border color of content boxes
headerColorOne=headercol1, % Background color for the header in the content boxes (left side)
headerColorTwo=headercol2, % Background color for the header in the content boxes (right side)
headerFontColor=headerfontcol, % Text color for the header text in the content boxes
boxColorOne=boxcolor, % Background color for the content in the content boxes
headershape=roundedright, % Specify the rounded corner in the content box headers
headerfont=\Large\sf\bf, % Font modifiers for the text in the content box headers
textborder=rectangle,
background=none,
headerborder=closed, % Change to closed for a line under the content box headers
boxshade=plain
}
{}
%
%----------------------------------------------------------------------------------------
%	TITLE AND AUTHOR NAME
%----------------------------------------------------------------------------------------
%
{\sf\bf The N Programming Language} % Poster title
{\vspace{1em} A. Shin, J. Thomas, K. Model, M. Minnick, S. Gupta, S. Shanmugam, V. Cushman, V. Zhou.\\
{\smaller Drexel University, College of Computing and Informatics}}
{\includegraphics[scale=0.20]{target/logo-wide.png}}

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\headerbox{Introduction}{name=introduction,column=0,row=0}{

{\bf {\LARGE T}he} N Language team aims to develop a specification, compiler, and associated tooling for a modern high-performance systems programming language with minimal undefined behavior and expressive standard library. N seeks to enable compatibility with existing libraries and object code, allowing for zero-cost bindings to any language exposing functionality via the C calling convention. The language is designed with simplicity, performance, and programmer ergonomics in mind, delivering an experience that combines the ease of development in high-level languages with the control and bare metal performance of languages such as C, C++, or Rust.
}

%----------------------------------------------------------------------------------------
%	EXAMPLE CODE - Hello World
%----------------------------------------------------------------------------------------

\headerbox{Hello, World!}{name=hello,column=0,below=introduction}{
{\bf{\LARGE C}ompilation} of the traditional ``Hello, World!'' program using the N
compiler, \texttt{nompile}.

\begin{center}
\includegraphics[width=0.85\linewidth]{png/hello-code.png}
\captionof{figure}{Hello World source code.}
\end{center}
\begin{center}
\includegraphics[width=0.85\linewidth]{png/hello-compiled.png}
\captionof{figure}{Compiled Hello World.}
\end{center}

}

%----------------------------------------------------------------------------------------
% EEXAMPLE CODE - Mandelbrot
%----------------------------------------------------------------------------------------
\headerbox{Generated Mandelbrot Set}{name=mand,column=0,below=hello}{
{\bf{\LARGE G}eneration} of an ASCII Mandelbrot set using N. The program producing this image is used for testing performance characteristics of the compiler, and demonstrates looping and conditional branching constructs as a proof of Turing completeness. 

\begin{center}
\includegraphics[width=0.85\linewidth]{png/Mandelbrot.png}
\captionof{figure}{Output of Mandelbrot Demo Program.}
\end{center}
}


%----------------------------------------------------------------------------------------
%	REFERENCES & ACKNOWLEDGMENTS
%----------------------------------------------------------------------------------------

\headerbox{References}{name=references,column=0,below=mand}{

\smaller % Reduce the font size in this block
\renewcommand{\section}[2]{\vskip 0.05em} % Get rid of the default "References" section title
\nocite{*} % Insert publications even if they are not cited in the poster

\bibliographystyle{unsrt}
\bibliography{ref}
}



%----------------------------------------------------------------------------------------
%	COMPILER PIPELINE
%----------------------------------------------------------------------------------------

\headerbox{Compiler Pipeline}{name=pipeline,span=2,column=1,row=0}{
% To reduce this block to 1 column width, remove 'span=2'
%------------------------------------------------

{\bf{\LARGE T}he} N compiler, \texttt{nompile}, consists of a five stage front end
that translates source files from N into C99. Generated C99 code may be
optionally compiled to native code using the POSIX \texttt{c99} utility as a compiler back end.

Compilation starts with lexing where a source file is passed through a
deterministic finite automaton splitting the contents of the source file into
a stream of tokens, the fundamental units of text recognized by the compiler
including keywords, identifiers, numbers, and strings.
                                                                                 
The parsing / lowing phases of the compiler transforms the token stream produced  
by the lexing phase into an tree-like in-memory representation of an
N program known as the N-Intermediate-Representation (NIR).                                 
The \texttt{nompile} parser implements the Earley Algorithm\cite{Earley1970},       
forming an abstract syntax tree (AST) from a valid sequence of tokens recognized 
by the N language.                                                               
This AST is converted into the NIR used by subsequent phases of the compiler
shortly thereafter through the process of lowering.
                                                                                 
The semantic analysis phase of the compiler performs a single depth first           
traversal over the NIR, checking program validity and constructing auxiliary        
data structures to accompany the NIR including a cache of all imported /            
compiled files and various symbol tables required for name and type resolution.  
                                                                                    
The code generation phase of the compiler performs another depth first traversal 
over the NIR, mapping the NIR onto equivalent C99\cite{ISO:C99} constructs. The generated C code is semantically equivalent to the        
source program and may be compiled and linked during compilation via the POSIX   
\texttt{c99} utility or written to regular file to be compiled at a later time.
\begin{center}
\includegraphics[width=0.80\linewidth]{target/nompile_pipeline.png}
\captionof{figure}{Pipeline of the N compiler \texttt{nompile}.}
\end{center}

}
%  The N Programming Language is formally specified using tools well understood in academic literature.

%\begin{itemize}
 %   \item Our lexical analyzer is specified formally using a Parsing Expression Grammar (PEG). The implementation is a hand-rolled lexer that can achieve speeds of 1GB/s in benchmarks.
 %   \item Our syntax analyzer is specified formally using a Context Free Grammar. The implementation is an optimized Earley Parser that can identify ambiguities in the grammar with ease.
%    \item Our semantic analyzer is formally reasoned with using attribute grammars.
%\end{itemize}

%----------------------------------------------------------------------------------------
%	Formally Specifying a Language
%----------------------------------------------------------------------------------------
\headerbox{Formally Specifying a Language}{name=spec,span=2,column=1,below=pipeline} {
\begin{multicols}{2}
{\bf{\LARGE W}hen} specifying the N Language our team synthesized the formal language semantics using context free grammars (CFG) for the syntax and attribute grammars -- with a few extensions -- for the semantics. \cite{knuth1968}
Practically, a CFG is a collection of productions or sequences of concrete textual symbols, such as a numeric literal, intermixed with sub-productions, such as an arithmetic expression.
A CFG is used by a parser to convert a sequence of input symbols into a syntax tree.
An AG is an extension to CFG, constructed by assigning a set of attributes to each symbol.

While the full N Language Specification would not fit here, a calculator example is shown.

The first production is a list of statements recursively defined to produce a linked list shaped syntax tree.

\begin{syntax}
    \prdn{\nt{stmts}{0}}{\nt{stmt}{0} \nt{stmts}{1}}
\end{syntax}
The linked list is terminated at the last statement.

\begin{syntax}
    \prdn{\nt{stmts}{0}}{\nt{stmt}{0}}
\end{syntax}
In this calculator, a statement assigns a value to a variable.

\begin{syntax}
    \prdn{\nt{stmt}{0}}{\id{0} \tm{:=} \nt{expr}{0} \tm{;}}
\end{syntax}
This brings the first logical extension to AG is the symbol table: a map of variable names to values, which allows reuse of named values later on.
\begin{semantic}
    \tblput{0}{\nt{expr}{0}\att{value}}
\end{semantic}

Expressions have their operator precedence encoded into the CFG.
This is because the CFG creates the syntax tree, and operator precedence is based on an operation's depth in the tree.

\begin{syntax}
    \prdn{\nt{expr}{0}}{\nt{expr}{1} \tm{+} \nt{term}{0}}
\end{syntax}
\begin{semantic}
    \attr{\nt{expr}{0}\att{value}}{\nt{expr}{1}\att{value} + \nt{term}{0}\att{value}}
\end{semantic}

\begin{syntax}
    \prdn{\nt{expr}{0}}{\nt{expr}{1} \tm{-} \nt{term}{0}}
\end{syntax}
\begin{semantic}
    \attr{\nt{expr}{0}\att{value}}{\nt{expr}{1}\att{value} - \nt{term}{0}\att{value}}
\end{semantic}

\begin{syntax}
    \prdn{\nt{expr}{0}}{\nt{term}{0}}
\end{syntax}
\begin{semantic}
    \attr{\nt{expr}{0}\att{value}}{\nt{term}{0}\att{value}}
\end{semantic}
\columnbreak

\begin{syntax}
    \prdn{\nt{term}{0}}{\nt{term}{1} \tm{*} \nt{factor}{0}}
\end{syntax}
\begin{semantic}
    \attr{\nt{term}{0}\att{value}}{\nt{term}{1}\att{value} * \nt{factor}{0}\att{value}}
\end{semantic}

\begin{syntax}
    \prdn{\nt{term}{0}}{\nt{term}{1} \tm{/} \nt{factor}{0}}
\end{syntax}
Here is the 1st ``\textbf{assert}'' statement, which, if its comparison resolves to false, will cause the language to generate a semantic error: in this case division by zero.

\begin{semantic}
    \asrt{\nt{factor}{0}\att{value} \neq 0}
    \attr{\nt{term}{0}\att{value}}{\nt{term}{1}\att{value} / \nt{factor}{0}\att{value}}
\end{semantic}

\begin{syntax}
    \prdn{\nt{term}{0}}{\nt{factor}{0}}
\end{syntax}
\begin{semantic}
    \attr{\nt{term}{0}\att{value}}{\nt{factor}{1}\att{value}}
\end{semantic}

An identifier, or variable, within an expression causes a lookup in the symbol table.

\begin{syntax}
    \prdn{\nt{factor}{0}}{\id{0}}
\end{syntax}
\begin{semantic}
    \asrt{\tblhas{0}}
    \attr{\nt{factor}{0}\att{value}}{\tblget{0}}
\end{semantic}

Literal symbols are simply parsed numerically.

\begin{syntax}
    \prdn{\nt{factor}{0}}{\st{literal}}
\end{syntax}
\begin{semantic}
    \attr{\nt{factor}{0}\att{value}}{$numericParse($\st{literal} $)$ }
\end{semantic}

Identifiers and literals are just regular expressions.
\begin{syntax}
    \prdn{\st{identifier}}{\regex{[a-zA-Z\_][a-zA-Z0-9\_]*}}
    \prdn{\st{literal}}{\regex{[+-]?[0-9]([0-9\_]*[0-9])?}}
\end{syntax}%
\begin{multicols*}{2}%
\columnbreak\includegraphics[width=.42\textwidth]{target/syntaxtree.png}%
\begin{alltt}%
a := 3;\\
b := 4;\\
c\_2 := a*a + b*b;
\end{alltt}%
\end{multicols*}%
\captionof{figure}{Example program and syntax tree.}%
\end{multicols}%
}
\end{poster}

\end{document}
