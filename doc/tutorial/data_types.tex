\chapter{Data Types}
\section{Introduction}
N-Lang requires that all variables are assigned a data type. N is a strong, statically typed language, which means that types must be known to the program at compile time. In this section, we will detail the various data types that are supported in N and how to properly use them.
\section{Primitives}

\subsection{Integer Primitives}

N provides a total of twelve integer primitives. Six of these are signed integers, while the other six are unsigned. The below chart details these options.
\begin{table}[H]
    \centering
    \caption{Range of Integer Types}
    \begin{tabular}{|l|l|l|}
        \hline
        Bitwidth       & Unsigned & Signed \\
        \hline
        8-bit          & u8     & s8       \\
        \hline
        16-bit         & u16    & s16      \\
        \hline
        32-bit         & u32    & s32      \\
        \hline
        64-bit         & u64    & s64      \\ 
        \hline
        arch-dependent & u      & s        \\
        \hline
    \end{tabular}
\end{table}

A \n{u8} integer would be an unsigned integer with a bitwidth of 8, meaning it would be able to hold values ranging from 0 to 255. An \n{s8} integer would be a signed integer with a bitwidth of 8, meaning that it can hold values between -128 and 127. 

The data types \n{u} and \n{s} are architecture dependent, and will have a bitwidth equal to the universal pointer bitwidth as defined by the architecture.

Integers can be declared by using \n{let} statements:

\n{let foo : u32  = 10u32;}

\n{let bar : s64 = -10s64;}

For more details on declaring variables, see the chapter on Variables.

\subsection{Floating Point Primitives}
Floating point numbers are a primitive data type used when you need more precision than you can get with an integer. In n, they are used similarly to the integer types described above. A float can be either \n{f32} or \n{64}.

They are declared using \n{let} statements:

\n{let foo\_float : f32 = 1.65f32;}

For more details on declaring variables, see the chapter on Variables.

\subsection{Boolean Primitive}
N provides a simple boolean primitive, which can take a true or false value. It is called \n{bool} and can be assigned to either \n{true} or \n{false}.

Booleans are declared using a let statement:

\n{let foo\_bool : bool = true;}

Boolean values are used both in variables and condition checking. \n{if} and \n{loop} statements both must contain a boolean as their condition.

\subsection{ASCII Primitive}
N provides a primitive that represents an ASCII character, called \n{ascii}. \n{ascii} is only a single character, not a string. To use a string, you will need to create an array of \n{ascii} values, as explained later.

\subsection{Function Type}

N supports functions as first class data types.

A function type consists of:
\begin{enumerate}
    \item A parameter list consisting of zero or more data types
    \item A return type
\end{enumerate}

A function type is written using the following lexical convention:
\begin{verbatim}
( PARAMETER_TYPE_LIST ) -> RETURN_TYPE
\end{verbatim}

A function type accepting single \n{ascii} parameter returning \n{u} is written
as:
\begin{verbatim}
(char) -> u
\end{verbatim}

A function type accepting an \n{ascii} parameter and a \n{s32} returning \n{u}
s written as:
\begin{verbatim}
(char, s32) -> u
\end{verbatim}

A function type that does not accept any parameters is written using an empty
pair of parentheses.
A function type accepting no parameters returning \n{u} is written as:
\begin{verbatim}
() -> u
\end{verbatim}

A function type that returns no value is given the return type \n{void}.
A function type accepting no parameters returning no value is written as:
\begin{verbatim}
() -> void
\end{verbatim}

\section{Data Type Qualifiers}

N provides several \n{qualifiers} that are used to give some quality to any data type \n{T}. Any particular data type can be qualfied by one or more qualifiers.

\subsection{Declaration vs. Assignment}

Before discussing qualifiers, it is important to note the distinction in
\n{N-Lang} between declaration and assignment. When creating a variable, you
must first declare its type. 

\n{let foo: mut u16;}

The above statement declares the variable \n{foo}, and states its type as a
mutable u16. Declaring a variable is a pre-requisite to assigning it a
value;

\n{foo = 123u16;}

This statement is an assignment. It takes the variable \n{foo} which we
declared above and assigns it the value \n{123u16}. 

The two statements above may be combined into a single line, as below:

\n{let foo: mut u16 = 123u16;}

\subsection{Qualifier: \n{mut}}

The \n{mut} qualifier is used when you want to indicate you want a mutable data type. By default, N data types are immutable, meaning that once they are assigned value that value will not be able to change. If you want to be able to edit values of your variables after assignment, you will need to use the \n{mut} qualifier.

\subsection{Qualifier: \n{volatile}}

Using the \n{volatile} qualifier tells the compiler that the value of this variable might be changed by any thread at any time, and therefore prevents certain optimizations from being done for this variable. See the section on multi-threading for more details on how to properly use this qualifier.

\subsection{Qualifier: \n{unique}}

The \n{unique} qualifier is used to optimize your N code. When you use this keyword to qualify a pointer, you are telling the compiler that this is the only access point to this object. This allows the compiler to not make extra checks, and will make your code more efficient.


