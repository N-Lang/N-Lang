#!/bin/bash

rm -f main*

cat preamble.tex > main.tex
while read p; do
	cat $p >> main.tex
done < order.txt

echo '\end{document}' >> main.tex

convert logo.svg logo.png
convert -transparent white logo.png

pdflatex main.tex
pdflatex main.tex
