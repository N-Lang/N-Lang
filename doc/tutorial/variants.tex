
\chapter{Variants}

The variant type is a custom type in N which defines a record type but
allows a selectable variation to each record. The intention of variants
is to provide a record type with a limited amount of extensibility. A
variant is structured much as if it is a group of named structs, known
as sections and identified by tags. The first of which is always the
common section, which is present and available at all times during the
lifetime of a variant instance. Subsequent sections are present only one
at a time, depending on the value of the instance's tag.

The variant type's primary use case is as an equivelant to the tagged
union in C. Anywhere a record or object's data might vary depending on
context, a variant would be applicable. There are further usecases for a
variant, which involve a bit more creativity. A variant could be used to
define a set of related, but differing, operations such as context
specific operations. The last use case of variants is as a homogenous,
yet variadic type. That is that individually, a variant can take
different forms, based on tagged sections. However, as a collection,
variants can be treated as if they are uniform.

\section{Variant Types Defined}

A variant is defined by an unbounded number of sections.
Each section must have an identifier which is unique within the containing variant.
The name of a section is referred to as a "tag".
The contents of each section is nearly identical to the contents of a struct.
A section may be identified by multiple tags, which indicates that the structure of both tag's sections are identical.
A variant may have additional data out side of any sections.
This makes it common data, which is available to all sections.
An example of a variant follows.

\begin{verbatim}
variant thingey
{
    let common_member_1 : u;
    let common_member_2 : f32;
    func common_method_1 = thingey_common_method_1;

    tag_1
    {
        let variated_member_1 : s32;
        func variated_method_1 = thingey_tag1_method_1;
    }

    tag_2, tag_3
    {
        let variated_member_2 : f64;
        let variated_member_3 : u8;
        let variated_member_4 : s16;
        func variated_method_2 = thingey_tag2_method_1;
        func variated_method_3 = thingey_tag2_method_2;
    }
    .
    .
    .
}
\end{verbatim}

\section{Syntax Conventions}

The literal values of a tag are written in the form of the variant's name
followed by the tag's name, separated by . operator.\\

Attributes and member functions of the common section may be accessed directly
using the . operator and the common attribute's name. The same may be
done to access the implicit tag attribute.

\begin{verbatim}
let t : mut thingey;
t.tag = thingey.tag_1;
t.common_member_1 = 12u;
t.common_method_1();
\end{verbatim}

Pointer member access is done in the same way, with the dot operator.
The N Compiler will automatically determine whether or not to access through
a pointer or not, when using the dot operator.\\

\begin{verbatim}
let p : ^ mut thingey = ?t;
p.tag = thingey.tag_2;
p.common_member_1 = 15u;
p.common_method_1();
\end{verbatim}

The attributes of tagged sections are also accessible programmatically.
For direct access this takes the form of using the variable name, the tag
name, and the attribute name, all separated by the . operator.\\

\begin{verbatim}
t.tag_1.variated_member_1 = -24s32;
t.tag_1.variated_method_1();
\end{verbatim}

To access the attributes of a tagged section through a pointer is very
similar. Again, simply use dot operator, and the compiler will work it out
using the type system.\\

\begin{verbatim}
p.tag_1.variated_member_1 = 42s32;
p.tag_1.variated_method_1();
\end{verbatim}

\section{Operations}

In order to define the member functions or operations of a variant, the 
function must be defined separately. Then the name of the function can be 
assigned as a member function to the variant.

\begin{verbatim}
func thingey_common_method : () -> void;
func thingey_tag_1_method  : () -> void;

variant thingey
{
    func common_method = thingey_common_method;

    tag_1
    {
        func tag_1_method = thingey_tag_1_method;
    }

}

func thingey_common_method : () -> void { ...  }
func thingey_tag_1_method : () -> void { ...  }
\end{verbatim}

One special feature of variants is interfaces, using the iface keyword,
which allow, variadic behavior to be defined in the common section. An
interface is behavior of a variant, similar to a method, but requires
each tag to define a differing behavior. To add an interface, use the
iface keyword followed by an identifier and type in the common section.
Once an interface is added to the common section, all tags must define a
method with the same name and type as the variant. The following is an
iface example.

\begin{verbatim}
variant thingey
{
    iface do_thingey : (thingey, u) -> void;

    tag_1
    {
        func do_thingey = tag1_do_thingey;
    }
    tag_2
    {
        func do_thingey = tag2_do_thingey;
    }
    tag_3
    {
    	# This one is an error because it does not implement the interface.
        func do_a_thingey = tag3_do_thingey;
    }
}
\end{verbatim}

Using an interface method allows the behavior of of a variant to vary
without having to test the .tag attribute of the variant.\\

\begin{verbatim}
let t : ^ thingey = make_a_new_thingey();
t.do_thingey(5u); # The behavior of this varies based on t's tag.
\end{verbatim}

