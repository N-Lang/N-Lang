# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "std/io.n"a;

# This function has been implemented due to a current lack of integer to ascii
# string conversion functions in the standard library at the time of writing
# this example.
# The parameter i must satisfy 0 <= i < 10 for this function to correctly print.
func doprinti : (i : u8) -> void
{
    let buf : [2u]mut ascii = ['\0'a, ..2u];
    buf[0u] = (i + ('0'a as u8)) as ascii;
    println($buf);
}

func entry : () -> void
{
    println($"Range-loop syntax examples:"a);

    println($"\nloop i : mut u8 in [0u, 3u]"a);
    loop i : mut u8 in [0u, 3u]
    {
        doprinti(i);
    }

    println($"\nloop i : mut u8 in [0u, 3u)"a);
    loop i : mut u8 in [0u, 3u)
    {
        doprinti(i);
    }

    println($"\nloop i : mut u8 in (0u, 3u]"a);
    loop i : mut u8 in (0u, 3u]
    {
        doprinti(i);
    }

    println($"\nloop i : mut u8 in (0u, 3u)"a);
    loop i : mut u8 in (0u, 3u)
    {
        doprinti(i);
    }
}
