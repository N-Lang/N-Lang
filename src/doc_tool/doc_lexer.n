import "std/io.n"a;
import "std/memory.n"a;
import "std/astring.n"a;
import "jank/array.n"a;

import "compiler.n"a;

export introduce variant doc_token;

export introduce func doc_token_print : (this : ^ doc_token) -> void;


export variant doc_token : u8
{
    #* the line of the comment this token is on.
    let line : mut u;

    func print = doc_token_print;

    #* The text of a comment.
    text
    {
        let text : mut astring;
    }

    #* A macro in the text of a comment.
    macro
    {
        let macro : mut astring;
    }

    #* Parenthesis are used by macros occasionally.
    open_paren, close_paren { }

    #* inline style delimiters.
    bold, underline, strike, open_italic, close_italic { }

    #* code blocks and inline code carries their own text.
    code, code_block
    {
        let text : mut astring;
    }

    #* block delimiters
    newline, double_newline, csv_block, open_quote_block, close_quote_block { }

    #* Tokens for CSV blocks.
    comma, quote, whitespace { }

    #* list parsing.
    ordered_list, set_ordered_list, unordered_list
    {
        let indent : mut u;
    }

    set_ordered_list
    {
        let number : mut u;
    }
}

#(**
  * print out a doc_token
  *
  * \param the token to print.
  *)#
export func doc_token_print : (this : ^ doc_token) -> void
{
    if this.tag == doc_token.text
    {
        eprint($"text: "a);
        eprintln(this.text.text.data());
    }
    elif this.tag == doc_token.macro
    {
        eprint($"macro: "a);
        eprintln(this.macro.macro.data());
    }
    elif this.tag == doc_token.open_paren
    {
        eprintln($"open paren"a);
    }
    elif this.tag == doc_token.close_paren
    {
        eprintln($"close paren"a);
    }
    elif this.tag == doc_token.bold
    {
        eprintln($"bold"a);
    }
    elif this.tag == doc_token.underline
    {
        eprintln($"underline"a);
    }
    elif this.tag == doc_token.strike
    {
        eprintln($"strike"a);
    }
    elif this.tag == doc_token.open_italic
    {
        eprintln($"open italic"a);
    }
    elif this.tag == doc_token.close_italic
    {
        eprintln($"close italic"a);
    }
    elif this.tag == doc_token.code
    {
        eprint($"code"a);
        println(this.code.text.data());
    }
    elif this.tag == doc_token.code_block
    {
        eprint($"code"a);
        eprintln(this.code.text.data());
    }
    elif this.tag == doc_token.newline
    {
        eprintln($"newline"a);
    }
    elif this.tag == doc_token.double_newline
    {
        eprintln($"double newline"a);
    }
    elif this.tag == doc_token.csv_block
    {
        eprintln($"csv block"a);
    }
    elif this.tag == doc_token.open_quote_block
    {
        eprintln($"open quote block"a);
    }
    elif this.tag == doc_token.close_quote_block
    {
        eprintln($"close quote block"a);
    }
    elif this.tag == doc_token.comma
    {
        eprintln($"comma"a);
    }
    elif this.tag == doc_token.whitespace
    {
        eprintln($"whitespace"a);
    }
    elif this.tag == doc_token.ordered_list
    {
        eprintln($"ordered list"a);
    }
    elif this.tag == doc_token.unordered_list
    {
        eprintln($"unordered list"a);
    }
    elif this.tag == doc_token.set_ordered_list
    {
        eprint($"set unordered list: "a);
        eprint_u(this.set_ordered_list.number);
    }
    else
    {
        eprintln($"unexpected token"a);
    }
}

export introduce struct doc_lexer;

export introduce func doc_lexer_init:
    (this : ^ mut doc_lexer, fname : ^ ascii) -> void;
export introduce func doc_lexer_fini : (this : ^ mut doc_lexer) -> void;

export introduce func doc_lexer_add_comment_token:
    (this : ^ mut doc_lexer, comment : ^ token) -> void;

export introduce func doc_lexer_peek:
    (this : ^ doc_lexer, chr : ascii, dist : u) -> bool;
export introduce func doc_lexer_at_end : (this : ^ doc_lexer) -> bool;
export introduce func doc_lexer_advance : (this : ^ mut doc_lexer, adv : u) -> void;
export introduce func doc_lexer_emit:
    (this : ^ mut doc_lexer, tkn : ^ mut doc_token) -> void;

export introduce func doc_lexer_warning:
    (this: ^ doc_lexer, str : ^ ascii) -> void;

export introduce func doc_lexer_lex : (this : ^ mut doc_lexer) -> void;

struct doc_lexer
{
    let text : mut astring;
    let index : mut u;
    let line : mut u;
    let token_start : mut u;
    let token_start_line : mut u;
    let tokens : mut ^ doc_token; #VLA

    let file : mut ^ ascii;
    let start_line : mut u;

    func init = doc_lexer_init;
    func fini = doc_lexer_fini;

    func add_comment_token = doc_lexer_add_comment_token;

    func peek = doc_lexer_peek;
    func at_end = doc_lexer_at_end;
    func advance = doc_lexer_advance;
    func emit = doc_lexer_emit;

    func warning = doc_lexer_warning;

    func lex = doc_lexer_lex;
}

#(*
  * Initialize a doc_lexer.
  *
  * \param the doc_lexer to initialize
  *
  * \param the name of the file being lexed.
  *)#
func doc_lexer_init : (this : ^ mut doc_lexer, fname : ^ ascii) -> void
{
    this.text.init();
    this.index = 0u;
    this.line = 0u;
    this.token_start = 0u;
    this.token_start_line = 1u;
    this.tokens = null;
    this.file = fname;
}

#(*
  * Deinitialize a doc_lexer.
  *
  * \param The doc_lexer to deinitialize.
  *)#
func doc_lexer_fini : (this : ^ mut doc_lexer) -> void
{
    this.text.fini();
}

#(*
  * Adds the text of a documentation comment to the text which will be
  * processed by this lexer. This will add text from the comment only if the
  * comment is a documentation comment, based on the presence of '*' characters
  * at the beginning of a comment. It removes these '*' characters and the
  * comment '#' character.
  * 
  * \param the doc_lexer to add text to.
  * 
  * \param the text to add.
  *)#
func doc_lexer_add_comment_token:
    (this : ^ mut doc_lexer, cmt : ^ token) -> void
{
    if this.line == 0u
    {
        this.line = cmt.line;
    }
    if @cmt.start == '#'a && cmt.start[1u] == '('a && cmt.start[2u] == '*'a
    {
        let inp : mut ^ ascii = cmt.start +^ 3u;
        let nested : mut u = 0u;
        # multiline doc comment
        loop inp -^^ cmt.start < cmt.length
        {
            if @inp == '\n'a
            {
                this.text.append_ascii('\n'a);
                inp = inp +^ 1u;
                loop inp -^^ cmt.start < cmt.length &&
                     (@inp == ' 'a || @inp == '\t'a)
                {
                    inp = inp +^ 1u;
                }
                loop @inp == '*'a
                {
                    inp = inp +^ 1u;
                }
            }
            elif @inp == '#'a && @(inp +^ 1u) == '('a
            {
                nested = nested + 1u;
                inp = inp +^ 2u;
            }
            elif nested != 0u && @inp == ')'a && @(inp +^ 1u) == '#'a
            {
                nested = nested - 1u;
                inp = inp +^ 2u;
            }
            elif nested == 0u && @inp == ')'a && @(inp +^ 1u) == '#'a
            {
                break;
            }
            elif nested == 0u
            {
                this.text.append_ascii(@inp);
                inp = inp +^ 1u;
            }
        }
    }
    elif cmt.start[0u] == '#'a && cmt.start[1u] == '*'a
    {
        # single line doc comment
        loop i : mut u in [2u, cmt.length)
        {
            this.text.append_ascii(cmt.start[i]);
        }
    }
}

#(**
   * test the next character in the text for equality with a predicted
   * character.
   *
   * \param This doc_lexer
   * 
   * \param the predicted character.
   *
   * \param the predicted location of the character, in number of characters
   * ahead of the current character.
   * 
   * \return true if the prediction is correct.
   *)#
func doc_lexer_peek:
    (this : ^ doc_lexer, chr : ascii, dist : u) -> bool
{
    if this.index + dist >= this.text.length()
    {
        return false;
    }
    else
    {
        return this.text.at(this.index + dist) == chr;
    }
}

#(**
  * Tells if the doc_lexer is at the end of its input text.
  * 
  * \param this doc_lexer.
  *
  * \return true if the end of the input has been reached.
  *)#
export func at_end : (this : ^ doc_lexer) -> bool
{
    return this.index >= this.text.length();
}

#(**
  * move the position of the doc_lexer forwards by the specified number of
  * characters.
  *
  * \param the doc_lexer to advance
  *
  * \param the number of characters to move forward by.
  *)#
func doc_lexer_advance : (this : ^ mut doc_lexer, adv : u) -> void
{
    if this.index + adv <= this.text.length()
    {
        this.index = this.index + adv;    
    }
}

#(**
  * Emit a token out.
  *
  * \param this doc lexer
  *
  * \param the token to emit.
  *)#
func doc_lexer_emit : (this : ^ mut doc_lexer, tkn : ^ mut doc_token) -> void
{
    tkn.line = this.token_start_line;
    this.token_start = this.index;
    this.token_start_line = this.line;
    this.tokens = array_push(this.tokens as mut ^ mut void, tkn);
}

#(**
  * Warn the user about an issue with the format.
  *
  * \param this doc_lexer
  *
  * \param the warning string.
  *)#
export func doc_lexer_warning:
    (this: ^ doc_lexer, str : ^ ascii) -> void
{
    eprint(this.file);
    eprintch(':'a);
    eprint_u(this.line);
    eprintln(str);
}

#(**
  * Lex the stream of characters.
  *
  * \param this doc_lexer.
  *)#
func doc_lexer_lex : (this : ^ mut doc_lexer) -> void
{
    loop this.index < this.text.length()
    {
        let t : mut doc_token;

        if this.peek('\n'a, 0u)
        {
            this.advance(1u);
            this.line = this.line + 1u;
            let wspace : mut u = 0u;
            loop this.peek(' 'a, wspace) || this.peek('\t'a, wspace)
            {
                wspace = wspace + 1u;
            }

            if this.peek('\n'a, wspace)
            {
                this.advance(wspace + 1u);
                this.line = this.line + 1u;
                t.tag = doc_token.double_newline;
                this.emit(?t);
            }
            elif (this.peek('-'a, wspace) ||
                  this.peek('*'a, wspace) ||
                  this.peek('+'a, wspace)) && (this.peek(' 'a, wspace + 1u) ||
                                                this.peek('\t'a, wspace + 1u))
            {
                this.advance(wspace + 2u);
                t.tag = doc_token.unordered_list;
                t.unordered_list.indent = wspace;
                this.emit(?t);
            }
            elif (this.peek('0'a, wspace) || this.peek('1'a, wspace) ||
                  this.peek('2'a, wspace) || this.peek('3'a, wspace) ||
                  this.peek('4'a, wspace) || this.peek('5'a, wspace) ||
                  this.peek('6'a, wspace) || this.peek('7'a, wspace) ||
                  this.peek('8'a, wspace) || this.peek('9'a, wspace))
            {
                let numeral : mut u = this.text.at(this.index + wspace) as u
                                    - '0'a as u;
                let ncnt : mut u = 1u;
                loop (this.peek('0'a, wspace + ncnt) || this.peek('1'a, wspace + ncnt) ||
                      this.peek('2'a, wspace + ncnt) || this.peek('3'a, wspace + ncnt) ||
                      this.peek('4'a, wspace + ncnt) || this.peek('5'a, wspace + ncnt) ||
                      this.peek('6'a, wspace + ncnt) || this.peek('7'a, wspace + ncnt) ||
                      this.peek('8'a, wspace + ncnt) || this.peek('9'a, wspace + ncnt))
                {
                    numeral = numeral * 10u
                              + this.text.at(this.index + wspace + ncnt) as u
                              - '0'a as u;
                    ncnt = ncnt + 1u;
                }
                if this.peek('.'a, wspace + ncnt) ||
                   this.peek(')'a, wspace + ncnt)
                {
                    this.advance(wspace + ncnt);
                    t.tag = doc_token.ordered_list;
                    t.ordered_list.indent = wspace;
                    this.emit(?t);
                }
                elif this.peek('!'a, wspace + ncnt)
                {
                    this.advance(wspace + ncnt);
                    t.tag = doc_token.set_ordered_list;
                    t.set_ordered_list.indent = wspace;
                    t.set_ordered_list.number = numeral;
                    this.emit(?t);
                }
                else
                {
                    t.tag = doc_token.newline;
                    this.emit(?t);
                }
            }
            else
            {
                t.tag = doc_token.newline;
                this.emit(?t);
            }
        }
        elif this.peek('*'a, 0u) && this.peek('*'a, 1u)
        {
            this.advance(2u);
            t.tag = doc_token.bold;
            this.emit(?t);
        }
        elif this.peek('_'a, 0u) && this.peek('_'a, 1u)
        {
            this.advance(2u);
            t.tag = doc_token.underline;
            this.emit(?t);
        }
        elif this.peek('~'a, 0u) && this.peek('~'a, 1u)
        {
            this.advance(2u);
            t.tag = doc_token.strike;
            this.emit(?t);
        }
        elif this.peek('~'a, 0u) && this.peek('/'a, 1u)
        {
            this.advance(2u);
            t.tag = doc_token.open_italic;
            this.emit(?t);
        }
        elif this.peek('/'a, 0u) && this.peek('~'a, 1u)
        {
            this.advance(2u);
            t.tag = doc_token.close_italic;
            this.emit(?t);
        }
        elif this.peek('('a, 0u)
        {
            this.advance(1u);
            t.tag = doc_token.open_paren;
            this.emit(?t);
        }
        elif this.peek(')'a, 0u)
        {
            this.advance(1u);
            t.tag = doc_token.close_paren;
            this.emit(?t);
        }
        elif this.peek(','a, 0u) && this.peek(','a, 1u) &&
             this.peek(','a, 2u)
        {
            this.advance(3u);
            t.tag = doc_token.csv_block;
            this.emit(?t);
        }
        elif this.peek('>'a, 0u) && this.peek('>'a, 1u) &&
             this.peek('>'a, 2u)
        {
            this.advance(3u);
            t.tag = doc_token.open_quote_block;
            this.emit(?t);
        }
        elif this.peek('<'a, 0u) && this.peek('<'a, 1u) &&
             this.peek('<'a, 2u)
        {
            this.advance(3u);
            t.tag = doc_token.close_quote_block;
            this.emit(?t);
        }
        elif this.peek(','a, 0u)
        {
            this.advance(1u);
            t.tag = doc_token.comma;
            this.emit(?t);
        }
        elif this.peek('"'a, 0u)
        {
            this.advance(1u);
            t.tag = doc_token.quote;
            this.emit(?t);
        }
        elif this.peek('`'a, 0u) && this.peek('`'a, 1u) &&
             this.peek('`'a, 2u)
        {
            this.advance(3u);
            t.tag = doc_token.code_block;
            t.code_block.text.init();
            loop !this.peek('`'a, 0u) && !this.peek('`'a, 1u) &&
                 !this.peek('`'a, 2u) && !this.peek('\0'a, 0u)
            {
                this.advance(1u);
                t.code_block.text.append_ascii(this.text.at(this.index));
            }
            if this.peek('\0'a, 0u)
            {
                this.warning($"Unexpected end of section reading code block"a);
            }
            else
            {
                this.advance(3u);
            }
            this.emit(?t);
        }
        elif this.peek('`'a, 0u) && this.peek('`'a, 1u)
        {
            this.advance(2u);
            t.tag = doc_token.code;
            t.code.text.init();
            loop !this.peek('`'a, 0u) && !this.peek('`'a, 1u) &&
                 !this.peek('\0'a, 0u)
            {
                t.code.text.append_ascii(this.text.at(this.index));
                this.advance(1u);
            }
            t.code.text.append_ascii(this.text.at(this.index));
            this.advance(1u);
            if this.peek('\0'a, 0u)
            {
                this.warning($"Unexpected end of section reading inline code"a);
            }
            else
            {
                this.advance(2u);
            }
            this.emit(?t);
        }
        elif this.peek('\\'a, 0u)
        {
            this.advance(1u);
            t.tag = doc_token.macro;
            t.macro.macro.init();
            loop !this.peek(' 'a, 0u) && !this.peek('\t'a, 0u) &&
                 !this.peek('\n'a, 0u) && !this.peek('('a, 0u) &&
                 !this.peek('\0'a, 0u)
            {
                t.macro.macro.append_ascii(this.text.at(this.index));
                this.advance(1u);
            }
            this.emit(?t);
        }
        else
        {
            let startIndex : u = this.index;
            loop this.peek(' 'a, 0u) || this.peek('\t'a, 0u)
            {
                this.advance(1u);
            }
            let text : mut bool = false;
            loop !this.peek('\n'a, 0u) && !this.peek(','a, 0u)  &&
                 !this.peek('"'a, 0u)  && !this.peek('\\'a, 0u) &&
                 !this.peek('('a, 0u)  && !this.peek(')'a, 0u)  &&
                 !(this.peek('`'a, 0u) && this.peek('`'a, 1u)) &&
                 !(this.peek('*'a, 0u) && this.peek('*'a, 1u)) &&
                 !(this.peek('_'a, 0u) && this.peek('_'a, 1u)) &&
                 !(this.peek('~'a, 0u) && this.peek('~'a, 1u)) &&
                 !(this.peek('~'a, 0u) && this.peek('/'a, 1u)) &&
                 !(this.peek('/'a, 0u) && this.peek('~'a, 1u)) &&
                 !(this.peek('>'a, 0u) && this.peek('>'a, 1u)
                                        && this.peek('>'a, 2u)) &&
                 !(this.peek('<'a, 0u) && this.peek('<'a, 1u)
                                        && this.peek('<'a, 2u)) &&
                 !this.peek('\0'a, 0u)
            {
                this.advance(1u);
                text = true;
            }
            if(text)
            {
                t.tag = doc_token.text;
                t.text.text.init();
                t.text.text.append_astr_n(this.text.data() +^ startIndex,
                                         this.index - startIndex);
                this.emit(?t);
            }
            else
            {
                t.tag = doc_token.whitespace;
                this.emit(?t);
            }
        }
    }
}
