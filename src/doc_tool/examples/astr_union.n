import "std/astr.n"a;
import "std/io.n"a;

#(*
  * \brief This function finds the union of all characters in two ``astr``s.
  *
  * It functions by first adding all characters of ``a`` to the new buffer,
  * if the buffer does not already contain said character. It then does the
  * same operation on b.
  *
  * \param The left hand side of the union.
  *
  * \param The right hand side of the union.
  *
  * \return A newly allocated buffer storing the result of the union operation.
  * The characters of the return buffer are guaranteed to be unique.
  *)#
func astr_union : (a : ^ ascii, b : ^ ascii) -> ^ ascii
{
    let alen : u = astr_length(a);
    let blen : u = astr_length(b);
    let ret : ^ mut ascii = allocate(alen + blen + 1u);
    memory_fill(ret, alen + blen + 1u, 0u8);

    let tmpa : mut ^ ascii = a;
    let tmpb : mut ^ ascii = b;
    let tret : mut ^ mut ascii = ret;

    loop i : mut u in [0u, alen)
    {
        let sret : mut ^ ascii = ret;
        let schar : ascii = @tmpa;
        let found : mut bool = false;
        loop sret != tret
        {
            if(schar == @sret) { found = true; break; }
            sret = sret +^ 1u;
        }
        if!found
        {
            @tret = schar;
            tret = tret +^ 1u;
        }
        tmpa = tmpa +^ 1u;
    }
    loop i : mut u in [0u, blen)
    {
        let sret : mut ^ ascii = ret;
        let schar : ascii = @tmpb;
        let found : mut bool = false;
        loop sret != tret
        {
            if(schar == @sret) { found = true; break; }
            sret = sret +^ 1u;
        }
        if!found
        {
            @tret = schar;
            tret = tret +^ 1u;
        }
        tmpb = tmpb +^ 1u;
    }
    return ret;
}

#(*
  * \brief This function finds the intersection of all characters in two
  * ``astr``s. 
  *
  * It functions by iterating over the ``a`` input, and appending that 
  * character to the output buffer, if it determines that the ``b`` buffer
  * also contains that character, and that the output buffer does not already
  * contain the character.
  *
  * \param The left hand side of the union.
  *
  * \param The right hand side of the union.
  *
  * \return A newly allocated buffer storing the result of the union operation.
  * The characters of the return buffer are guaranteed to be unique.
  *)#
func astr_intersect : (a : ^ ascii, b : ^ ascii) -> ^ ascii
{
    let alen : u = astr_length(a);
    let blen : u = astr_length(b);
    let min : mut u;
    if alen < blen { min = alen; } else { min = blen; }
    let ret : ^ mut ascii = allocate(min + 1u);
    memory_fill(ret, min + 1u, 0u8);

    let tmpa : mut ^ ascii = a;
    let tret : mut ^ mut ascii = ret;

    loop i : mut u in [0u, alen)
    {
        let sret : mut ^ ascii = ret;
        let sb : mut ^ ascii = b;
        let schar : ascii = @tmpa;
        let bfound : mut bool = false;
        let rfound : mut bool = false;
        loop j : mut u in [0u, blen)
        {
            if schar == @sb { bfound = true; break; }
            sb = sb +^ 1u;

        }
        loop sret != tret
        {
            if(schar == @sret) { rfound = true; break; }
            sret = sret +^ 1u;
        }
        if bfound && !rfound
        {
            @tret = schar;
            tret = tret +^ 1u;
        }
        tmpa = tmpa +^ 1u;
    }
    return ret;
}

func entry : (argc : u, argv : ^ ^ ascii) -> void
{
    println(astr_union(argv[1u], argv[2u]));
    println(astr_intersect(argv[1u], argv[2u]));
}
