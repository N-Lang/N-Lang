import "std/astring.n"a;
import "std/io.n"a;

import "doc_rep.n"a;
import "doc_lexer.n"a;

export introduce struct doc_parser;

export introduce func doc_parser_init:
    (this : mut ^ mut doc_parser,
     sec : ^ mut doc_section,
     file : ^ ascii) -> void;
export introduce func doc_parser_advance:
    (this : ^ mut doc_parser, adv : u) -> void;
export introduce func doc_parser_peek:
    (this : ^ doc_parser, tkn : u8, dst : u) -> bool;
export introduce func doc_parser_at_end : (this : ^ doc_parser) -> bool;
export introduce func doc_parser_parse:
    (this : ^ mut doc_parser) -> void;
export introduce func doc_parser_warning:
    (this : ^ doc_parser, str : ^ ascii) -> void;

#* Represents a parser for ``doc_tokens`` within a documentation comment
export struct doc_parser
{
    #* The ``doc_token``s being parsed
    let tokens : mut ^ doc_token; # VLA
    #* The current location within the sequence of tokens
    let index : mut u;
    #* the name of the file being parsed
    let file : mut ^ ascii;
    #* the doc_section being parsed
    let section : mut ^ mut doc_section;

    func init = doc_parser_init;
    func advance = doc_parser_advance;
    func peek = doc_parser_peek;
    func at_end = doc_parser_at_end;
    func warning = doc_parser_warning;
    func parse = doc_parser_parse;
}

#(*
  * initialize a doc_parser.
  *
  * \param this doc_parser.
  *
  * \param the doc_section which will be filled by this parser.
  *
  * \param the name of the file being parsed.
  *)#
export func doc_parser_init:
    (this : ^ mut doc_parser,
     sec : ^ mut doc_section,
     file : ^ ascii) -> void
{
    this.tokens = sec.tokens;
    this.section = sec;
    this.index = 0u;
    this.file = file;
}

#(*
  * advance the specified amount of tokens forward in the sequence of tokens.
  *
  * \param this doc_parser
  *
  * \param the amount ot tokens to advance forwards.
  *)#
export func doc_parser_advance : (this : ^ mut doc_parser, adv : u) -> void
{
    if this.index + adv <= array_length(this.tokens)
    {
        this.index = this.index + adv;
    }
}

#(*
  * peek at a token and test if it matches a prediction about the token stream.
  *
  * \param this doc_parser
  *
  * \param the expected ``doc_token`` as one of its tags.
  *
  * \return true if the token matches the prediction.
  *)#
export func doc_parser_peek:
    (this : ^ doc_parser, tkn : u8, dst : u) -> bool
{
    if this.index + dst < array_length(this.tokens)
    {
        return this.tokens[this.index].tag == tkn;
    }
    else
    {
        return false;
    }
}

#(*
  * Tests if the parser is at the end of its token sequence.
  *
  * \param this doc_parser
  *)#
export func doc_parser_at_end : (this : ^ doc_parser) -> bool
{
    return this.index >= array_length(this.tokens);
}

#(*
  * emit a warning when an unexpected token is reached.
  * It uses the current token for the line number.
  *
  * \param this doc_lexer, used to get the current line number.
  *
  * \param the warning message.
  *)#
export func doc_parser_warning:
    (this: ^ doc_parser, str : ^ ascii) -> void
{
    eprint(this.file);
    eprintch(':'a);
    eprint_u(this.tokens[this.index].line);
    eprintln(str);
}

#(*
  * parser for a single paragraph or sequence of inline style elements in
  * the sequence.
  *
  * \param this doc_parser.
  *
  * \param the expected end token for this paragraph
  *)#
func doc_parse_paragraph:
    (this : ^ mut doc_parser, end : u8) -> mut ^ mut inline_style
{
    let ret : mut ^ mut inline_style =
        array_allocate(0u, sizeof(inline_style));
    let style : mut inline_style;
    style.init();

    loop !this.at_end() && !this.peek(end, 0u)
    {
        if this.peek(doc_token.newline, 0u)
        {
            style.text.append_ascii('\n'a);
            this.advance(1u);
        }
        elif this.peek(doc_token.whitespace, 0u)
        {
            style.text.append_ascii(' 'a);
            this.advance(1u);
        }
        elif this.peek(doc_token.comma, 0u)
        {
            style.text.append_ascii(','a);
            this.advance(1u);
        }
        elif this.peek(doc_token.quote, 0u)
        {
            style.text.append_ascii('"'a);
            this.advance(1u);
        }
        elif this.peek(doc_token.open_paren, 0u)
        {
            style.text.append_ascii('('a);
            this.advance(1u);
        }
        elif this.peek(doc_token.csv_block, 0u)
        {
            style.text.append_astr($",,,"a);
            this.advance(1u);
            this.warning($"unexpected csv block."a);

        }
        elif this.peek(doc_token.open_quote_block, 0u)
        {
            style.text.append_astr($">>>"a);
            this.advance(1u);
            this.warning($"unexpected open quote block."a);

        }
        elif this.peek(doc_token.close_quote_block, 0u)
        {
            style.text.append_astr($"<<<"a);
            this.advance(1u);
            this.warning($"unexpected csv block."a);
        }
        elif this.peek(doc_token.close_paren, 0u)
        {
            style.text.append_ascii(')'a);
            this.advance(1u);
        }
        elif this.peek(doc_token.text, 0u)
        {
            style.text.append(?this.tokens[this.index].text.text);
            this.advance(1u);
        }
        elif this.peek(doc_token.macro, 0u)
        {
            style.text.append(?this.tokens[this.index].macro.macro);
            this.advance(1u);
        }
        elif this.peek(doc_token.code_block, 0u)
        {
            this.warning($"Code block within paragraph omitted. Use inline code instead."a);
            this.advance(1u);
        }
        elif this.peek(doc_token.bold, 0u)
        {
            ret = array_push(ret, ?style);
            this.advance(1u);
            style.is_bold = !style.is_bold;
            style.text.init();
        }
        elif this.peek(doc_token.underline, 0u)
        {
            ret = array_push(ret, ?style);
            this.advance(1u);
            style.is_underline = !style.is_underline;
            style.text.init();
        }
        elif this.peek(doc_token.strike, 0u)
        {
            ret = array_push(ret, ?style);
            this.advance(1u);
            style.is_strike = !style.is_strike;
            style.text.init();
        }
        elif this.peek(doc_token.open_italic, 0u)
        {
            if style.is_italics
            {
                this.warning($"Open italics when already in italics."a);
            }
            ret = array_push(ret, ?style);
            this.advance(1u);
            style.is_italics = true;
            style.text.init();
        }
        elif this.peek(doc_token.close_italic, 0u)
        {
            if !style.is_italics
            {
                this.warning($"Close italics when not in italics."a);
            }
            ret = array_push(ret, ?style);
            this.advance(1u);
            style.is_italics = false;
            style.text.init();
        }
        elif this.peek(doc_token.code, 0u)
        {
            ret = array_push(ret, ?style);
            style.text.init();
            let c : mut inline_style;
            c.init();
            c.is_code = true;
            c.text.append(?this.tokens[this.index].code.text);
            ret = array_push(ret, ?c);
            this.advance(1u);
        }
        else
        {
            this.warning($"Omitting unknown token"a);
            this.advance(1u);
        }
    }
    ret = array_push(ret, ?style);
    return ret;
}

#(*
  * parse the doc_token sequence into the doc_section.
  *
  * \param this doc_parser.
  *)#
export func doc_parser_parse:
    (this : ^ mut doc_parser) -> void
{
    if this.peek(doc_token.text, 0u) &&
       astr_compare(this.tokens[0u].text.text.data(), $"*"a) == 0s
    {
        this.advance(1u);
    }

    loop !this.at_end() && (this.peek(doc_token.whitespace, 0u) ||
                             this.peek(doc_token.newline, 0u) ||
                             this.peek(doc_token.double_newline, 0u))
    {
        this.advance(1u);
    }

    loop !this.at_end()
    {
        if this.peek(doc_token.csv_block, 0u)
        {
            this.warning($"CSV is unimplemented currently."a);
            this.advance(1u);
        }
        elif this.peek(doc_token.open_quote_block, 0u)
        {
            this.warning($"quotes are currently unimplemented."a);
            this.advance(1u);
        }
        elif this.peek(doc_token.ordered_list, 0u) ||
             this.peek(doc_token.set_ordered_list, 0u)
        {
            this.warning($"ordered lists are currently unimplemented."a);
            this.advance(1u);
        }
        elif this.peek(doc_token.unordered_list, 0u)
        {
            this.warning($"unordered lists are currently unimplemented."a);
            this.advance(1u);
        }
        elif this.peek(doc_token.macro, 0u)
        {
            if astr_compare(this.tokens[this.index].macro.macro.data(),
                        $"param"a) == 0s
            {
                this.advance(1u);
                let cmt : mut doc_comment;

                cmt.tag = doc_comment.param;
                cmt.param.text =
                    doc_parse_paragraph(this, doc_token.double_newline);

                this.section.comments =
                    array_push(this.section.comments, ?cmt);
            }
            elif astr_compare(this.tokens[this.index].macro.macro.data(),
                        $"return"a) == 0s
            {
                this.advance(1u);
                let cmt : mut doc_comment;

                cmt.tag = doc_comment.return_dt;
                cmt.return_dt.text =
                    doc_parse_paragraph(this, doc_token.double_newline);

                this.section.comments =
                    array_push(this.section.comments, ?cmt);
            }
            elif astr_compare(this.tokens[this.index].macro.macro.data(),
                        $"brief"a) == 0s
            {
                this.advance(1u);
                let cmt : mut doc_comment;

                cmt.tag = doc_comment.brief;
                cmt.brief.text =
                    doc_parse_paragraph(this, doc_token.double_newline);

                this.section.comments =
                    array_push(this.section.comments, ?cmt);
            }

            else
            {
                this.warning($"Unrecognized macro token."a);
                let cmt : mut doc_comment;

                cmt.tag = doc_comment.paragraph;
                cmt.paragraph.text =
                    doc_parse_paragraph(this, doc_token.double_newline);

                this.section.comments =
                    array_push(this.section.comments, ?cmt);
            }
        }
        else
        {
            let cmt : mut doc_comment;

            cmt.tag = doc_comment.paragraph;
            cmt.paragraph.text =
                doc_parse_paragraph(this, doc_token.double_newline);

            this.section.comments =
                array_push(this.section.comments, ?cmt);
        }
        loop !this.at_end() && (this.peek(doc_token.whitespace, 0u) ||
                                 this.peek(doc_token.newline, 0u) ||
                                 this.peek(doc_token.double_newline, 0u))
        {
            this.advance(1u);
        }
    }
}
