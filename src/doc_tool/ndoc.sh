#! /bin/sh

print_help()
{
    echo "N Doc is a tool for making API Documentation based on N-Source code."
    echo "It outputs HTML encoded documentation based on formatted comments."
    echo
    echo "USAGE:"
    echo "       ndoc <list-of-source-files-or-directories>"
    echo "       ndoc -o <directory> <list-of-source-files-or-directories>"
    echo
    echo "  -o <dir>     the directory to output documentation files."
    echo "               Default: the current working directory."
    echo "  <file>       process documentation for this file."
    echo "  <directory>  process documentation for all N source files in this"
    echo "               directory, and recurse through all sub-directories."
    exit
}

files=""
output="./"

arg_parse()
{
    parsing=true
    while "$parsing"
    do
        if [ "$1" = "-o" -a "$#" -gt 1 ]
        then
            output="$2"
            shift
        elif [ "$1" = "-o" ]
        then
            echo "ERROR: -o requires argument"
            print_help
        elif [ "$1" = "-h" -o "$1" = "--help" ]
        then
            print_help
        else
            files="$files $1"
        fi

        if [ "$#" -gt 0 ]
        then
            shift
        fi
        if [ "$#" -eq 0 ]
        then
            parsing=false
        fi
    done
}

process_file()
{
    if [ -f "$1" -a $( echo "$1" | grep '.n$' > /dev/null ; echo "$?" ) -eq 0 ]
    then
        echo "file: $1"
        ndoc_processor $1 > "${output}/${1}.html"
    elif [ -d "$1" ]
    then
        echo " dir: $1"
        mkdir -p "${output}/${1}"
        for file in $( ls ${1} )
        do
            process_file "${1}/${file}"
        done
    fi
}

arg_parse "$@"

echo "output: $output"
mkdir -p "$output"

for file in $files
do
    process_file "$file"
done
