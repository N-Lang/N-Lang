import "std/io.n"a;
import "std/astring.n"a;

import "compiler.n"a;
import "parser.n"a;
import "doc_parser.n"a;
import "doc_rep.n"a;

func entry : (argc : u, argv : ^^ ascii) -> s
{
    LEXER_EMIT_COMMENTS = true;
    loop i : mut u in [1u, argc)
    {
        let mod : mut module;
        mod.init();
        mod.src_path.assign_cstr(argv[i]);
        file_read_into_nstr(argv[i], ?mod.src);
        dophase_lexical_analysis(?mod);
        
        let p : mut parser;
        p.init(?mod);
        let sections : mut ^ mut doc_section = p.parse();
        loop j : mut u in [0u, array_length(sections))
        {
            let dp : mut doc_parser;
            dp.init(?sections[j], argv[i]);
            dp.parse();
            sections[j].print();
        }
        mod.fini();
    }
    return 0s;
}
