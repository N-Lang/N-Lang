import "std/io.n"a;
import "std/astring.n"a;
import "jank/array.n"a;

import "compiler.n"a;
import "doc_lexer.n"a;

export introduce struct inline_style;

export introduce func inline_style_init : (this : ^ mut inline_style) -> void;
export introduce func inline_style_print : (this : ^ inline_style) -> void;

export struct inline_style
{
    let is_bold       : mut bool;
    let is_underline  : mut bool;
    let is_italics    : mut bool;
    let is_strike     : mut bool;
    let is_code       : mut bool;
    let is_identifier : mut bool;
    let is_url        : mut bool;

    let text : mut astring;

    func init = inline_style_init;
    func print = inline_style_print;
}

#(*
  * initialize an inline style.
  *
  * \param this inline style to be initialized.
  *)#
export func inline_style_init : (this : ^ mut inline_style) -> void
{
    this.is_bold = false;
    this.is_italics = false;
    this.is_underline = false;
    this.is_strike = false;
    this.is_code = false;
    this.is_identifier = false;
    this.is_url = false;

    this.text.init();
}

#(*
  * print out an inline style.
  *
  * \param the inline style to print.
  *)#
export func inline_style_print : (this : ^ inline_style) -> void
{
    if this.is_bold      { print($"<strong>"a); }
    if this.is_italics   { print($"<em>"a); }
    if this.is_underline { print($"<u>"a); }
    if this.is_strike    { print($"<strike>"a); }
    if this.is_code      { print($"<code>"a); }

    print(this.text.data());

    if this.is_code      { print($"</code>"a); }
    if this.is_strike    { print($"</strike>"a); }
    if this.is_underline { print($"</u>"a); }
    if this.is_italics   { print($"</em>"a); }
    if this.is_bold      { print($"</strong>"a); }
}

export introduce variant doc_comment;

export introduce func doc_comment_paragraph_print:
    (this : ^ doc_comment) -> void;
export introduce func doc_comment_code_print:
    (this : ^ doc_comment) -> void;

export variant doc_comment
{
    iface print : (^ doc_comment) -> void;

    paragraph, param, return_dt, brief
    {
        let text : mut ^ mut inline_style; # ! VLA

        func print = doc_comment_paragraph_print;
    }
    code
    {
        let text : mut astring;

        func print = doc_comment_code_print;
    }
}

#(*
  * print out a paragraph form for a doc_comment.
  *
  * \param the doc_comment to print. should have the ``paragraph``,
  * ``param``, ``return_dt``, or ``brief`` form of a doc_comment.
  *)#
export func doc_comment_paragraph_print : (this : ^ doc_comment) -> void
{
    print($"<p>"a);
    loop i : mut u in [0u, array_length(this.paragraph.text))
    {
        this.paragraph.text[i].print();
    }
    print($"</p>"a);
}

#(*
  * print out the code section of a doc_comment.
  *
  * \param the doc_comment. its tag should be ``code``.
  *)#
export func doc_comment_code_print : (this : ^ doc_comment) -> void
{
    print($"<p><pre><code>"a);
    print(this.code.text.data());
    print($"</code></pre></p>"a);
}

export introduce variant doc_section;

export struct parameter
{
    let id : mut ^ token;
    let dt : mut ^ token; # VLA
}

export introduce func doc_section_print : (this : ^ doc_section) -> void;

export variant doc_section
{
    let tokens : mut ^ mut doc_token; # ! VLA
    let comments : mut ^ mut doc_comment; # ! VLA

    let exported : mut bool;
    let introduced : mut bool;

    func print = doc_section_print;

    function
    {
        let id : mut ^ token;
        let parameters : mut ^ mut parameter; # ! VLA
        let return_dt : mut ^ token; # VLA
    }

    none { }
}

#(*
  * prints out a token.
  *
  * \param the token to be printed.
  *)#
func token_print : (tok : ^ token) -> void
{
    write(STDOUT_FILENO, tok.start, tok.length);
}

#(*
  * Prints a data type which is represented as a sequence of tokens from the
  * source.
  *
  * \param the sequence of tokens. It is a resizeable array.
  *)#
func dt_print : (tok : ^ token) -> void
{
    loop i : mut u in [0u, array_length(tok))
    {
        if i != 0u { printch(' 'a); }
        token_print(?tok[i]);
    }
}

#(*
  * print out a doc_token.
  *
  * \param this doc_section.
  *)#
export func doc_section_print : (this : ^ doc_section) -> void
{
    if array_length(this.comments) == 0u
    {
        return;
    }
    if this.tag == doc_section.function
    {
        print($"<div><h2>Function: "a);
        token_print(this.function.id);
        print($"</h2>"a);

        let param_num : mut u = 0u;
        loop i : mut u in [0u, array_length(this.comments))
        {
            let cmt : ^ doc_comment = ?this.comments[i];

            if cmt.tag == doc_comment.param
            {
                if param_num < array_length(this.function.parameters)
                {
                    let p : ^ parameter =
                        ?this.function.parameters[param_num];
                    param_num = param_num + 1u;
                    print($"<h3>Parameter: "a);
                    token_print(p.id);
                    print($"</h3><table><tr><td><code>"a);
                    dt_print(p.dt);
                    print($"</code></td><td>"a);
                    cmt.print();
                    print($"</td></tr></table>"a);
                }
            }
            elif cmt.tag == doc_comment.return_dt
            {
                print($"<h3>Return</h3><table><tr><td><code>"a);
                dt_print(this.function.return_dt);
                print($"</code></td><td>"a);
                cmt.print();
                print($"</td></tr></table>"a);
            }
            elif cmt.tag == doc_comment.brief
            {
                print($"<h3>Brief</h3>"a);
                cmt.print();
            }
            else
            {
                cmt.print();
            }
        }

        print($"</div>"a);
    }
}
