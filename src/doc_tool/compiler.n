import "cstd/primitives.n"a;

###############################################################################
## N Utilities
###############################################################################

export introduce struct nstr_t;

#(*
  * Initialize an nstr as empty.
  * 
  * \param this nstr
  *)#
export introduce func nstr_init : (this : mut ^ mut nstr_t) -> void;

#(*
  * Initialize an nstr as a copy of the value of an astr.
  *
  * \param this nstr
  *
  * \param the astr to copy.
  *)#
export introduce func nstr_init_cstr:
    (this : mut ^ mut nstr_t, str : mut ^ ascii) -> void;

#(*
  * Copy the value of an astr onto this nstr
  *
  * \param this nstr
  *
  * \param the astr to copy
  *)#
export introduce func nstr_assign_cstr:
    (this : mut ^ mut nstr_t, cstr : mut ^ ascii) -> void;

#(*
  * Copy the value of one nstr onto another nstr
  *
  * \param this nstr
  *
  * \param the nstr to be copied onto this.
  *)#
export introduce func nstr_assign_nstr:
    (this : mut ^ mut nstr_t, other : mut ^ nstr_t) -> void;

#(*
  * nstr_t is the ``nompile`` compiler's high level abstraction for strings.
  *)#
export struct nstr_t
{
    let data : mut ^ mut ascii;
    let len : mut size_t;
    let capacity : mut size_t;

    func init = nstr_init;
    func init_cstr = nstr_init_cstr;
    func assign_cstr = nstr_assign_cstr;
    func assign_nstr = nstr_assign_nstr;
}

#(*
  * Reads the contents of a text file into an nstr.
  *
  * \param the path which names the desired file.
  *
  * \param the nstr destination.
  *)#
export introduce func file_read_into_nstr:
    (path : mut ^ ascii, nstr : mut ^ mut nstr_t) -> ssize_t;

#(*
  * ``narr`` (N-Array) is ``nompile``'s abstraction for resizable arrays.
  * The type heavily uses C-Macros and ``^ void`` pointers.
  * these are not used heavily by the ``doc_tool``.
  *
  * \brief This function simply queries the length of a ``narr``.
  *
  * \param the narr to read.
  *
  * \return the length of the narr.
  *)#
export introduce func narr_length : (narr : ^ mut void) -> size_t;

###############################################################################
## Tokens
###############################################################################

#* Token type is an enum listing the possible varieties of tokens.
export enum token_type
{
    TOKEN_KEYWORD_ALIAS;
    TOKEN_KEYWORD_AS;
    TOKEN_KEYWORD_BOOL;
    TOKEN_KEYWORD_BREAK;
    TOKEN_KEYWORD_ASCII;
    TOKEN_KEYWORD_CONTINUE;
    TOKEN_KEYWORD_COUNTOF;
    TOKEN_KEYWORD_DEFER;
    TOKEN_KEYWORD_ELIF;
    TOKEN_KEYWORD_ELSE;
    TOKEN_KEYWORD_ENUM;
    TOKEN_KEYWORD_EXPORT;
    TOKEN_KEYWORD_F128;
    TOKEN_KEYWORD_F16;
    TOKEN_KEYWORD_F32;
    TOKEN_KEYWORD_F64;
    TOKEN_KEYWORD_FALSE;
    TOKEN_KEYWORD_FUNC;
    TOKEN_KEYWORD_IF;
    TOKEN_KEYWORD_IFACE;
    TOKEN_KEYWORD_IMPORT;
    TOKEN_KEYWORD_IN;
    TOKEN_KEYWORD_INTRODUCE;
    TOKEN_KEYWORD_ISOLATE;
    TOKEN_KEYWORD_LET;
    TOKEN_KEYWORD_LOOP;
    TOKEN_KEYWORD_MUT;
    TOKEN_KEYWORD_NULL;
    TOKEN_KEYWORD_RETURN;
    TOKEN_KEYWORD_S;
    TOKEN_KEYWORD_S16;
    TOKEN_KEYWORD_S32;
    TOKEN_KEYWORD_S64;
    TOKEN_KEYWORD_S8;
    TOKEN_KEYWORD_SIZEOF;
    TOKEN_KEYWORD_ASTRING;
    TOKEN_KEYWORD_STRUCT;
    TOKEN_KEYWORD_TRUE;
    TOKEN_KEYWORD_TYPEOF;
    TOKEN_KEYWORD_U;
    TOKEN_KEYWORD_U16;
    TOKEN_KEYWORD_U32;
    TOKEN_KEYWORD_U64;
    TOKEN_KEYWORD_U8;
    TOKEN_KEYWORD_UNION;
    TOKEN_KEYWORD_UNIQUE;
    TOKEN_KEYWORD_VARIANT;
    TOKEN_KEYWORD_VOID;
    TOKEN_KEYWORD_VOLATILE;

    #/////////////////////////
    #/ OPERATOR
    #/////////////////////////

    #/ LENGTH 3
    TOKEN_OPERATOR_DASH_CARET_CARET;
    #/ LENGTH 2
    TOKEN_OPERATOR_AMPERSAND_AMPERSAND;
    TOKEN_OPERATOR_BANG_EQUAL;
    TOKEN_OPERATOR_DASH_CARET;
    TOKEN_OPERATOR_DASH_GREATERTHAN;
    TOKEN_OPERATOR_DOT_DOT;
    TOKEN_OPERATOR_EQUAL_EQUAL;
    TOKEN_OPERATOR_GREATERTHAN_EQUAL;
    TOKEN_OPERATOR_GREATERTHAN_GREATERTHAN;
    TOKEN_OPERATOR_LESSTHAN_EQUAL;
    TOKEN_OPERATOR_LESSTHAN_LESSTHAN;
    TOKEN_OPERATOR_PIPE_PIPE;
    TOKEN_OPERATOR_PLUS_CARET;
    #/ LENGTH 1
    TOKEN_OPERATOR_AMPERSAND;
    TOKEN_OPERATOR_ASTERISK;
    TOKEN_OPERATOR_AT;
    TOKEN_OPERATOR_BANG;
    TOKEN_OPERATOR_CARET;
    TOKEN_OPERATOR_COLON;
    TOKEN_OPERATOR_COMMA;
    TOKEN_OPERATOR_DOLLAR;
    TOKEN_OPERATOR_DOT;
    TOKEN_OPERATOR_EQUAL;
    TOKEN_OPERATOR_GREATERTHAN;
    TOKEN_OPERATOR_LEFTBRACE;
    TOKEN_OPERATOR_LEFTBRACKET;
    TOKEN_OPERATOR_LEFTPARENTHESIS;
    TOKEN_OPERATOR_LESSTHAN;
    TOKEN_OPERATOR_PIPE;
    TOKEN_OPERATOR_QUESTION;
    TOKEN_OPERATOR_RIGHTBRACE;
    TOKEN_OPERATOR_RIGHTBRACKET;
    TOKEN_OPERATOR_RIGHTPARENTHESIS;
    TOKEN_OPERATOR_SEMICOLON;
    TOKEN_OPERATOR_SLASH;
    TOKEN_OPERATOR_TILDE;
    #/ SPECIAL CASES
    TOKEN_OPERATOR_DASH;
    TOKEN_OPERATOR_PLUS;

    #/////////////////////////
    #/ VALUES
    #/////////////////////////

    TOKEN_IDENTIFIER;

    TOKEN_LITERAL_U8;
    TOKEN_LITERAL_U16;
    TOKEN_LITERAL_U32;
    TOKEN_LITERAL_U64;
    TOKEN_LITERAL_U;
    TOKEN_LITERAL_S8;
    TOKEN_LITERAL_S16;
    TOKEN_LITERAL_S32;
    TOKEN_LITERAL_S64;
    TOKEN_LITERAL_S;
    TOKEN_LITERAL_F16;
    TOKEN_LITERAL_F32;
    TOKEN_LITERAL_F64;
    TOKEN_LITERAL_F128;
    TOKEN_LITERAL_ASCII;
    TOKEN_LITERAL_ASTRING;
    TOKEN_LITERAL_ENUM_I;

    TOKEN_COMMENT;

    TOKEN_EOF;

    #! Delimiter token for Earley Parser
    TOKEN_UNDEFINED;
}

#(**
  * Gets an astr which names the token type given.
  *
  * \param the type of the token.
  *
  * \return astr naming the token type.
  *)#
export introduce func token_type_to_cstr : (ttype : token_type) -> ^ ascii;

export introduce struct module;

#* representation of a single token in source.
export struct token
{
    #* Variant tag of the token type.
    let tag : token_type;

    #* Pointer to the module which contains this token.
    let module : mut ^ mut module;

    #* Pointer to the location in the source buffer where the token begins.
    let start : mut ^ ascii;

    #* Number of characters in the token.
    let length : mut size_t;

    #* one-indexex line in the source buffer where the first character of this
    #* token was parsed from.
    let line : mut size_t;

    #* one indexed column in the source buffer where the first character of
    #* this token was parsed from.
    let column : mut size_t;
}

#* Instruct the lexer whether to emit comments. ``true`` if it should.
#*
#* **Default:** do not emit comments.
export introduce let LEXER_EMIT_COMMENTS : mut bool;

#(*
  * Convert a source file from source form to a list of tokens.
  *
  * \param the lexer operates on a module, reading its ``src`` string and
  * writing tokens into its ``tokens`` list.
  *
  * \return ``true`` on success. ``false`` otherwise.
  *)#
export introduce func dophase_lexical_analysis:
    (mod : mut ^ mut module) -> bool;

###############################################################################
## N Intermediate Representation
###############################################################################

#* The identifer is the NIR abstraction for an identifier in source code.
export struct identifier
{
    #* Pointer to the start of the identifier in the source.
    let start : mut ^ ascii;

    #* The number of characters in this identifier.
    let length : mut size_t;

    #* The token which this identifier was taken from.
    let srctok : mut ^ token;
}

export introduce struct module;

#(*
  * Initialize an empty module.
  *
  * \param the module to initialize.
  *)#
export introduce func module_init : (this : mut ^ mut module) -> void;

#(*
  * Deinitialize a module.
  *
  * \param the module to finish.
  *)#
export introduce func module_fini : (this : mut ^ mut module) -> void;

#* Module is the abstraction for one source file
export struct module
{
    #* Relative file path of the module source.
    let src_path : mut nstr_t;

    #* File path of the compiled output for this module.
    let out_path : mut nstr_t;

    #* Contents of the #src_path.
    let src : mut nstr_t;

    #* Tokenization of #src.
    let tokens : mut ^ mut token; #! narr_t

    #* The global scope of this module.
    #*
    #* This is represented as an array of 64 characters, however in ``nompile``
    #* This is actually a structure which happens to have sizeof = 64
    #* on alternate archietectures, this may be a different size.
    let global_scope : [64u] ascii;

    #* List of each #identifier for symbols exported from the global #scope of
    #* this module.
    let exports : mut ^ mut identifier; #! narr_t

    #* Used for generating unique identifiers within this module.
    #* These integer identifiers are used for generating unique variable and
    #* label names within the module.
    let uid_counter : u32;

    func init = module_init;
    func fini = module_fini;
}


