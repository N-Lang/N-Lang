import "compiler.n"a;
import "doc_lexer.n"a;
import "doc_rep.n"a;

export introduce struct parser;

export introduce func parser_init:
    (this : ^ mut parser, module : ^ module) -> void;
export introduce func parser_advance : (this : mut ^ mut parser, adv : u) -> void;
export introduce func parser_peek:
    (this : ^ parser, tkn : token_type, dst : u) -> bool;
export introduce func parser_at_end : (this : ^ parser) -> bool;
export introduce func parser_eat_between_curly_braces:
    (this : ^ mut parser) -> void;
export introduce func parser_parse:
    (this : ^ mut parser) -> mut ^ mut doc_section;

export struct parser
{
    let tokens : mut ^ token;
    let index : mut u;
    let file : mut ^ ascii;

    func init = parser_init;
    func advance = parser_advance;
    func peek = parser_peek;
    func at_end = parser_at_end;
    func eat_between_curly_braces = parser_eat_between_curly_braces;
    func parse = parser_parse;
}

export func parser_init:
    (this : ^ mut parser, module : ^ module) -> void
{
    this.tokens = module.tokens;
    this.index = 0u;
    this.file = module.src_path.data;
}

export func parser_advance : (this : ^ mut parser, adv : u) -> void
{
    if this.index + adv < array_length(this.tokens)
    {
        this.index = this.index + adv;
    }
}
export func parser_peek:
    (this : ^ parser, tkn : token_type, dst : u) -> bool
{
    if this.index + dst < array_length(this.tokens)
    {
        return this.tokens[this.index].tag == tkn;
    }
    else
    {
        return false;
    }
}

export func parser_at_end : (this : ^ parser) -> bool
{
    return this.index >= array_length(this.tokens) ||
        this.tokens[this.index].tag == token_type.TOKEN_EOF;
}

export func parser_eat_between_curly_braces : (this : ^ mut parser) -> void
{
    loop !this.peek(token_type.TOKEN_OPERATOR_LEFTBRACE, 0u)
    {
        this.advance(1u);
    }

    let num : mut u = 1u;
    this.advance(1u);
    loop num != 0u
    {
        if this.peek(token_type.TOKEN_OPERATOR_LEFTBRACE, 0u)
        {
            num = num + 1u;
        }
        elif this.peek(token_type.TOKEN_OPERATOR_RIGHTBRACE, 0u)
        {
            num = num - 1u;
        }
        this.advance(1u);
    }
}

export func parser_parse : (this : ^ mut parser) -> mut ^ mut doc_section
{
    let lex : mut doc_lexer;
    lex.init(this.file);

    let is_introduce : mut bool = false;
    let is_export : mut bool = false;

    let sections : mut ^ mut doc_section =
        array_allocate(0u, sizeof(doc_section));

    loop !this.at_end()
    {
        if this.peek(token_type.TOKEN_KEYWORD_IMPORT, 0u)
        {
            this.advance(3u);
        }
        elif this.peek(token_type.TOKEN_COMMENT, 0u)
        {
            lex.add_comment_token(?this.tokens[this.index]);
            this.advance(1u);
        }
        elif this.peek(token_type.TOKEN_KEYWORD_EXPORT, 0u)
        {
            this.advance(1u);
            is_export = true;
        }
        elif this.peek(token_type.TOKEN_KEYWORD_INTRODUCE, 0u)
        {
            this.advance(1u);
            is_introduce = true;
        }
        elif this.peek(token_type.TOKEN_KEYWORD_FUNC, 0u)
        {
            this.advance(1u);
            let section : mut doc_section;
            section.tag = doc_section.function;
            section.tokens = array_allocate(0u, sizeof(doc_token));
            section.comments = array_allocate(0u, sizeof(doc_comment));
            section.introduced = is_introduce;
            section.exported = is_export;
            section.function.parameters =
                array_allocate(0u, sizeof(parameter));
            lex.tokens = section.tokens;
            lex.lex();
            section.tokens = lex.tokens as ^ mut doc_token;
            lex.fini();

            section.function.id = ?this.tokens[this.index];
            this.advance(3u);

            loop true
            {
                let param : mut parameter;
                param.id = ?this.tokens[this.index];
                this.advance(2u);
                param.dt = array_allocate(0u, sizeof(token));
                loop !this.peek(token_type.TOKEN_OPERATOR_COMMA, 0u) &&
                     !this.peek(token_type.TOKEN_OPERATOR_RIGHTPARENTHESIS, 0u)
                {
                    param.dt = array_push(param.dt as ^ mut void,
                        ?this.tokens[this.index] as ^ mut void);
                    this.advance(1u);
                }
                section.function.parameters =
                    array_push(section.function.parameters, ?param);
                if this.peek(token_type.TOKEN_OPERATOR_RIGHTPARENTHESIS, 0u)
                {
                    break;
                }
                else
                {
                    this.advance(1u);
                }
            }
            this.advance(2u);

            section.function.return_dt = array_allocate(0u, sizeof(token));
            loop !this.peek(token_type.TOKEN_OPERATOR_SEMICOLON, 0u) &&
                 !this.peek(token_type.TOKEN_OPERATOR_LEFTBRACE, 0u)
            {
                section.function.return_dt =
                    array_push(section.function.return_dt as ^ mut void,
                               ?this.tokens[this.index] as ^ mut void);
                this.advance(1u);
            }
            sections = array_push(sections, ?section);

            if(!is_introduce)
            {
                this.eat_between_curly_braces();
            }
            else
            {
                this.advance(1u);
            }

            lex.init(this.file);
            is_introduce = false;
            is_export = false;
        }
        elif this.peek(token_type.TOKEN_KEYWORD_LET, 0u)
        {
            loop !this.peek(token_type.TOKEN_OPERATOR_SEMICOLON, 0u)
            {
                this.advance(1u);
            }
            this.advance(1u);

            lex.fini();
            lex.init(this.file);
            is_introduce = false;
            is_export = false;
        }
        elif this.peek(token_type.TOKEN_KEYWORD_ENUM, 0u) ||
             this.peek(token_type.TOKEN_KEYWORD_UNION, 0u) ||
             this.peek(token_type.TOKEN_KEYWORD_STRUCT, 0u) ||
             this.peek(token_type.TOKEN_KEYWORD_VARIANT, 0u)
        {
            if(is_introduce)
            {
                loop !this.peek(token_type.TOKEN_OPERATOR_SEMICOLON, 0u)
                {
                    this.advance(1u);
                }
                this.advance(1u);
            }
            else
            {
                this.eat_between_curly_braces();
            }
            lex.fini();
            lex.init(this.file);
            is_introduce = false;
            is_export = false;
        }
        else
        {
            lex.fini();
            lex.init(this.file);
            is_introduce = false;
            is_export = false;
            this.advance(1u);
        }
    }

    return sections;
}
