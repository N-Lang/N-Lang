/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
#define _GNU_SOURCE

////////////////////////////////////////////////////////////////////////////////
//      LIBRARY-WIDE HEADERS                                                  //
////////////////////////////////////////////////////////////////////////////////
// C Standard Library
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <signal.h>
#include <stdalign.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
// POSIX
#include <ftw.h>
#include <getopt.h>
#include <libgen.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

////////////////////////////////////////////////////////////////////////////////
//      STRICTER DEFINITIONS OF BUILT IN CONSTANTS                            //
////////////////////////////////////////////////////////////////////////////////
// ISO/IEC 9899:TC3 - 7.16.3
// ```
// The remaining [after _Bool] three macros are suitable for use in #if
// preprocessing directives. They are
// true
// which expands to the integer constant 1,
// false
// which expands to the integer constant 0, and
// __bool_true_false_are_defined
// which expands to the integer constant 1.
// ```
// Under the C standard `true` and `false` are of type `int`, not type `_Bool`.
// These redefinitions place tighter type restrictions on these macros.
//
// These redefinitions also allow Doxygen to reference `true` and `false` with
// #true and #false.

// clang-format off
#undef true
#define true ((_Bool)1)
#undef false
#define false ((_Bool)0)
// clang-format on

// ISO/IEC 9899:TC3 - 6.3.2.3.3
// ```
// An integer constant expression with the value 0, or such an expression cast
// to type void *, is called a null pointer constant.
// ```
//
// ISO/IEC 9899:TC3 - 7.17.3
// ```
// The macros [within stddef.h] are
// NULL
// which expands to an implementation-defined null pointer constant...
// ```
//
// Under the C standard the type of `NULL` is not guaranteed to be type `void*`
// as `NULL` may expand to type `int`.
// This redefinition places a tighter type restriction on the `NULL` macro.
//
// This redefinition also allows Doxygen to reference `NULL` with #NULL.
#undef NULL
#define NULL ((void*)0)

////////////////////////////////////////////////////////////////////////////////
//      ARCHITECTURE AND ENVIRONMENT ASSERTIONS                               //
////////////////////////////////////////////////////////////////////////////////
static_assert(CHAR_BIT == 8, "invalid CHAR_BIT");

////////////////////////////////////////////////////////////////////////////////
//      STATUS BOOLEAN                                                        //
////////////////////////////////////////////////////////////////////////////////
//! Type used to represent a Boolean success or failure condition.
typedef bool stbool;
//! Indicates a Boolean success condition.
#define STBOOL_SUCCESS true
//! Indicates a Boolean failure condition.
#define STBOOL_FAILURE false

////////////////////////////////////////////////////////////////////////////////
//      UTILITY MACROS                                                        //
////////////////////////////////////////////////////////////////////////////////
// These macros exist to provide N-Lang developers with a set of standardized
// reusable macros for some common C idioms and preprocessor tricks.
// Use these macros when applicable in order to reduce code duplication and
// provide clear semantic intent to future developers and maintainers of written
// code.

//! Macro expanding to the empty string constant in read-only memory.
#define EMPTY_STRING ""

//! The following macros provide lengths for common magic numbers seen in code.
//! For instance when allocating a buffer of size S plus a slot for the null
//! terminator, the code `malloc(S + NULL_TERMINATOR_LENGTH)` is much clearer
//! than `malloc(S + 1)` as the intent of the `+ 1` is explicitly stated.
//!{

#define SINGLE_QUOTE_LENGTH 1    //!< `'`
#define DOUBLE_QUOTE_LENGTH 1    //!< `"`
#define NULL_TERMINATOR_LENGTH 1 //!< `\0`

//!}

//! Thread local storage.
#ifndef thread_local
#define thread_local __thread
#endif

//! Typeof operator.
#ifndef typeof
#define typeof __typeof
#endif

//! Infinite loop.
#define LOOP_FOREVER while (1)

//! Suppress "unused <whatever>" warnings for a single variable.
#define SUPPRESS_UNUSED(variable) (void)(variable)

#define _STRINGIFY_HELPER(...) #__VA_ARGS__
//! Turn the provided text into a cstring.
//! @note
//!     Macros are expanded by the preprocessor, so this macro does NOT
//!     preserve whitespace.
//! ```
//!     Example:
//!     printf(
//!         "%s qux\n",
//!         STRINGIFY(foo bar,          baz)
//!     );
//!     ^^ prints "foo bar, baz qux"
//! ```
#define STRINGIFY(...) _STRINGIFY_HELPER(__VA_ARGS__)

//! Plain old data copy.
//! @note
//!     This macro literally just an assignment statement. The only point of the
//!     macro is to make it clear that a structure is copying all it's data by
//!     value.
#define POD_COPY(dest, src) ((dest) = (src))

//! returns a pointer to the struct containing the member pointed to by
//! @p p_member who's member identifier in struct @p struct_type is
//! @p member_id. this is the same macro as the linux kernel's `list_entry`.
//! @param p_member
//!     pointer to a member variable with identifier @p member_id within a
//!     struct of type @p struct_type.
//! @param struct_type
//!     the `typeof` the struct containing the provided member.
//! @param member_id
//!     identifier of `*p_member` within the provided @p struct_type.
#define p_struct_from_p_member(p_member, struct_type, member_id)               \
    ((struct_type*)((char*)(p_member)-offsetof(struct_type, member_id)))

//! Zero the memory pointed to by @p p_instance.
//! @param p_instance
//!     Pointer to an object of type @p type.
//! @param type
//!     The `typeof` `*p_instance`.
#define MEMZERO_INSTANCE(p_instance, type) memset(p_instance, 0, sizeof(type))

//! Memset the memory pointed to by @p p_instance.
//! @param p_instance
//!     Pointer to an object of type @p type.
//! @param type
//!     The `typeof` `*p_instance`.
//! @param value
//!     8-bit value that will fill each byte of p_instance.
#define MEMSET_INSTANCE(p_instance, type, value)                               \
    memset(p_instance, value, sizeof(type))

//! Linux Kernel `likely` and `unlikely` definitions.
//! Used to hint branch prediction within conditional statements.
//!{

//! Condition @p x is overwhelmingly likely to resolve to a true value.
#define LIKELY(x) __builtin_expect((x), 1)
//! Condition @p x is overwhelmingly unlikely to resolve to a true value.
#define UNLIKELY(x) __builtin_expect((x), 0)

//!}

////////////////////////////////////////////////////////////////////////////////
//      COMMON GNUC DIAGNOSTICS/PRAGMAS/ATTRIBUTES                            //
////////////////////////////////////////////////////////////////////////////////
//! Open a new region of code in which to toggle diagnostic settings.
#define DIAGNOSTIC_PUSH _Pragma("GCC diagnostic push")

//! Close a region of code opened by #DIAGNOSTIC_PUSH.
//! Diagnostic settings from before #DIAGNOSTIC_PUSH are restored.
#define DIAGNOSTIC_POP _Pragma("GCC diagnostic pop")

#define _DIAGNOSTIC_IGNORE_HELPER(w) STRINGIFY(GCC diagnostic ignored w)
//! Ignore the warning specified by the provided cstring @p w.
//! @br
//! Example:
//! ```
//! // Ignore conversions implicitly changing values for the rest of this
//! // diagnostic region.
//! DIAGNOSTIC_IGNORE("-Wconversion")
//! ```
#define DIAGNOSTIC_IGNORE(w) _Pragma(_DIAGNOSTIC_IGNORE_HELPER(w))

#define ATTR_DEFER_FINI(fini_func) __attribute__((cleanup(fini_func)))

////////////////////////////////////////////////////////////////////////////////
//      ANSI TERMINAL ESCAPE SEQUENCES                                        //
////////////////////////////////////////////////////////////////////////////////
#define ANSI_ESC_DEFAULT "\x1b[0m"

#define ANSI_ESC_BOLD "\x1b[1m"
#define ANSI_ESC_ITALIC "\x1b[3m"
#define ANSI_ESC_UNDERLINE "\x1b[4m"

// clang-format off
#define ANSI_ESC_COLOR_BLACK   "\x1b[30m"
#define ANSI_ESC_COLOR_RED     "\x1b[31m"
#define ANSI_ESC_COLOR_GREEN   "\x1b[32m"
#define ANSI_ESC_COLOR_YELLOW  "\x1b[33m"
#define ANSI_ESC_COLOR_BLUE    "\x1b[34m"
#define ANSI_ESC_COLOR_MAGENTA "\x1b[35m"
#define ANSI_ESC_COLOR_CYAN    "\x1b[36m"
#define ANSI_ESC_COLOR_WHITE   "\x1b[37m"
// clang-format on

//! Applies the ANSI escape sequence @p esc_seq to @p target if @p FILEptr is a
//! tty as specified by isatty(3).
//! @param FILEptr
//!     Stream that is tested with `isatty`.
//! @param esc_seq
//!     Cstring literal escape sequence to be applied to @p target.
//! @param target
//!     Cstring literal to be escaped.
//! @note
//!     Multiple escape sequences can be combined together through compile time
//!     string concatenation to create a compound escape sequence.
//! @note
//!     If @p FILEptr is indeed a tty and @p target is escaped, the sequence is
//!     terminated with #ANSI_ESC_DEFAULT, setting all following output to have
//!     default text representation in the terminal.
#define ANSI_ESC_IF_TTY(                                                       \
    /* FILE* */ FILEptr,                                                       \
    /* char const* (literal) */ esc_seq,                                       \
    /* char const* (literal) */ target)                                        \
    (isatty(fileno(FILEptr)) ? esc_seq target ANSI_ESC_DEFAULT : target)

//! Applies the ANSI escape sequence @p esc_seq to @p target if @p FILEptr is a
//! tty as specified by isatty(3).
//! If @p FILEptr is *not* a tty, @p is enclosed in 'single quotes'.
//! @param FILEptr
//!     Stream that is tested with `isatty`.
//! @param esc_seq
//!     Cstring literal escape sequence to be applied to @p target.
//! @param target
//!     Cstring literal to be escaped.
//! @note
//!     Multiple escape sequences can be combined together through compile time
//!     string concatenation to create a compound escape sequence.
//! @note
//!     If @p FILEptr is indeed a tty and @p target is escaped, the sequence is
//!     terminated with #ANSI_ESC_DEFAULT, setting all following output to have
//!     default text representation in the terminal.
#define ANSI_ESC_IF_TTY_ELSE_QUOTE(                                            \
    /* FILE* */ FILEptr,                                                       \
    /* char const* (literal) */ esc_seq,                                       \
    /* char const* (literal) */ target)                                        \
    (isatty(fileno(FILEptr)) ? esc_seq target ANSI_ESC_DEFAULT : "'" target "'")

////////////////////////////////////////////////////////////////////////////////
//      PANIC                                                                 //
////////////////////////////////////////////////////////////////////////////////
//! Function to be invoked when an unexpected/unrecoverable error has occurred.
//! Prints a formatted message and a backtrace to `stderr` before calling `exit`
//! with argument `EXIT_FAILURE`.
//! @param fmt
//!     printf-style format string.
//! @param ...
//!     Arguments to the format specifiers of @p fmt.
void npanic(char const* fmt, ...) __attribute__((noreturn));

// clang-format off
#define _NPANIC_DEBUG_FILE_LINE                                                \
    "\n    FILE: " __FILE__                                                    \
    "\n    LINE: " STRINGIFY(__LINE__)
// clang-format on

//! Panic because a default case in a `switch` was reached that should never
//! have been reached.
#define NPANIC_DFLT_CASE()                                                     \
    npanic(                                                                    \
        "switch failed to match a non-default "                                \
        "case" _NPANIC_DEBUG_FILE_LINE)

//! Panic because an "unset" enum was reached in a switch that should never have
//! been reached.
#define NPANIC_UNSET_CASE()                                                    \
    npanic(                                                                    \
        "switch reached an unexpected 'unset' "                                \
        "enum" _NPANIC_DEBUG_FILE_LINE)

//! Panic because control should never have reached this point.
#define NPANIC_UNEXPECTED_CTRL_FLOW()                                          \
    npanic("unexpected control flow" _NPANIC_DEBUG_FILE_LINE)

//! Panic because we were too lazy to implement something and it turns out it is
//! needed.
#define NPANIC_TODO() npanic("TODO" _NPANIC_DEBUG_FILE_LINE)

////////////////////////////////////////////////////////////////////////////////
//      NASSERT                                                               //
////////////////////////////////////////////////////////////////////////////////
#ifdef NDEBUG
#define nassert(expr) /* nothing */
#else
//! Panic if @p expr evaluates to zero, printing the expression, file, and line
//! that caused the assertion failure.
//! @note
//!     This macro is disabled for builds where `NDEBUG` is defined.
#define nassert(expr)                                                          \
    (void)((expr) || (npanic("nassert failure --> " #expr _NPANIC_DEBUG_FILE_LINE), 0))
#endif

////////////////////////////////////////////////////////////////////////////////
//      NARRAY                                                                //
////////////////////////////////////////////////////////////////////////////////
//! Container-style type that provides a way to explicitly state that an array
//! is an narray.
#define narr_t(T) T*

//! Allocate an array of @p numelem elements each of size @p elemsize.
//! @param numelem
//!     The initial number of elements in the narray.
//! @param elemsize
//!     The `sizeof` elements in the narray.
//! @return
//!     Heap allocated narray with length @p numelem.
void* narr_alloc(size_t numelem, size_t elemsize)
    __attribute__((warn_unused_result));

//! Free narray @p narr allocated with #narr_alloc.
//! @param narr
//!     Target narray.
void narr_free(void* narr);

//! Returns the length (number of elements) in target narray.
//! @param narr
//!     Target narray.
//! @return
//!     Length of @p narr.
size_t narr_length(void const* narr);

//! Returns the maximum number of elements target narray can hold without
//! requiring memory reallocation.
//! @param narr
//!     Target narray.
//! @return
//!     Capacity of @p narr.
size_t narr_capacity(void const* narr);

//! Returns the `sizeof` elements in target narray.
//! @param narr
//!     Target narray.
//! @return
//!     The `sizeof` elements in @p narr.
size_t narr_sizeof_elem(void const* narr);

//! Guarantee that at least @p numelem elements beyond the current length of
//! narray @p narr can be inserted/pushed without requiring memory reallocation.
//! @param narr
//!     Target narray.
//! @param numelem
//!     Number of elements to be reserved.
//! @return
//!     @p narr with reserved space.
//! @note
//!     Does NOT change the length of @p narr.
//! @note
//!     May change the capacity of @p narr.
void* narr_reserve(void* narr, size_t numelem)
    __attribute__((warn_unused_result));

//! Change the length of a narray @p narr to @p len.
//! @param narr
//!     Target narray.
//! @param len
//!     New length of the narray.
//! @return
//!     @p narr with changed length.
void* narr_resize(void* narr, size_t len) __attribute__((warn_unused_result));

//! Append the object pointed to by @p ptr_to_val to the back of narray @p narr.
//! @param narr
//!     Target narray.
//! @param ptr_to_val
//!     Pointer to an object type-compatible with the elements of @p narr.
//! @return
//!     @p narr who's last value is a copy of *@p ptr_to_val.
//! @note
//!     Increases the length of @p narr by 1.
//! @note
//!     *@p ptr_to_val is memory copied to the back of @p narr. If you wish to
//!     create a duplicate of the object pointed to by *@p ptr_to_val with its
//!     own resources you must manually duplicate *@p ptr_to_val before pushing.
void* narr_push(void* narr, void const* ptr_to_val)
    __attribute__((warn_unused_result));

//! Remove the last element off the back of @p narr.
//! @param narr
//!     Target narray.
//! @note
//!     Decreases the length of @p narr by 1.
void narr_pop(void* narr);

//! Prepend the object pointed to by @p ptr_to_val to the front of narray @p
//! narr.
//! @param narr
//!     Target narray.
//! @param ptr_to_val
//!     Pointer to an object type-compatible with the elements of @p narr.
//! @return
//!     @p narr who's first value is a copy of *@p ptr_to_val.
//! @note
//!     Increases the length of @p narr by 1.
//! @note
//!     *@p ptr_to_val is memory copied to the back of @p narr. If you wish to
//!     create a duplicate of the object pointed to by *@p ptr_to_val with its
//!     own resources you must manually duplicate *@p ptr_to_val before
//!     unshifting.
void* narr_unshift(void* narr, void const* ptr_to_val)
    __attribute__((warn_unused_result));

//! Remove the first element off the front of @p narr.
//! @param narr
//!     Target narray.
//! @note
//!     Decreases the length of @p narr by 1.
void narr_shift(void* narr);

//! Get the first element in target narray.
//! @param narr
//!     Target narray.
//! @return
//!     Pointer to the first element of @p narr if it exists.
//! @return
//!     `NULL` if the @p narr has no first element.
void* narr_front(void const* narr);

//! Get the last element in target narray.
//! @param narr
//!     Target narray.
//! @return
//!     Pointer to the last element of @p narr if it exists.
//! @return
//!     `NULL` if @p narr has no last element.
void* narr_back(void const* narr);

////////////////////////////////////////////////////////////////////////////////
//      STRING                                                                //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
//      CSTRING                       //
////////////////////////////////////////
int cstr_cmp(char const* lhs, char const* rhs);
bool cstr_eq(char const* lhs, char const* rhs);
bool cstr_ne(char const* lhs, char const* rhs);

int cstr_n_cmp(char const* lhs, char const* rhs, size_t n);
bool cstr_n_eq(char const* lhs, char const* rhs, size_t n);
bool cstr_n_ne(char const* lhs, char const* rhs, size_t n);

int cstr_cmp_case(char const* lhs, char const* rhs);
bool cstr_eq_case(char const* lhs, char const* rhs);
bool cstr_ne_case(char const* lhs, char const* rhs);

int cstr_n_cmp_case(char const* lhs, char const* rhs, size_t n);
bool cstr_n_eq_case(char const* lhs, char const* rhs, size_t n);
bool cstr_n_ne_case(char const* lhs, char const* rhs, size_t n);

size_t cstr_len(char const* str);
bool cstr_len_ge(char const* str, size_t n);

void cstr_cat(char* dest, char const* src);
void cstr_n_cat(char* dest, char const* src, size_t n);

void cstr_cpy(char* dest, char const* src);
void cstr_n_cpy(char* dest, char const* src, size_t n);

////////////////////////////////////////
//      NSTRING                       //
////////////////////////////////////////
typedef struct
{
    char* data;
    size_t len;
    size_t capacity;
} nstr_t;

void nstr_init(nstr_t* nstr);
void nstr_init_cstr(nstr_t* nstr, char const* other);
void nstr_init_nstr(nstr_t* nstr, nstr_t const* other);
void nstr_init_fmt(nstr_t* nstr, char const* fmt, ...);
void nstr_fini(nstr_t* nstr);
#define nstr_defer_fini __attribute__((cleanup(nstr_fini)))

void nstr_resize(nstr_t* nstr, size_t len);
void nstr_reserve(nstr_t* nstr, size_t nchars);

void nstr_assign_cstr(nstr_t* nstr, char const* other);
void nstr_assign_nstr(nstr_t* nstr, nstr_t const* other);
void nstr_assign_fmt(nstr_t* nstr, char const* fmt, ...);

void nstr_clear(nstr_t* nstr);

int nstr_cmp(nstr_t const* lhs, nstr_t const* rhs);
bool nstr_eq(nstr_t const* lhs, nstr_t const* rhs);
bool nstr_ne(nstr_t const* lhs, nstr_t const* rhs);

int nstr_cmp_case(nstr_t const* lhs, nstr_t const* rhs);
bool nstr_eq_case(nstr_t const* lhs, nstr_t const* rhs);
bool nstr_ne_case(nstr_t const* lhs, nstr_t const* rhs);

void nstr_cat_cstr(nstr_t* dest, char const* src);
void nstr_cat_nstr(nstr_t* dest, nstr_t const* src);
void nstr_cat_fmt(nstr_t* dest, char const* fmt, ...);

////////////////////////////////////////////////////////////////////////////////
//      DICTIONARY UTILITIES                                                  //
////////////////////////////////////////////////////////////////////////////////
#define DICT_MAX_ELEMENTS UINT32_MAX

#define DICT_PRIMES_LENGTH 26
//! List of prime number roughly splitting incremental powers of two.
//! Creating a hash table with a number of slots equal to a prime in this list
//! will probably have good distribution.
extern uint32_t const dict_primes[DICT_PRIMES_LENGTH];

//! Find the density of a dictionary satisfying the equation:
//! ```
//! LOAD_FACTOR = SLOTS_IN_USE / SLOTS_TOTAL
//! ```
//! @param slots_in_use
//!     Number of non-empty slots within the dictionary.
//! @param slots_total
//!     Total number of slots within the dictionary.
//! @note
//!     The true load factor for a dictionary depends on the method used to
//!     store elements.
//!     For example, this function will *not* count the load factor for a
//!     dictionary using bucketed storage.
double dict_slot_load_factor(uint32_t slots_in_use, uint32_t slots_total);

//! Calculate the index into a dictionary.
//! For dictionary traversal using linear, quadratic, etc. probing the value of
//! this macro will correspond to the starting index for the probe.
//! For a dictionary using bucketed storage the value of this macro will
//! correspond to the index of the target bucket.
#define DICT_HASH_IDX(hash, dict_slots_total) (hash % dict_slots_total)

//! Move a linear probe forward by one element.
//! @param probe_idx @multi_eval
//!     Lvalue containing the current index of the probe.
//! @param dict_slots_total
//!     Total number of slots within the dictionary.
//!     This value is used for modulo-wrapping.
#define DICT_INCR_LINEAR_PROBE(probe_idx, dict_slots_total)                    \
    (probe_idx = (probe_idx + 1) % dict_slots_total);

//! #npanic indicating that the dictionary has grown too large.
#define NPANIC_DICT_TOO_LARGE() npanic("dictionary is too large");

//! Compute the hash of a character slice for dictionary indexing.
//! @param start
//!     Pointer to the first character in the character slice.
//! @param length
//!     Number of characters in the slice.
uint32_t hash_char_slice(char const* start, size_t length);

////////////////////////////////////////////////////////////////////////////////
//      IO                                                                    //
////////////////////////////////////////////////////////////////////////////////
//! Log level for debugging purposes.
#define LOG_DEBUG (1 << 0)
//! Log level for performance metrics.
#define LOG_METRIC (1 << 1)
//! Log level with no positive or negative connotation.
#define LOG_INFO (1 << 2)
//! Log level indicating a warning.
#define LOG_WARNING (1 << 3)
//! Log level indicating an error.
#define LOG_ERROR (1 << 4)
//! Log level indicating an unrecoverable error.
#define LOG_FATAL (1 << 5)

//! Global logging configuration for the program, designed to be set once just
//! after application startup.
//! Assign to #log_config the logical OR of all the log levels to be written.
extern int log_config;
//! Default value of #log_config at application startup.
#define DEFAULT_LOG_CONFIG                                                     \
    (LOG_INFO | LOG_DEBUG | LOG_WARNING | LOG_ERROR | LOG_FATAL)

//! Pointer to the output file stream that all logging is written to.
extern FILE* const* log_stream;
//! Default value of #log_stream at application startup.
#define DEFAULT_LOG_STREAM (&stdout)

//! If #log_stream is a tty, bold and color the provided cstring literal using a
//! standardized color.
//! If #log_stream is not a tty, 'quote' the cstring literal so that a user
//! seeing the "emphasized" text in a file understands that it is a snippet of
//! text that is different from any surrounding text.
//! This macro curries #ANSI_ESC_IF_TTY_ELSE_QUOTE.
//! @br
//! ```
//! EMPHASIZE("foo")
//!     isatty  => "<BEGIN-BOLD-AND-COLOR>foo<BEGIN-BOLD-AND-COLOR>"
//!     !isatty => "\'foo\'"
//! ```
//! @note
//!     If @p FILEptr is indeed a tty and @p target is escaped, the sequence is
//!     terminated with #ANSI_ESC_DEFAULT, setting all following output to have
//!     default text representation in the terminal.
#define EMPHASIZE(cstr_literal)                                                \
    ANSI_ESC_IF_TTY_ELSE_QUOTE(                                                \
        *log_stream, ANSI_ESC_BOLD ANSI_ESC_COLOR_MAGENTA, cstr_literal)

//! Log a message at the specified log level using printf-style formatting.
//! @param level
//!     Log level the message will be written at.
//! @param fmt
//!     printf-style format string.
//! @param ...
//!     Arguments to the format specifiers of @p fmt.
//! @note
//!     A newline is automatically appended to the format string.
void nlogf(int level, char const* fmt, ...)
    __attribute__((format(printf, 2, 3)));

//! nlogf variant that prints `<file>:<line>:<column>` before the log-level
//! string. Used for printing a location in source code along with a message.
//! @param level
//!     Log level the message will be written at.
//! @param path
//!     File path string used for `<file>`.
//! @param line
//!     Used for `<line>`.
//! @param col
//!     Used for `<column>`.
//! @param fmt
//!     printf-style format string.
//! @param ...
//!     Arguments to the format specifiers of @p fmt.
//! @note
//!     A newline is automatically appended to the format string.
void nlogf_flc(
    int level, char const* path, size_t line, size_t col, char const* fmt, ...)
    __attribute__((format(printf, 5, 6)));

//! nlogf variant that prints `<file>:<line>:<column>` before the log-level
//! string as well as display of the location in-source.
//! @param level
//!     Log level the message will be written at.
//! @param path_src
//!     Contents of `<file>`.
//! @param path
//!     File path string used for `<file>`.
//! @param line
//!     Used for `<line>`.
//! @param col
//!     Used for `<column>`.
//! @param fmt
//!     printf-style format string.
//! @param ...
//!     Arguments to the format specifiers of @p fmt.
//! @note
//!     A newline is automatically appended to the format string.
void nlogf_pretty(
    int level,
    nstr_t const path_src,
    char const* path,
    size_t line,
    size_t col,
    char const* fmt,
    ...) __attribute__((format(printf, 6, 7)));

//! Check the existence of a file.
//! @param path
//!     Relative path of the file.
//! @return
//!     #true if the file exists.
//! @return
//!     #false otherwise.
bool file_exist(char const* path);

//! Check the size of a file in bytes.
//! @param path
//!     Relative path of the file.
//! @return
//!     The size of the file in bytes on success.
//! @return
//!     -1 on failure.
ssize_t file_length(char const* path);

//! Read all text from a file opened in binary mode.
//! @param path
//!     Relative path of the file.
//! @param buff
//!     Buffer to store the contents of the file.
//! @param bufsize
//!     Number of characters that @p buff can hold, including the null
//!     terminator.
//! @return
//!     Number of bytes read on success.
//! @return
//!     -1 on failure, including if @p bufsize was not large enough to hold the
//!     null terminated string containing the file's content.
ssize_t file_read_into_buff(char const* path, char* buff, size_t bufsize);

//! Read all text from a file opened in binary mode into an nstring.
//! @param path
//!     Relative path of the file.
//! @param nstr
//!     Target nstring.
//! @return
//!     Number of bytes read on success.
//! @return
//!     -1 on failure.
ssize_t file_read_into_nstr(char const* path, nstr_t* nstr);

//! Remove the potentially non-empty directory specified by @p dirpath.
//! @param dirpath
//!     Path of the directory to remove.
//! @return
//!     0 on sucdess.
//! @return
//!     -1 on failure.
int remove_rf_dir(char const* dirpath);

//! Get the length of the formatted string (without its null terminator) using
//! printf-style formatting.
//! @param fmt
//!     printf-style format string.
//! @return
//!     Length of the formatted string without its null terminator on success.
//! @return
//!     -1 on failure.
//! @note
//!     A buffer of `fmt_len(format, ...args)+1` is exactly large enough to hold
//!     the contents of `sprintf(format, ...args)`.
ssize_t fmt_len(char const* fmt, ...);

////////////////////////////////////////////////////////////////////////////////
//      MEMORY                                                                //
////////////////////////////////////////////////////////////////////////////////
//! malloc-like allocator. Returns a non-`NULL` pointer or causes a panic.
//! @param size
//!     Number of bytes to allocate.
//! @return
//!     Pointer to the start of the allocated memory.
//! @note
//!     #nalloc will never return `NULL`.
//! @note
//!     #nalloc does NOT zero the memory it returns.
void* nalloc(size_t size);

#define NALLOC_SIZEOF_TYPEOF(p_variable) nalloc(sizeof(typeof(*(p_variable))))

//! realloc-like allocator. Returns a non-`NULL` pointer or causes a panic.
//! @param ptr
//!     Pointer to the start of a memory block allocated with #nalloc or
//!     #nrealloc.
//! @param size
//!     New size of the reallocated memory block in bytes.
//! @return
//!     Pointer to the start of the reallocated memory.
//! @note
//!     #nrealloc will never return `NULL`.
//! @note
//!     #nalloc does NOT zero newly allocated memory memory block.
//! @warning
//!     Do not call #nrealloc on memory allocated with `malloc` or `realloc`.
void* nrealloc(void* ptr, size_t size);

//! Free memory allocated with #nalloc or #nrealloc.
//! @param ptr
//!     Pointer to the start of a memory block allocated with #nalloc or
//!     #nrealloc.
//! @warning
//!     Do not call #nfree on memory allocated with `malloc` or `realloc`.
void nfree(void* ptr);

//! Swap two objects of a given size in memory.
//! @param p1
//!     Pointer to the first object.
//! @param p2
//!     Pointer to the second object.
//! @param size
//!     `sizeof` @p p1 and @p p2.
void memswap(void* p1, void* p2, size_t size);

////////////////////////////////////////////////////////////////////////////////
//      METRICS                                                               //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////
//      INIT FINI COUNTER             //
////////////////////////////////////////
void _verify_balanced_init_fini(
    char const* type_name, unsigned init_numcalls, unsigned fini_numcalls);

#define INIT_FINI_COUNTER_HEADER(struct_name)                                  \
    extern unsigned _init_numcalls_##struct_name;                              \
    extern unsigned _fini_numcalls_##struct_name;                              \
    void _verify_balanced_init_fini_##struct_name(void)

#define INIT_FINI_COUNTER_SOURCE(struct_name)                                  \
    unsigned _init_numcalls_##struct_name = 0;                                 \
    unsigned _fini_numcalls_##struct_name = 0;                                 \
    void _verify_balanced_init_fini_##struct_name(void)                        \
    {                                                                          \
        _verify_balanced_init_fini(                                            \
            STRINGIFY(struct_name),                                            \
            _init_numcalls_##struct_name,                                      \
            _fini_numcalls_##struct_name);                                     \
    }

#define INIT_FINI_COUNTER_INCR_INIT(struct_name)                               \
    _init_numcalls_##struct_name += 1

#define INIT_FINI_COUNTER_INCR_FINI(struct_name)                               \
    _fini_numcalls_##struct_name += 1

#define INIT_FINI_COUNTER_VERIFY_BALANCED(struct_name)                         \
    _verify_balanced_init_fini_##struct_name()

////////////////////////////////////////
//      HIGH PRECISION TIMING         //
////////////////////////////////////////
//! Calls `clock_gettime` with clock id `CLOCK_MONOTONIC`.
//! Populates @p ts with the time on success.
//! Panics on failure.
void ngettime(struct timespec* ts);

//! Calculated the difference between @p start and @p stop in milliseconds.
long long nelapsed_ms(struct timespec start, struct timespec stop);

////////////////////////////////////////////////////////////////////////////////
//      SIGNAL HANDLING                                                       //
////////////////////////////////////////////////////////////////////////////////
//! Generic signal handler that should report most erroneous signals and exit
//! appropriately.
void generic_sighandler(int signum);

//! Install #generic_sighandler as the defualt `sigaction` handler for most
//! signals.
//! This function is intended to be run before or just shortly after control
//! reaches `main`.
void install_generic_sighandler(void);
