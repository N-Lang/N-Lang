/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once
#include "grammar.h"
#include "nir.h"

////////////////////////////////////////////////////////////////////////////////
// BEGIN Earley Recognizer

//! An LR(0) item consists of a grammar rule and a DOT representing
//! the current position the parser is in. For example, the rule:
//! X -> A B * C D
//! says that in the rule X, A and B have been parsed so far, and the
//! parser is now looking for C.
//!
//! An Earley item is an LR(0) item augmented with a starting position.
//! (X -> A B * C D, N) is an Earley item that says that we've seen
//! the rule X start at N, and we've parsed A and B so far. We don't
//! yet know the end position.
struct parser_earley_item
{
    //! Grammar Rule
    enum parser_grammar_rules rule_index;
    //! Dot
    uint8_t next_item;
    //! Start position
    uint32_t item_start;
};

//! Earley set
//!
//! An Earley set is an insertion-order preserving set that is used
//! by Jay Earley's algorithm. Earley sets are implemented using
//! dynamic arrays, with earley_set_append() used to ensure that
//! the set invariant is maintained.
struct earley_set
{
    //! Slots used so far.
    uint16_t size;
    //! Total availiable slots.
    uint16_t capacity;
    //! Array of stored earley items.
    struct parser_earley_item* data;
};

//! Initialize the Earley Set
void earley_set_init(struct earley_set* earley_set);
//! Add an item to the end of an Earley set if the item does not already exist
void earley_set_append(
    struct earley_set* earley_set, struct parser_earley_item* value);
//! De-initialize the Earley Set
void earley_set_fini(struct earley_set* earley_set);

//! FIRST set
struct first_set
{
    //! Slots used so far.
    uint32_t size;
    //! Total available slots.
    uint32_t capacity;
    //! Array of earley items we're storing.
    enum token_type* data;
};

//! Initialize the FIRST Set
void first_set_init(struct first_set* set);
//! Add an item to the end of a FIRST set if the item does not already exist
void first_set_append(struct first_set* set, enum token_type const value);
//! De-initialize the FIRST Set
void first_set_fini(struct first_set* set);

//! Generate the FIRST set
//!
//! @param set
//!     Set containing list of terminals that could start the rule
//! @param nullable
//!     Array containing information on whether a non-terminal is nullable
void setup_first_set(
    struct first_set set[NUMBER_OF_NONTERMINALS],
    bool const nullable[NUMBER_OF_NONTERMINALS]);

//! When the next symbol seen by the Earley recognizer is a non-terminal
//! symbol, we use the predict function to enumerate all possible derivations
//! that can be parsed according to the grammar. The dot does not move,
//! unless the symbol is nullable.
//!
//! @param table
//!     List of alternatives for a given grammar rule
//! @param items
//!     The Earley Set that we will predict into
//! @param item_start
//!     The position at which the rule is predicted
//! @param r
//!     Current rule, in case of nullable symbol
//! @param lookahead
//!     The current token in the token stream
//! @param first_set
//!     Set containing list of terminals that could start the rule
//! @param completed
//!     The Earley Set we will push into if an Earley item is completed
void predict(
    narr_t(enum parser_grammar_rules) * table,
    struct earley_set* items,
    uint32_t item_start,
    bool nullable,
    struct parser_earley_item r,
    enum token_type lookahead,
    struct first_set first_set,
    struct earley_set* completed);

//! When the next symbol seen by the Earley recognizer is a terminal symbol,
//! we use the scan function to proceed in the recognition process. The
//! dot moves over the terminal symbol.
//!
//! @param items
//!     The Earley Set that we will scan into
//! @param item
//!     The Earley Item being scanned
//! @param completed
//!     The Earley Set we will push into if an Earley item is completed
void scan(
    struct earley_set* items,
    struct parser_earley_item item,
    struct earley_set* completed);

//! When the rule being derived by Earley's algorithm is successfully matched,
//! we use the complete function to move the dot over the non-terminal in the
//! rule that matched it.
//!
//! @param old_items
//!     The Earley Set that we will complete from
//! @param new_items
//!     The Earley Set that we will complete into
//! @param nonterminal
//!     The non-terminal that we are searching for in old_items
//! @param completed
//!     The Earley Set we will push into if an Earley item is completed
//! @param lookahead
//!     The current token in the token stream
//! @param first_set
//!     Set containing list of terminals that could start the rule
//! @param nullable
//!     Contains information on whether nonterminal is nullable
//! @param has_errored
//!     Tells us if we are allowed to use reduction lookahead
void complete(
    struct earley_set old_items,
    struct earley_set* new_items,
    enum parser_nonterminal nonterminal,
    struct earley_set* completed,
    enum token_type lookahead,
    struct first_set* first_set,
    bool* nullable,
    bool has_errored);

//! One step of the recognizer, which runs predict(), scan(), and complete()
//!
//! @param item_working_set
//!     Dynamic Array used as Earley Set
//! @param completed_working_set
//!     Dynamic Array used as Earley Set for Completed Items
//! @param items
//!     The entire Chart containing Earley items
//! @param o
//!     The current index into items
//! @param predict_lookup_table
//!     Lookup table containing list of alternatives for a given grammar rule
//! @param nullable
//!     Array containing information on whether a non-terminal is nullable
//! @param first_set
//!     Array of sets containing list of terminals that could start the rule
//! @param module
//!     The global module, used to lookup tokens
//! @param has_errored
//!     Signal to stop using reduction lookahead in complete()
void earley_recognize(
    struct earley_set item_working_set[2],
    struct earley_set completed_working_set[2],
    struct earley_set* items,
    uint32_t o,
    enum parser_grammar_rules* predict_lookup_table[NUMBER_OF_NONTERMINALS],
    bool nullable[NUMBER_OF_NONTERMINALS],
    struct first_set first_set[NUMBER_OF_NONTERMINALS],
    struct module* module,
    bool has_errored);

// END Earley Recognizer
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// BEGIN Parser

//! The parse tree, also known as a derivation tree, is the ordered
//! rooted tree that represents the syntactic structure of the string
//! being parsed in accordance with the grammar defined in grammar.c
//!
//! The nodes of a parse tree can be terminals, non-terminals, or the
//! empty string. Each node has a span, which ranges [start, stop].
//! The empty string has a span [start, start). Each node contains
//! information about which grammar rule derived it. A non-terminal
//! node will have one or more children, while terminals and empty
//! string have no children.
struct parse_tree
{
    //! Tag representing whether the node in the parse tree is
    //! a terminal, non-terminal, or the empty string.
    enum
    {
        PARSE_TREE_EPSILON,
        PARSE_TREE_NONTERMINAL,
        PARSE_TREE_TERMINAL
    } tag;

    //! A parse tree node can be epsilon, or it contains
    //! either a non-terminal or a terminal symbol.
    union {
        enum parser_nonterminal nonterminal;
        struct token* terminal;
    };

    //! Starting position
    uint32_t start;
    //! Ending postion
    uint32_t stop;

    //! Grammar rule
    enum parser_grammar_rules rule_index;

    //! Children if the node is a non-terminal
    struct parse_tree* children;

    //! Number of children, in case the node is a non-terminal
    uint8_t children_length;
};

//! Parser
//!
//! Since the Earley algorithm is essentially a recognition algorithm
//! that builds a Chart data structure, we need a function to convert
//! the Chart into a parse tree. This function is solely responsible
//! for doing a depth-first search on the Chart and returning the
//! first valid parse tree that is observable.
//!
//! In particular, this function builds the node. Then,
//! earley_build_tree_recursive() builds its children.
//!
//! @param tobuild
//!     The parse tree that we need to build
//! @param items
//!     The entire Chart containing Earley Sets of Earley Items
//! @param tokens
//!     The token stream
//! @param nullable
//!     Array containing information on whether a non-terminal is nullable
void earley_build_tree(
    struct parse_tree* tobuild,
    struct earley_set* items,
    struct token* tokens,
    bool nullable[NUMBER_OF_NONTERMINALS]);

//! Helper struct used to pass around function call arguments
//! in earley_build_tree() and related functions
struct earley_build_tree_params
{
    struct parse_tree* node;
    uint32_t stop;
};

//! See earley_build_tree()
//!
//! @param tobuild
//!     The parse tree that we need to build
//! @param items
//!     The entire Chart containing Earley Sets of Earley Items
//! @param stop
//!     The stop position of the span of a rule
//! @param tokens
//!     The token stream
//! @param ebtp
//!     N-Array containing function parameters passed into earley_build_tree()
//! @param nullable
//!     Array containing information on whether a non-terminal is nullable
void earley_build_tree_recursive(
    struct parse_tree* tobuild,
    struct earley_set* items,
    uint32_t stop,
    struct token* tokens,
    narr_t(struct earley_build_tree_params) * ebtp,
    bool nullable[NUMBER_OF_NONTERMINALS]);

//! Since we are doing a depth first search, it is possible that the search
//! will fail. To avoid the problem of backtracking, this function performs
//! reachability analysis to ensure that the path we traverse will yield
//! a valid parse tree.
//!
//! @param tobuild
//!     The parse tree that we need to build
//! @param items
//!     The entire Chart containing Earley Sets of Earley Items
//! @param start
//!     The start position of the span of a rule
//! @param stop
//!     The stop position of the span of a rule
//! @param start_idx
//!     The number of items since start that we have processed already
//! @param stop_idx
//!     The number of items from stop that we have processed already
//! @param nullable
//!     Array containing information on whether a non-terminal is nullable
bool earley_build_tree_reachable(
    struct parse_tree* tobuild,
    struct earley_set* items,
    uint32_t start,
    uint32_t stop,
    size_t start_idx,
    size_t stop_idx,
    bool nullable[NUMBER_OF_NONTERMINALS]);

//! Free the memory used by the parse tree.
void earley_unbuild_tree(struct parse_tree* root);

// END Parser
////////////////////////////////////////////////////////////////////////////////

//! Perform the syntax analysis phase of the compiling process.
//! @param module
//!     Target module of compilation.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool dophase_syntax_analysis(struct module* module);
