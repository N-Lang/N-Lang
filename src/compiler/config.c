/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "config.h"

// Config defaults.
struct _config config;

__attribute__((constructor)) void init_config(void)
{
    config = (struct _config){.target = TARGET_UNSET,
                              .is_compile_only = false,
                              .is_debug_mode = false,
                              .is_metric_mode = false,
                              .is_gen_debug_info = false,
                              .is_optimize = false,
                              .is_save_temps = false,
                              .input_file = NULL,
                              .output_file = NULL};
    config.link_dirs = narr_alloc(0, sizeof(char const*));
    config.link_libs = narr_alloc(0, sizeof(char const*));
    config.with_files = narr_alloc(0, sizeof(char const*));
}

__attribute__((destructor)) void fini_config(void)
{
    narr_free(config.link_dirs);
    narr_free(config.link_libs);
    narr_free(config.with_files);
}

void print_usage(void)
{
    fprintf(
        stdout,
        // Width of the usage message should always be less than 80 columns.
        // clang-format off
"Usage: %s [options...] infile\n"
"Options:\n"
"  -c, --compile-only               Compile to object file without linking.\n"
"  -d, --compiler-debug             Run the compiler in debug mode.\n"
"  -g, --gen-debug                  Generate debugging information.\n"
"  -m, --compiler-metric            Run the compiler in metric tracking mode.\n"
"  -h, --help                       Print this help message and exit.\n"
"  -L <dir>                         Add <dir> to the library search path.\n"
"  -l <lib>                         Search for and link library <lib>.\n"
"  -o <file>, --outfile <file>      Set output file.\n"
"  -O, --optimize                   Optimize output.\n"
"  -t <val>, --target <val>         Specify compilation target.\n"
"                                   [native, c99]\n"
"  -v, --version                    Print version information and exit.\n"
"  -w, --with <file>                Invoke the c99 compiler with <file>\n"
"  --save-temps                     Save intermediate compilation files.\n",
    COMPILER_NAME_STR
        // clang-format on
    );
}

#define OPT_SAVE_TEMPS 1000
static char const* opts_str = "cdgmL:l:o:Ot:w:";
static struct option opts_long[] = {
    // clang-format off
    {"compile-only",    no_argument,       0, 'c'},
    {"compiler-debug",  no_argument,       0, 'd'},
    {"gen-debug",       no_argument,       0, 'g'},
    {"compiler-metric", no_argument,       0, 'm'},
    {"",                required_argument, 0, 'L'},
    {"",                required_argument, 0, 'l'},
    {"outfile",         required_argument, 0, 'o'},
    {"optimize",        no_argument,       0, 'O'},
    {"target",          required_argument, 0, 't'},
    {"with",            required_argument, 0, 'w'},
    {"save-temps",      no_argument,       0, OPT_SAVE_TEMPS},
    {0, 0, 0, 0}
    // clang-format on
};
stbool prod_config(int argc, char** argv)
{
    int option_index;
    int option;

    for (int i = 0; i < argc; ++i)
    {
        if (cstr_eq(argv[i], "-h") || cstr_eq(argv[i], "--help"))
        {
            goto prod_failure_print_usage;
        }
        if (cstr_eq(argv[i], "-v") || cstr_eq(argv[i], "--version"))
        {
            puts(COMPILER_NAME_STR " " VERSION_STR);
            return STBOOL_FAILURE;
        }
    }

    LOOP_FOREVER
    {
        option = getopt_long(argc, argv, opts_str, opts_long, &option_index);
        if (-1 == option)
        {
            break;
        }

        switch (option)
        {
        case 'c':
        {
            if (config.is_compile_only)
            {
                nlogf(LOG_ERROR, "Compile-only flag already set.");
                goto prod_failure_print_usage;
            }
            config.is_compile_only = true;
        }
        break;

        case 'd':
        {
            if (config.is_debug_mode)
            {
                nlogf(LOG_ERROR, "Debug mode flag already set.");
                goto prod_failure_print_usage;
            }
            config.is_debug_mode = true;
        }
        break;

        case 'g':
        {
            if (config.is_gen_debug_info)
            {
                nlogf(LOG_ERROR, "Gen-debug flag already set.");
                goto prod_failure_print_usage;
            }
            config.is_gen_debug_info = true;
        }
        break;

        case 'L':
        {
            config.link_dirs = narr_push(config.link_dirs, &optarg);
        }
        break;

        case 'l':
        {
            config.link_libs = narr_push(config.link_libs, &optarg);
        }
        break;

        case 'w':
        {
            config.with_files = narr_push(config.with_files, &optarg);
        }
        break;

        case 'm':
        {
            if (config.is_metric_mode)
            {
                nlogf(LOG_ERROR, "Metric tracking mode flag already set.");
                goto prod_failure_print_usage;
            }
            config.is_metric_mode = true;
        }
        break;

        case 'o':
        {
            if (NULL != config.output_file)
            {
                nlogf(LOG_ERROR, "Output file already set.");
                goto prod_failure_print_usage;
            }
            config.output_file = optarg;
        }
        break;

        case 'O':
        {
            if (config.is_optimize)
            {
                nlogf(LOG_ERROR, "Optimize flag already set.");
                goto prod_failure_print_usage;
            }
            config.is_optimize = true;
        }
        break;

        case 't':
        {
            if (TARGET_UNSET != config.target)
            {
                nlogf(LOG_ERROR, "Compilation target already set.");
                goto prod_failure_print_usage;
            }
            if (cstr_eq("native", optarg))
            {
                config.target = TARGET_NATIVE;
            }
            else if (cstr_eq("c99", optarg))
            {
                config.target = TARGET_C99;
            }
            else
            {
                nlogf(LOG_ERROR, "Unknown compilation target '%s'.", optarg);
                goto prod_failure_print_usage;
            }
        }
        break;

        case OPT_SAVE_TEMPS:
        {
            if (config.is_save_temps)
            {
                nlogf(LOG_ERROR, "Save-temps flag already set.");
                goto prod_failure_print_usage;
            }
            config.is_save_temps = true;
        }
        break;

        case ':': /* intentional fallthrough */
        case '?':
            return STBOOL_FAILURE;

        default:
            NPANIC_DFLT_CASE();
        }
    }

    while (optind < argc) // input file(s)
    {
        if (NULL != config.input_file)
        {
            nlogf(LOG_ERROR, "Input file already specified.");
            goto prod_failure_print_usage;
        }
        config.input_file = argv[optind];
        ++optind;
    }

    return STBOOL_SUCCESS;

prod_failure_print_usage:
    print_usage();
    return STBOOL_FAILURE;
}

stbool proc_config(void)
{
    if (TARGET_UNSET == config.target)
    {
        config.target = TARGET_NATIVE;
    }

    if (NULL == config.output_file)
    {
        switch (config.target)
        {
        case TARGET_NATIVE:
            config.output_file = "a.out";
            break;
        case TARGET_C99:
            config.output_file = "a.c";
            break;
        default:
            NPANIC_DFLT_CASE();
        }
    }

    if (NULL == config.input_file)
    {
        nlogf(LOG_ERROR, "No input file specified.");
        goto proc_failure_print_usage;
    }

    return STBOOL_SUCCESS;

proc_failure_print_usage:
    print_usage();
    return STBOOL_FAILURE;
}
