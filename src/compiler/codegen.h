/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "config.h"
#include "nir.h"

//! Perform the code generation phase of the compiling process.
//! @param module
//!     Target module of compilation.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool dophase_codegen(struct module* module);

//============================================================================//
//      NATIVE                                                                //
//============================================================================//
//! Generate native code.
//! @param module
//!     Target module of compilation.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool native_codegen(struct module* module);

//============================================================================//
//      C99                                                                   //
//============================================================================//
struct c99_codegen_context
{
    struct module* module;
    size_t indent_level;
    uint32_t current_scope_uid;

    //! If we are currently within the imports section
    bool is_imports;

    struct scope* defer_stack[4096];
    size_t defer_stack_len;

    //! The function that code generation is acting within.
    //! #NULL if code generation is not acting within a function.
    //! @note
    //!     Not necessarily the immediate child #scope of the function itself in
    //!     the case of additional nested scopes within the function.
    struct function_decl* cur_func;

    nstr_t c_code_nstr;
};
//! @init
void c99_codegen_context_init(
    struct c99_codegen_context* ctx, struct module* module);
//! @fini
void c99_codegen_context_fini(struct c99_codegen_context* ctx);

//! Concatenate the a format string with arguments to the back of C source code
//! nstring within the provided #c99_codegen_context.
//! @param p_context
//!     Pointer to a #c99_codegen_context.
//! @param ...
//!     Format string with arguments that will be appended to the c99 source
//!     code in @p p_context.
#define C99_SRC_APPEND(p_context, ...)                                         \
    nstr_cat_fmt(&((p_context)->c_code_nstr), __VA_ARGS__);

//! Generate C99 code for this compilation unit.
//! @param module
//!     Target module of compilation.
//! @param out_path_override @nullable
//!     Override for the output file path.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool c99_codegen(struct module* module, char const* out_path_override);

void c99_codegen_identifier(
    struct c99_codegen_context* ctx, struct identifier* id);

void c99_codegen_data_type(
    struct c99_codegen_context* ctx,
    struct data_type* dt,
    struct identifier* id);

void c99_codegen_literal(
    struct c99_codegen_context* ctx, struct literal* literal);

void c99_codegen_expr(struct c99_codegen_context* ctx, struct expr* expr);

void c99_codegen_variable_decl_stmt(
    struct c99_codegen_context* ctx, struct variable_decl* variable_decl);

void c99_codegen_function_decl_stmt(
    struct c99_codegen_context* ctx, struct function_decl* function_decl);

void c99_codegen_struct_decl_stmt(
    struct c99_codegen_context* ctx, struct struct_decl* struct_decl);

void c99_codegen_if_stmt(
    struct c99_codegen_context* ctx, struct if_stmt* if_stmt);

void c99_codegen_loop_stmt(
    struct c99_codegen_context* ctx, struct loop_stmt* loop_stmt);

void c99_codegen_alias_stmt(
    struct c99_codegen_context* ctx, struct alias_stmt* alias_stmt);

void c99_codegen_scope(struct c99_codegen_context* ctx, struct scope* scope);

void c99_codegen_stmt(struct c99_codegen_context* ctx, struct stmt* stmt);

void c99_codegen_enum_decl(struct c99_codegen_context*, struct enum_decl*);

void c99_codegen_union_decl(struct c99_codegen_context*, struct union_decl*);

void c99_codegen_variant_decl(
    struct c99_codegen_context*, struct variant_decl*);

void c99_codegen_variant_def(struct c99_codegen_context*, struct variant_def*);

void c99_codegen_member_var(struct c99_codegen_context*, struct member_var*);

void c99_codegen_member_tag(struct c99_codegen_context*, struct member_tag*);

void c99_codegen_member_iface(
    struct c99_codegen_context*, struct member_iface*, struct variant_decl*);
