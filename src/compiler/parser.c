/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file parser.c
 * @brief Earley Parser for the N Language
 *
 * References:
 *   - Grune and Jacobs
 *     _Parsing Techniques - A Practical Guide_
 *     An older version of the text is freely available online:
 *     https://dickgrune.com/Books/PTAPG_1st_Edition/BookBody.pdf
 *   - Jay Earley (1970)
 *     _An efficient context-free parsing algorithm_
 *     DOI: 10.1145/362007.362035
 *   - Aycock and Horspool (2002)
 *     _Practical Earley Parsing_
 *     DOI: 10.1093/comjnl/45.6.620
 *   - Joop Leo (1991)
 *     _A general context-free parsing algorithm running in linear
 *     time on every LR(k) grammar without using lookahead_
 *     DOI: 10.1016/0304-3975(91)90180-A
 *   - Masaru Tomita (1985)
 *     _An Efficient Context-free Parsing Algorithm For Natural Languages_
 *     IJCAI 1985
 *   - Elizabeth Scott (2008)
 *     _SPPF-Style Parsing From Earley Recognisers_
 *     DOI: 10.1016/j.entcs.2008.03.044
 *
 *
 * The Earley Parser is explained in a wide range of text-books.
 * One such reference is the the book by Grune and Jacobs.
 *
 * The recognizer uses the algorithm proposed by Jay Earley (1970)
 *
 * The recognizer includes a bug fix for nullable symbols
 * proposed by Aycock and Horspool (2002).
 *
 * TODO: While the Earley parser can handle left/right/middle
 * recursion, it is faster handling left recursion than right
 * recursion. To improve the performance on right-recursive
 * grammars, we could use the memoization trick for recognizing
 * LR grammars in linear time, as proposed by Joop Leo (1991).
 *
 * The recognizer includes prediction lookahead, and reduction
 * lookahead, as mentioned in the book by Grune and Jacobs.
 *
 * The reasons for choosing an Earley recognizer are:
 *   - It can parse general context-free grammars. We no longer
 *     have to contort the grammar to fit under restricted
 *     classes such as LL(k), LR(1), etc.
 *   - It can have good error handling
 *   - It is relatively simple to implement
 *   - For the purposes of the N grammar, the Earley recognizer
 *     runs in linear time and takes linear space
 *
 * It is not straightforward to convert an Earley recognizer into
 * an Earley parser. The reason is that the Earley parse forest
 * representation over-represents valid parses. This has been
 * described in detail by Masaru Tomita (1985).
 *
 * TODO: For converting the Earley recognizer into an Earley parser,
 * we currently use the technique proposed by Grune and Jacobs.
 * If we wanted to get a shared-packed parse forest representation
 * that can represent all possible derivations, then we should
 * use the trick proposed by Elizabeth Scott (2008), under the section
 * _An integrated parsing algorithm_
 *
 * There are some alternatives to Earley that have been considered:
 *   - Recursive Descent (Pratt)
 *     This has been used in the compiler earlier. The problem with
 *     this approach is that it is restricted to LL(k) grammars, and
 *     it is easy to introduce bugs into the parser for a grammar of
 *     a reasonable size.
 *   - GLL
 *     GLL is a general recursive-descent like parsing technique.
 *     It maintains all the niceness of recursive descent. But this
 *     would also imply that it is easy to introduce bugs into the parser
 *     for a grammar of a reasonable size.
 *
 * TODO: If performance ever becomes an issue, then we should implement
 * the automaton proposed in Aycock and Horspool (2002). The performance
 * of an Earley parser using such an automaton should be comparable to
 * that of parsers generated from tools such as yacc/bison.
 *
 */

#include "parser.h"
#include "lowering.h"
#include "nutils.h"
#include "parser_errors.h"
#include "semantic.h"

////////////////////////////////////////////////////////////////////////////////
// Phase: Context-Free Parsing, Syntax analysis

stbool dophase_syntax_analysis(struct module* module)
{
    stbool return_value = STBOOL_SUCCESS;

    // The input to the parser is a stream of tokens from the lexer
    uint32_t tokens_length = (uint32_t)narr_length(module->tokens);

    // The Earley Working Set
    //
    // The key insight here is that only the current Earley Set
    // and the next Earley set are being modified at a single time.
    //
    // Hence, we use two working sets of dynamic size.
    //
    // The actual Chart size can then allocate based on the size
    // of the this working set, making the actual Chart have a
    // size known at runtime, reducing the amount of allocations
    // and re-allocations we have to perform in total.
    struct earley_set item_working_set[2];
    earley_set_init(&item_working_set[0]);
    earley_set_init(&item_working_set[1]);

    // Earley Working Set for completed items
    struct earley_set completed_working_set[2];
    earley_set_init(&completed_working_set[0]);
    earley_set_init(&completed_working_set[1]);

    // The Chart
    //
    // The Chart data structure is a dynamic array of Earley Sets.
    // The Earley Parser is a Chart Parser. Chart Parsers are
    // suitable for ambiguous grammars. They use dynamic programming
    // to eliminate backtracking and prevent combinatorial explosion.
    //
    // REFERENCE: https://en.wikipedia.org/wiki/Chart_parser
    struct earley_set* items =
        nalloc(sizeof(struct earley_set) * (tokens_length + 1));

    // Chart containing completed items only.
    //
    // Since we only need completed items when converting the
    // recognizer into a parser, we can free the other items
    // to make space for the parse tree. To make this process
    // easier, we use a separate Chart containing completed items only.
    struct earley_set* completed =
        nalloc(sizeof(struct earley_set) * (tokens_length + 1));

    // To make prediction faster, we pre-compute values to be predicted.
    // This was proposed in the original paper by Jay Earley (1970).
    narr_t(enum parser_grammar_rules)
        predict_lookup_table[NUMBER_OF_NONTERMINALS];
    for (enum parser_nonterminal i = 0; i < NUMBER_OF_NONTERMINALS; ++i)
    {
        predict_lookup_table[i] =
            narr_alloc(0, sizeof(enum parser_grammar_rules));
    }

    // Initialize the Charts
    for (uint32_t i = 0; i <= tokens_length; ++i)
    {
        items[i].size = 0;
        items[i].capacity = 0;
        completed[i].size = 0;
        completed[i].capacity = 0;
    }

    // FIRST set
    //
    // The FIRST set aids at predictive parsing by providing lookahead.
    //
    // REFERENCE: https://www.google.com/search?q=FIRST+and+FOLLOW+set
    struct first_set first_set[NUMBER_OF_NONTERMINALS];
    for (enum parser_nonterminal i = 0; i < NUMBER_OF_NONTERMINALS; ++i)
    {
        first_set_init(&first_set[i]);
    }

    // The Nullable Set
    //
    // The Nullable Set contains information about whether a nonterminal
    // is nullable.
    bool nullable[NUMBER_OF_NONTERMINALS] = {0};

    // Construct prediction lookup table
    //
    // To make prediction faster, we pre-compute values to be predicted.
    // In Jay Earley's (1970) paper, he describes this using the words:
    // _For each nonterminal, we keep a linked list of its alternatives,
    // for use in prediction._
    for (enum parser_grammar_rules i = 0; i < NUMBER_OF_GRAMMAR_RULES; ++i)
    {
        const enum parser_nonterminal index = parser_grammar[i].nonterminal;
        predict_lookup_table[index] =
            narr_push(predict_lookup_table[index], &i);
    }

    // Construct the Nullable Set
    //
    // Isolate this block
    {
        // Initial size of the nullable set
        uint32_t nullable_size = 0;

        // Previously known size of the nullable set
        uint32_t old_nullable_size = 1; // NOTE: any number other than 0 works
        // Compute the fixed point
        //
        // REFERENCE: https://en.wikipedia.org/wiki/Fixed_point_(mathematics)
        while (old_nullable_size != nullable_size)
        {
            old_nullable_size = nullable_size;

            // For each grammar rule
            for (enum parser_grammar_rules i = 0; i < NUMBER_OF_GRAMMAR_RULES;
                 ++i)
            {
                uint32_t nonterminal = parser_grammar[i].nonterminal;
                struct parser_symbol const* symbol_list =
                    parser_grammar[i].symbol_list;

                // If the Grammar rule isn't known to be nullable
                while (!nullable[nonterminal])
                {
                    // Check if the rule is nullable by seeing if we
                    // reach the END_TERMINAL by traversing through
                    // nullable non-terminals only.
                    if (symbol_list->tag == END_TERMINAL)
                    {
                        nullable[nonterminal] = true;
                        nullable_size += 1;
                    }
                    // If we see a nullable non-terminal, continue
                    else if (
                        symbol_list->tag == NON_TERMINAL
                        && nullable[symbol_list->nonterminal])
                    {
                        symbol_list += 1; // increment the pointer
                    }
                    // If we see a terminal, or a non-terminal that isn't
                    // yet known to be nullable, then exit
                    else
                    {
                        break;
                    }
                }
            }
        }
    }

    // Setup the FIRST set
    setup_first_set(first_set, nullable);

    ////////////////////////////////////////////////////////
    // Begin Earley's algorithm

    // Add the augmented grammar rule TOP
    //
    // We intentionally augment the grammar. If the
    // start symbol is S, then we add the rule:
    // TOP -> S 'EOF'
    struct parser_earley_item init_item1 = {
        .rule_index = RULE_TOP, .next_item = 0, .item_start = 0};
    earley_set_append(&item_working_set[0], &init_item1);

    uint32_t last_position = 0;

    // Reduction lookahead makes error handling more difficult.
    // We run the recognizer without reduction lookahead.
    bool has_errored = true;

    // Loop over state sets
    for (uint32_t o = 0; o <= tokens_length; ++o)
    {
        // If the number of items is ever zero, then we need to perform
        // error handling. This implies that a required token was not
        // successfully scanned.
        if (item_working_set[0].size == 0)
        {
            handle_error(items, o, first_set, module);
            return_value = STBOOL_FAILURE;
            break;
        }

        if (o != tokens_length)
        {
            // Run predict(), scan(), and complete() functions
            // for each element of the state set
            earley_recognize(
                item_working_set,
                completed_working_set,
                items,
                o,
                predict_lookup_table,
                nullable,
                first_set,
                module,
                has_errored);
        }

        // Initialize the Earley Set

        // items[o] = working_set[0]
        items[o].size = item_working_set[0].size;
        items[o].capacity = item_working_set[0].size;
        items[o].data = nalloc(
            sizeof(struct parser_earley_item) * item_working_set[0].size);
        for (size_t i = 0; i < item_working_set[0].size; ++i)
        {
            items[o].data[i] = item_working_set[0].data[i];
        }
        // working_set[0] = working_set[1]
        item_working_set[0].size = item_working_set[1].size;
        if (item_working_set[0].size >= item_working_set[0].capacity)
        {
            item_working_set[0].capacity *= 2;
            item_working_set[0].data = realloc(
                item_working_set[0].data,
                sizeof(struct parser_earley_item)
                    * item_working_set[0].capacity);
        }
        for (size_t i = 0; i < item_working_set[1].size; ++i)
        {
            item_working_set[0].data[i] = item_working_set[1].data[i];
        }
        // working_set[1] = []
        item_working_set[1].size = 0;

        // Initialize the Completed Earley Set

        // completed[o] = working_set[0]
        completed[o].size = completed_working_set[0].size;
        completed[o].capacity = completed_working_set[0].size;
        completed[o].data = nalloc(
            sizeof(struct parser_earley_item) * completed_working_set[0].size);
        for (size_t i = 0; i < completed_working_set[0].size; ++i)
        {
            completed[o].data[i] = completed_working_set[0].data[i];
        }
        // working_set[0] = working_set[1]
        completed_working_set[0].size = completed_working_set[1].size;
        if (completed_working_set[0].size >= completed_working_set[0].capacity)
        {
            completed_working_set[0].capacity *= 2;
            completed_working_set[0].data = realloc(
                completed_working_set[0].data,
                sizeof(struct parser_earley_item)
                    * completed_working_set[0].capacity);
        }
        for (size_t i = 0; i < completed_working_set[1].size; ++i)
        {
            completed_working_set[0].data[i] = completed_working_set[1].data[i];
        }
        // working_set[1] = []
        completed_working_set[1].size = 0;

        last_position = o;
    }

    // Cleanup Working Sets
    earley_set_fini(&item_working_set[0]);
    earley_set_fini(&item_working_set[1]);

    earley_set_fini(&completed_working_set[0]);
    earley_set_fini(&completed_working_set[1]);

    // End Earley's algorithm
    ////////////////////////////////////////////////////////

    if (return_value == STBOOL_SUCCESS
        && (items[tokens_length].size == 0
            || !(items[tokens_length].data[0].rule_index == RULE_TOP
                 && items[tokens_length].data[0].next_item == 2
                 && items[tokens_length].data[0].item_start == 0)))
    {
        handle_error(items, tokens_length, first_set, module);
        return_value = STBOOL_FAILURE;
    }

    // Cleanup
    for (uint32_t i = 0; i <= last_position; ++i)
    {
        earley_set_fini(&items[i]);
    }
    nfree(items);

    // Cleanup
    for (uint32_t i = 0; i < NUMBER_OF_NONTERMINALS; ++i)
    {
        first_set_fini(&first_set[i]);
        narr_free(predict_lookup_table[i]);
    }

    // Convert the Earley recognizer into a parser that returns
    // a single parse tree
    struct parse_tree root;
    if (return_value == STBOOL_SUCCESS)
    {
        earley_build_tree(&root, completed, module->tokens, nullable);
    }

    // Cleanup
    for (uint32_t i = 0; i <= last_position; ++i)
    {
        earley_set_fini(&completed[i]);
    }
    nfree(completed);

    // Lower the parse tree to NIR
    if (return_value == STBOOL_SUCCESS)
    {
        lower_to_nir(module, &root);
    }

    // Cleanup
    if (return_value == STBOOL_SUCCESS)
    {
        earley_unbuild_tree(&root);
    }

    return return_value;
}

// Run one step of the Earley recognizer
//
// This loops through a state set running predict, scan, and complete
void earley_recognize(
    struct earley_set working_set[2],
    struct earley_set completed_working_set[2],
    struct earley_set* items,
    uint32_t o,
    enum parser_grammar_rules* predict_lookup_table[NUMBER_OF_NONTERMINALS],
    bool nullable[NUMBER_OF_NONTERMINALS],
    struct first_set first_set[NUMBER_OF_NONTERMINALS],
    struct module* module,
    bool has_errored)
{
    // Loop over Earley items in state set
    for (size_t i = 0; i < working_set[0].size; ++i)
    {
        // Grab an Earley item
        struct parser_earley_item r = working_set[0].data[i];

        // Get the symbol from the Earley item
        struct parser_symbol sym =
            parser_grammar[r.rule_index].symbol_list[r.next_item];

        // Predict if the symbol is a non-terminal
        if (NON_TERMINAL == sym.tag)
        {
            predict(
                &predict_lookup_table[sym.nonterminal],
                &working_set[0],
                o,
                nullable[sym.nonterminal],
                r,
                module->tokens[o].tag,
                first_set[sym.nonterminal],
                &completed_working_set[0]);
        }

        // Scan if the symbol is a terminal
        else if (
            TERMINAL == sym.tag && sym.terminal == module->tokens[o].tag
            && o + 1 <= narr_length(module->tokens))
        {
            scan(&working_set[1], r, &completed_working_set[1]);
        }

        // Complete if the symbol is an end terminal
        else if (END_TERMINAL == sym.tag)
        {
            complete(
                items[r.item_start],
                &working_set[0],
                parser_grammar[r.rule_index].nonterminal,
                &completed_working_set[0],
                module->tokens[o].tag,
                first_set,
                nullable,
                has_errored);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Dynamic Array with the Set property

// Initialize the SET
#define SET_INIT(NAME, STYPE, VTYPE)                                           \
    void NAME(STYPE* set)                                                      \
    {                                                                          \
        set->size = 0;                                                         \
        set->capacity = 1;                                                     \
        set->data = nalloc(sizeof(VTYPE) * set->capacity);                     \
    }
// Append to the SET if element doesn't already exist
#define SET_APPEND(NAME, STYPE, VTYPE)                                         \
    void NAME(STYPE* set, VTYPE const value)                                   \
    {                                                                          \
        for (size_t i = 0; i < set->size; ++i)                                 \
        {                                                                      \
            if (value == set->data[i])                                         \
            {                                                                  \
                return;                                                        \
            }                                                                  \
        }                                                                      \
        if (set->size >= set->capacity)                                        \
        {                                                                      \
            set->capacity *= 2;                                                \
            set->data = realloc(set->data, sizeof(VTYPE) * set->capacity);     \
        }                                                                      \
        set->data[set->size] = value;                                          \
        set->size = set->size + 1;                                             \
    }
// Free the SET
#define SET_FREE(NAME, STYPE)                                                  \
    void NAME(STYPE* set)                                                      \
    {                                                                          \
        nfree(set->data);                                                      \
    }

SET_INIT(earley_set_init, struct earley_set, struct parser_earley_item);

// Since (STRUCT == STRUCT) is weakly defined in C, we have to explicitly
// write this function out rather than using the macro SET_APPEND
void earley_set_append(
    struct earley_set* earley_set, struct parser_earley_item* value)
{
    for (size_t i = 0; i < earley_set->size; ++i)
    {
        struct parser_earley_item* it = &earley_set->data[i];
        if (value->rule_index == it->rule_index
            && value->next_item == it->next_item
            && value->item_start == it->item_start)
        {
            return;
        }
    }
    if (earley_set->size >= earley_set->capacity)
    {
        earley_set->capacity *= 2;
        earley_set->data = realloc(
            earley_set->data,
            sizeof(struct parser_earley_item) * earley_set->capacity);
    }
    earley_set->data[earley_set->size] = *value;
    earley_set->size = earley_set->size + 1;
}

SET_FREE(earley_set_fini, struct earley_set);

SET_INIT(first_set_init, struct first_set, enum token_type);

SET_APPEND(first_set_append, struct first_set, enum token_type);

SET_FREE(first_set_fini, struct first_set);

////////////////////////////////////////////////////////////////////////////////
// The three main steps of Earley's recognizer
//
// 1. Predictor (a.k.a. predict)
// 2. Scanner   (a.k.a. scan)
// 3. Completer (a.k.a. complete)

// Lookahead, as mentioned in the book by Grune and Jacobs
#define LOOKAHEAD(sym, lookahead, nullable, first_set)                         \
    if (sym.tag == TERMINAL && sym.terminal != lookahead)                      \
    {                                                                          \
        continue;                                                              \
    }                                                                          \
    else if (sym.tag == NON_TERMINAL && !nullable)                             \
    {                                                                          \
        bool valid = false;                                                    \
        for (size_t i = 0; i < first_set.size; ++i)                            \
        {                                                                      \
            if (first_set.data[i] == lookahead)                                \
            {                                                                  \
                valid = true;                                                  \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        if (!valid)                                                            \
        {                                                                      \
            continue;                                                          \
        }                                                                      \
    }

// From Earley's paper:
// _The predictor operation is applicable to a state when there is a
// nonterminal to the right of the dot._
void predict(
    narr_t(enum parser_grammar_rules) * table,
    struct earley_set* items,
    uint32_t item_start,
    bool nullable,
    struct parser_earley_item r,
    enum token_type lookahead,
    struct first_set first_set,
    struct earley_set* completed)
{
    // Loop over the pre-computed lookup table and add these items
    // to the Earley Set
    //
    // In Jay Earley's (1970) paper, he describes this using the words:
    // _For each nonterminal, we keep a linked list of its alternatives,
    // for use in prediction._
    for (size_t i = 0; i < narr_length(*table); ++i)
    {
        // If the symbol is nullable, we must advance the current item.
        //
        // This trick was proposed by Aycock and Horspool (2002)
        //
        // They say:
        // _Our solution involves a simple modification to PREDICTOR_
        // _we eagerly move the dot over a non-terminal if that
        // non-terminal can derive epsilon and effectively 'disappear'._
        if (nullable)
        {
            struct parser_earley_item q = {.rule_index = r.rule_index,
                                           .next_item = r.next_item + 1,
                                           .item_start = r.item_start};
            earley_set_append(items, &q);

            // Add to completed SET if necessary
            if (END_TERMINAL
                == parser_grammar[q.rule_index].symbol_list[q.next_item].tag)
            {
                earley_set_append(completed, &q);
            }
        }

        struct parser_earley_item new_item;

        struct parser_symbol sym = parser_grammar[(*table)[i]].symbol_list[0];

        LOOKAHEAD(sym, lookahead, nullable, first_set);

        new_item.rule_index = (*table)[i];

        // From Earley's (1970) paper:
        // _We put the dot at the beginning of the production in
        // each new state, since we have not scanned any of its
        // symbols yet._
        new_item.next_item = 0;

        // From Earley's (1970) paper:
        // _The pointer is set to i, since the state was created
        // in S(i)_
        new_item.item_start = item_start;

        earley_set_append(items, &new_item);

        // Add to completed SET if necessary
        if (END_TERMINAL
            == parser_grammar[new_item.rule_index]
                   .symbol_list[new_item.next_item]
                   .tag)
        {
            earley_set_append(completed, &new_item);
        }
    }
}

// From Earley's (1970) paper:
// _The scanner compares the symbol with X(i+1), and if they match,
// adds the state to S(i+1), with the dot moved over one_
void scan(
    struct earley_set* items,
    struct parser_earley_item item,
    struct earley_set* completed)
{
    // Move the dot over one
    item.next_item += 1;
    earley_set_append(items, &item);

    // Add to completed SET if necessary
    if (END_TERMINAL
        == parser_grammar[item.rule_index].symbol_list[item.next_item].tag)
    {
        earley_set_append(completed, &item);
    }
}

// From Earley's (1970) paper:
// _Completer is applicable to a state if its dot is at the end of its
// production. It compares the lookahead string [X(i+1)..X(i+k)]. If
// they match, it goes back to the state set indicated by the pointer,
// and adds all the states which have P at the right of the dot. It
// moves the dot over P in these states._
void complete(
    struct earley_set old_items,
    struct earley_set* new_items,
    enum parser_nonterminal nonterminal,
    struct earley_set* completed,
    enum token_type lookahead,
    struct first_set* first_set,
    bool* nullable,
    bool has_errored)
{
    for (size_t i = 0; i < old_items.size; ++i)
    {
        struct parser_earley_item it = old_items.data[i];

        struct parser_symbol sym =
            parser_grammar[it.rule_index].symbol_list[it.next_item];

        // Check for P over the dot
        if (sym.tag == NON_TERMINAL && sym.nonterminal == nonterminal)
        {
            // Move the dot over P
            it.next_item += 1;

            struct parser_symbol lsym =
                parser_grammar[it.rule_index].symbol_list[it.next_item];

            if (!has_errored)
            {
                LOOKAHEAD(
                    lsym,
                    lookahead,
                    nullable[lsym.nonterminal],
                    first_set[lsym.nonterminal]);
            }

            earley_set_append(new_items, &it);

            // Add to completed SET if necessary
            if (END_TERMINAL
                == parser_grammar[it.rule_index].symbol_list[it.next_item].tag)
            {
                earley_set_append(completed, &it);
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Convert the Earley Recognizer into a Parser
//
// The parser builds a parse tree from the Chart.
// We only obtain one possible parse tree, ignoring ambiguous parses.
//
// This technique is outline in the book by Grune and Jacobs.
//
// Alternate REFERENCE: http://loup-vaillant.fr/tutorials/earley-parsing/parser
//

// Build a parse tree by traversing the Chart
void earley_build_tree(
    struct parse_tree* root,
    struct earley_set* items,
    struct token* tokens,
    bool nullable[NUMBER_OF_NONTERMINALS])
{
    // For each grammar rule, we traverse the items from right to left.
    // Thus, we use a stop variable to indicate where we are in the algorithm.
    uint32_t stop = (uint32_t)narr_length(tokens) - 1;

    *root = (struct parse_tree){.rule_index = RULE_TOP,
                                .start = 0,
                                .stop = stop,
                                .tag = PARSE_TREE_NONTERMINAL,
                                .nonterminal = NONTERMINAL_TOP,
                                .children = NULL,
                                .children_length = 0};

    // We need to use recursion, but we do not care about the return
    // values. Hence we use a stack on the heap to avoid stack overflow.
    narr_t(struct earley_build_tree_params) ebtp =
        narr_alloc(0, sizeof(struct earley_build_tree_params));
    struct earley_build_tree_params r;
    r.node = root;
    r.stop = stop;
    ebtp = narr_push(ebtp, &r);
    // Keep looping until there is no more left to recurse
    for (size_t i = 0; i < narr_length(ebtp); ++i)
    {
        struct earley_build_tree_params x = ebtp[i];
        earley_build_tree_recursive(
            x.node, items, x.stop, tokens, &ebtp, nullable);
    }
    narr_free(ebtp);
}

// It is possible that the path we traverse on the Chart does not
// yield a valid parse. This does not mean there is no valid parse
// tree, rather it implies that we chose the wrong path. Thus, we
// perform reachability analysis to ensure that our search will succeed.
//
// REFERENCE: https://en.wikipedia.org/wiki/Reachability
bool earley_build_tree_reachable(
    struct parse_tree* tobuild,
    struct earley_set* items,
    uint32_t start,
    uint32_t stop,
    size_t start_idx,
    size_t stop_idx,
    bool nullable[NUMBER_OF_NONTERMINALS])
{
    // Grab the grammar rule that we are interested in traversing
    struct parser_rule cur_rule = parser_grammar[tobuild->rule_index];
    uint8_t num_items = tobuild->children_length;

    // Go right to left, searching in the remaining search space
    for (size_t i = stop_idx; i < num_items - start_idx; ++i)
    {
        size_t child_idx = num_items - i - 1;

        struct parser_symbol sym = cur_rule.symbol_list[child_idx];

        nassert(sym.tag == TERMINAL || sym.tag == NON_TERMINAL);

        // If we see a terminal at the final position, then we
        // ensure that the terminal was reachable
        if (sym.tag == TERMINAL && i == num_items - 1 - start_idx)
        {
            return (start == stop);
        }
        // If we see a terminal at a non-final position, decrease
        // the stop value
        else if (sym.tag == TERMINAL)
        {
            stop -= 1;
        }
        // If we see a non-terminal
        else if (sym.tag == NON_TERMINAL)
        {
            // Loop over the Earley set where the non-terminal completed
            struct earley_set es = items[stop + 1];
            for (size_t j = 0; j < es.size; ++j)
            {
                // If a rule we find while looping is of interest
                if (parser_grammar[es.data[j].rule_index].nonterminal
                        == sym.nonterminal
                    && es.data[j].item_start >= start)
                {
                    // If the rule is the EMPTY rule, then perform
                    // reachability analysis without changing the stop value
                    if (es.data[j].next_item == 0
                        && earley_build_tree_reachable(
                               tobuild,
                               items,
                               start,
                               stop,
                               start_idx,
                               i + 1,
                               nullable))
                    {
                        return true;
                    }
                    // If the rule is not the EMPTY case, then perform
                    // reachability analysis by adjusting the stop value
                    else if (
                        es.data[j].next_item != 0
                        && earley_build_tree_reachable(
                               tobuild,
                               items,
                               start,
                               es.data[j].item_start - 1,
                               start_idx,
                               i + 1,
                               nullable))
                    {
                        return true;
                    }
                }
            }
        }
    }

    // Upon finishing reachability analysis, `stop + 1` which is
    // used to traverse the next Earley Set should now be the
    // new `start` value.
    if (stop == start - 1 && stop_idx == num_items - start_idx)
    {
        return true;
    }

    return false;
}

// Build the children of a parse tree node
void earley_build_tree_recursive(
    struct parse_tree* tobuild,
    struct earley_set* items,
    uint32_t stop,
    struct token* tokens,
    narr_t(struct earley_build_tree_params) * ebtp,
    bool nullable[NUMBER_OF_NONTERMINALS])
{
    // Grab the grammar rule that we are interested in traversing
    struct parser_rule cur_rule = parser_grammar[tobuild->rule_index];

    // Find out the number of items in the grammar rule
    uint8_t num_items = 0;
    for (uint8_t i = 0; i < MAX_NUMBER_OF_RHS_SYMBOLS; ++i)
    {
        if (END_TERMINAL == cur_rule.symbol_list[i].tag)
        {
            num_items = i;
            break;
        }
    }

    // Allocate memory for the children
    tobuild->children_length = num_items;
    tobuild->children = nalloc(num_items * sizeof(typeof(*tobuild->children)));

    // Start position
    uint32_t start = tobuild->start;

    // Iteration value, used to cut down search space
    size_t start_idx = 0;

    // Iteration value, used to cut down search space
    size_t stop_idx = 0;

    // Do a first pass left to right, filling in the span for terminals
    //
    // This step is not necessary, but cuts down our search space
    for (size_t i = 0; i < num_items; ++i)
    {
        struct parse_tree* child = &tobuild->children[i];

        struct parser_symbol sym = cur_rule.symbol_list[i];

        nassert(sym.tag == TERMINAL || sym.tag == NON_TERMINAL);

        if (sym.tag == TERMINAL)
        {
            *child = (struct parse_tree){.rule_index = 0,
                                         .start = start,
                                         .stop = start,
                                         .tag = PARSE_TREE_TERMINAL,
                                         .terminal = &tokens[start],
                                         .children = NULL,
                                         .children_length = 0};
            start += 1;
            start_idx += 1;
        }
        else if (sym.tag == NON_TERMINAL)
        {
            break;
        }
    }

    // Do a second pass right to left, filling in the span for terminals
    //
    // This step is not necessary, but cuts down our search space
    for (size_t i = 0; i < num_items; ++i)
    {
        size_t child_idx = num_items - i - 1;
        struct parse_tree* child = &tobuild->children[child_idx];

        struct parser_symbol sym = cur_rule.symbol_list[child_idx];

        nassert(sym.tag == TERMINAL || sym.tag == NON_TERMINAL);

        if (sym.tag == TERMINAL)
        {
            *child = (struct parse_tree){.rule_index = 0,
                                         .start = stop,
                                         .stop = stop,
                                         .tag = PARSE_TREE_TERMINAL,
                                         .terminal = &tokens[stop],
                                         .children = NULL,
                                         .children_length = 0};
            stop -= 1;
            stop_idx += 1;
        }
        else if (sym.tag == NON_TERMINAL)
        {
            break;
        }
    }

    // Go right to left, filling in the remaining items
    //
    for (size_t i = stop_idx; i < num_items - start_idx; ++i)
    {
        // Traverse right to left
        size_t child_idx = num_items - i - 1;
        struct parse_tree* child = &tobuild->children[child_idx];
        struct parser_symbol sym = cur_rule.symbol_list[child_idx];

        nassert(sym.tag == TERMINAL || sym.tag == NON_TERMINAL);

        // If the symbol we see is a terminal, then update the stop
        // value by the span of the terminal (1 in this case)
        if (sym.tag == TERMINAL)
        {
            *child = (struct parse_tree){.rule_index = 0,
                                         .start = stop,
                                         .stop = stop,
                                         .tag = PARSE_TREE_TERMINAL,
                                         .terminal = &tokens[stop],
                                         .children = NULL,
                                         .children_length = 0};
            stop -= 1;
        }
        // If the symbol we see is a terminal, then update the stop
        // value by the span of the non-terminal
        else if (sym.tag == NON_TERMINAL)
        {
            // Loop over the Earley Set of intetrest
            struct earley_set es = items[stop + 1];
            for (size_t j = 0; j < es.size; ++j)
            {
                // Upon finding a grammar rule which may interest us
                if (parser_grammar[es.data[j].rule_index].nonterminal
                        == sym.nonterminal
                    && es.data[j].item_start >= start)
                {
                    // Reachability analysis
                    //
                    // Here, we ensure that the start value of the
                    // final symbol we see is correct. If it is not,
                    // then this item is not of any value to us.
                    if (i == num_items - 1 - start_idx
                        && start != es.data[j].item_start)
                    {
                        continue;
                    }
                    // If we see a non-terminal value that is the EMPTY case,
                    // we reuse the same start value.
                    if (es.data[j].next_item == 0
                        && earley_build_tree_reachable(
                               tobuild,
                               items,
                               start,
                               stop,
                               start_idx,
                               i + 1,
                               nullable))
                    {
                        // If reachability analysis succeeded, then
                        // use the child we found.
                        *child = (struct parse_tree){.rule_index = 0,
                                                     .start = stop,
                                                     .stop = stop - 1,
                                                     .tag = PARSE_TREE_EPSILON,
                                                     .children = NULL,
                                                     .children_length = 0};
                        // Break to ensure that we use the first born child
                        break;
                    }
                    // If we see a non-terminal value that is not the
                    // EMPTY rule, we update the start value.
                    else if (earley_build_tree_reachable(
                                 tobuild,
                                 items,
                                 start,
                                 es.data[j].item_start - 1,
                                 start_idx,
                                 i + 1,
                                 nullable))
                    {
                        // If reachability analysis succeeded, then
                        // use the child we found.
                        *child = (struct parse_tree){
                            .rule_index = es.data[j].rule_index,
                            .start = es.data[j].item_start,
                            .stop = stop,
                            .tag = PARSE_TREE_NONTERMINAL,
                            .nonterminal = sym.nonterminal,
                            .children = NULL,
                            .children_length = 0};

                        // Since we added a child which may contain children
                        // of its own, we add this child to the recursion stack
                        struct earley_build_tree_params r;
                        r.node = child;
                        r.stop = stop;
                        *ebtp = narr_push(*ebtp, &r);

                        stop = child->start - 1;

                        // Break to ensure that we use the first born child
                        break;
                    }
                }
            }
        }
    }
}

// Free memory used by the parse tree
void earley_unbuild_tree(struct parse_tree* root)
{
    // Add a recursion stack on the heap
    narr_t(struct parse_tree) rs = narr_alloc(0, sizeof(struct parse_tree));

    rs = narr_push(rs, root);

    // Loop through the values until there is no more to recurse
    for (size_t i = 0; i < narr_length(rs); ++i)
    {
        // For every node, push all of its children
        for (size_t j = 0; j < rs[i].children_length; ++j)
        {
            rs = narr_push(rs, &rs[i].children[j]);
        }

        // Free only the children
        nfree(rs[i].children);
    }

    // Free the recursion stack
    narr_free(rs);
}

////////////////////////////////////////////////////////////////////////////////
// Helper functions

// Compute the FIRST set
//
// The FIRST set aids at predictive parsing by providing lookahead.
//
// REFERENCE: https://www.google.com/search?q=FIRST+and+FOLLOW+set
void setup_first_set(
    struct first_set set[NUMBER_OF_NONTERMINALS],
    bool const nullable[NUMBER_OF_NONTERMINALS])
{
    bool keep_going = true;
    // Compute the Fixed Point
    //
    // REFERENCE: https://en.wikipedia.org/wiki/Fixed_point_(mathematics)
    while (keep_going)
    {
        keep_going = false;
        // For each grammar rule
        for (size_t i = 0; i < NUMBER_OF_GRAMMAR_RULES; ++i)
        {
            uint32_t index = parser_grammar[i].nonterminal;
            struct parser_symbol const* symbol_list =
                parser_grammar[i].symbol_list;
            uint32_t old_size = set[index].size;

            // Keep looping until we've seen a non-nullable non-terminal
            for (size_t j = 0; symbol_list[j].tag == TERMINAL
                 || (symbol_list[j].tag == NON_TERMINAL
                     && (j == 0 || nullable[symbol_list[j - 1].nonterminal]));
                 ++j)
            {
                // If the first symbol we encounter is a terminal symbol,
                // then add that to the FIRST set
                if (symbol_list[j].tag == TERMINAL)
                {
                    first_set_append(&set[index], symbol_list[j].terminal);
                    break;
                }
                // If we see a non-terminal, then the first symbol may
                // arise from any of it's productions.
                else if (symbol_list[j].tag == NON_TERMINAL)
                {
                    struct first_set iset = set[symbol_list[j].nonterminal];
                    for (size_t k = 0; k < iset.size; ++k)
                    {
                        first_set_append(&set[index], iset.data[k]);
                    }
                }
            }
            uint32_t new_size = set[index].size;
            if (old_size != new_size)
            {
                keep_going = true;
            }
        }
    }
}
