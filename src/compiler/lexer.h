/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "nir.h"

//! Data structure for the lexer.
//!
//! This contains a pointer to the source code, the line number,
//! column number, and length of the token seen so far.
struct lexer
{
    char const* src;
    uint32_t line;
    uint32_t column;
    uint32_t length;
};

// ASCII CHAR SET OF 128
#define TRIE_CHILDREN 128

struct trie
{
    struct trie* children[TRIE_CHILDREN];
    bool end_of_word;
    enum token_type token;
};

struct trie* trie_alloc();
void trie_insert(struct trie* root, char const* key, enum token_type token);
void trie_setup(struct trie* trie);
enum token_type trie_lookup(
    struct trie* root, char const* key, uint32_t len_key);
void trie_free(struct trie* trie);

void start_state(struct module* module, struct lexer* lexer, stbool* status);
void lex_comment(struct module* module, struct lexer* lexer);
void operator_dash(struct module* module, struct lexer* lexer, stbool* status);
void operator_plus(struct module* module, struct lexer* lexer, stbool* status);
void lex_identifier(
    struct module* module, struct lexer* lexer, struct trie* trie);
void lex_string(struct module* module, struct lexer* lexer, stbool* status);
void lex_string_escaped_char(
    struct module* module, struct lexer* lexer, stbool* status);
void check_keyword(struct module* module, struct lexer* lexer);
void lex_char(struct module* module, struct lexer* lexer, stbool* status);
void literal_hexadecimal(
    struct module* module, struct lexer* lexer, stbool* status);
void literal_octal(struct module* module, struct lexer* lexer, stbool* status);
void literal_binary(struct module* module, struct lexer* lexer, stbool* status);
void literal_decimal(
    struct module* module, struct lexer* lexer, stbool* status);
void literal_float(struct module* module, struct lexer* lexer, stbool* status);
void literal_exponent(
    struct module* module, struct lexer* lexer, stbool* status);
void numeric_datatype(
    struct module* module, struct lexer* lexer, stbool* status);

//! Configuration for the lexer to emit comment tokens or not.
//! By default it does not emit comments, but if this variable is set true
//! then it will.
extern bool LEXER_EMIT_COMMENTS;

//! Perform the lexical analysis phase of the compiling process.
//! @param module
//!     Target module of compilation.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool dophase_lexical_analysis(struct module* module);
