import "std/astring.n"a;
import "std/astr.n"a;
import "std/io.n"a;
import "cstd/stdlib.n"a;
import "cstd/time.n"a;

let test_astr    : mut bool = false;
let test_astring : mut bool = true;
let randomize    : mut bool = false;

let numiters : mut u64 = 0u64;

func arg_parse : (argc : u, argv : ^^ ascii) -> void
{
    if(argc < 3u)
    {
        println($"required arguments: <num iterations> <argument>"a);
        exit(1s as sint);
    }
    elif astr_eq(argv[2u], $"-a"a)
    {
        test_astring = true;
    }
    elif astr_eq(argv[2u], $"-b"a)
    {
        test_astring = false;
        test_astr = true;
    }
    elif astr_eq(argv[2u], $"-c"a)
    {
        test_astr = true;
    }
    elif astr_eq(argv[2u], $"-d"a)
    {
        randomize = true;
    }
    elif astr_eq(argv[2u], $"-e"a)
    {
        test_astring = false;
        test_astr = true;
        randomize = true;
    }
    elif astr_eq(argv[2u], $"-f"a)
    {
        test_astring = true;
        test_astr = true;
        randomize = true;
    }
    else
    {
        println($"astring performace test, runs a string performance test."a);
        println($"ast_perf_test <num iterations> <argument>"a);
        println($"  -a          Test astrings only, with constant PRNG"a);
        println($"  -b          Test astrs only, with constant PRNG"a);
        println($"  -c          Test both, with constant PRNG"a);
        println($"  -d          Test astrings only, with time-seed  PRNG"a);
        println($"  -e          Test astrs only, with time-seed PRNG"a);
        println($"  -f          Test both, with time-seed PRNG"a);
        exit(1s as sint);
    }

    astr_to_u64(argv[1u], ?numiters, 10u8);
}

func rand_bool : () -> bool
{
    return u_mod(rand() as u, 2u) == 0u;
}

func rand_u : (max : u) -> u
{
    return u_mod(rand() as u, max);
}

func perf_test : () -> void
{    
    let astr_The    : ^ ascii = $"The"a;
    let astr_quick  : ^ ascii = $"quick"a;
    let astr_brown  : ^ ascii = $"brown"a;
    let astr_fox    : ^ ascii = $"fox"a;
    let astr_jumped : ^ ascii = $"jumped"a;
    let astr_over   : ^ ascii = $"over"a;
    let astr_the    : ^ ascii = $"the"a;
    let astr_sleepy : ^ ascii = $"sleepy"a;
    let astr_dog    : ^ ascii = $"dog"a;
    let strings : [9u] mut ^ ascii;
    strings[0u] = astr_The;
    strings[1u] = astr_quick;
    strings[2u] = astr_brown;
    strings[3u] = astr_fox;
    strings[4u] = astr_jumped;
    strings[5u] = astr_over;
    strings[6u] = astr_the;
    strings[7u] = astr_sleepy;
    strings[8u] = astr_dog;

    let string1 : mut astring;  defer { if test_astring { string1.fini(); } }
    let str1 : mut ^ mut ascii; defer { if test_astr { deallocate(str1); } }
    isolate # allocate somewhat random strings
    {
        let initstr : ^ ascii = strings[rand_u(countof(strings))];
        if test_astring { string1.init_astr(initstr); }
        if test_astr
        {
            let initstr_len : u = astr_length(initstr);
            str1 = allocate(initstr_len + 1u);
            astr_copy_n(str1, initstr, initstr_len);
        }

        let n_segments : u = rand_u(20u);
        loop i : mut u in [0u, n_segments)
        {
            let newstr : ^ ascii = strings[rand_u(countof(strings))];
            if rand_bool() # prepend
            {
                if test_astring
                {
                    string1.prepend_astr(newstr);
                }
                if test_astr
                {
                    let str1len : u = astr_length(str1);
                    let newstrlen : u = astr_length(newstr);
                    let tmp : ^ mut ascii = allocate(str1len + newstrlen + 1u);
                    astr_copy_n(tmp, newstr, newstrlen);
                    astr_append_n(tmp, str1, str1len);
                    deallocate(str1);
                    str1 = tmp;
                }
            }
            else # postpend
            {
                if test_astring
                {
                    string1.append_astr(newstr);
                }
                if test_astr
                {
                    let str1len : u = astr_length(str1);
                    let newstrlen : u = astr_length(newstr);
                    let tmp : ^ mut ascii = allocate(str1len + newstrlen + 1u);
                    astr_copy_n(tmp, str1, str1len);
                    astr_append_n(tmp, newstr, newstrlen);
                    deallocate(str1);
                    str1 = tmp;
                }
            }
        }
        if test_astring && test_astr
        {
            if string1.compare_astr(str1) != 0s
            {
                println($"astring and astr initialize different strings"a);
                exit(1s as sint);
            }
        }
    }

    isolate # do some comparisons and tests.
    {
        let ntests : mut u = rand_u(50u);

        loop i : mut u in [0u, ntests)
        {
            let teststr : ^ ascii = strings[rand_u(countof(strings))];
            let op : u = rand_u(4u);
            if op == 0u # compare
            {
                let string_ret : mut s;
                let str_ret : mut s;
                if test_astring
                {
                    string_ret = string1.compare_astr(teststr);
                }
                if test_astr
                {
                    str_ret = astr_compare(str1, teststr);
                }
                if test_astring && test_astr &&
                   ((string_ret > 0s && str_ret <= 0s) ||
                    (string_ret == 0s && str_ret != 0s) ||
                    (string_ret < 0s && str_ret >= 0s))
                {
                    println($"astring and astr compare do not match."a);
                    print($"astring: "a);
                    println(string1.data());
                    print($"astr:    "a);
                    println(str1);
                    exit(1s as sint);
                }
            }
            elif op == 1u # contains
            {
                let string_ret : mut bool;
                let str_ret : mut bool;
                if test_astring
                {
                    string_ret = string1.contains_astr(teststr);
                }
                if test_astr
                {
                    str_ret = astr_contains(str1, teststr);
                }
                if test_astring && test_astr && (str_ret != string_ret)
                {
                    println($"astring and astr contains do not match."a);
                    print($"astring: "a);
                    println(string1.data());
                    print($"astr:    "a);
                    println(str1);
                    exit(1s as sint);
                }
            }
            elif op == 2u # starts_with
            {
                let string_ret : mut bool;
                let str_ret : mut bool;
                if test_astring
                {
                    string_ret = string1.starts_with_astr(teststr);
                }
                if test_astr
                {
                    str_ret = astr_starts_with(str1, teststr);
                }
                if test_astring && test_astr && (str_ret != string_ret)
                {
                    println($"astring and astr starts with do not match."a);
                    print($"astring: "a);
                    println(string1.data());
                    print($"astr:    "a);
                    println(str1);
                    exit(1s as sint);
                }
            }
            elif op == 3u # ends_with
            {
                let string_ret : mut bool;
                let str_ret : mut bool;
                if test_astring
                {
                    string_ret = string1.ends_with_astr(teststr);
                }
                if test_astr
                {
                    str_ret = astr_ends_with(str1, teststr);
                }
                if test_astring && test_astr && (str_ret != string_ret)
                {
                    println($"astring and astr ends with do not match."a);
                    print($"astring: "a);
                    println(string1.data());
                    print($"astr:    "a);
                    println(str1);
                    exit(1s as sint);
                }
            }
        }
    }
}

func entry : (argc: u, argv: ^^ ascii) -> void
{
    arg_parse(argc, argv);
    if randomize { srand(time(null) as u32); }
    else         { srand(420u as uint); }

    loop i : mut u64 in [0u64, numiters)
    {
        perf_test();
    }
}
